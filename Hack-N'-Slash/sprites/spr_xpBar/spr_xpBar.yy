{
    "id": "631eaa80-54b7-4ee6-affd-ca66ec2130bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xpBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dabc0796-0e43-40fd-94b1-2c857aceb8bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "631eaa80-54b7-4ee6-affd-ca66ec2130bd",
            "compositeImage": {
                "id": "c13136d2-0a7b-4315-9b79-4da7ce020539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dabc0796-0e43-40fd-94b1-2c857aceb8bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ef7324d-eb30-4ddb-8c1f-1a72c6ba5807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dabc0796-0e43-40fd-94b1-2c857aceb8bf",
                    "LayerId": "56a39ca7-57f9-4c90-a643-faf1df4b5a31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "56a39ca7-57f9-4c90-a643-faf1df4b5a31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "631eaa80-54b7-4ee6-affd-ca66ec2130bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}