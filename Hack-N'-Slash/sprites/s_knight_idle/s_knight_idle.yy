{
    "id": "3dfd62bb-226a-4657-8069-1c585b6be11d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 5,
    "bbox_right": 30,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e790474-ff0b-4418-9fd0-9849c487f6b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dfd62bb-226a-4657-8069-1c585b6be11d",
            "compositeImage": {
                "id": "f086a0ee-ed43-4d5e-a591-0fbe92e7610a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e790474-ff0b-4418-9fd0-9849c487f6b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad25d3c6-a26a-47bc-bc20-e474d51622e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e790474-ff0b-4418-9fd0-9849c487f6b5",
                    "LayerId": "a6326ce4-808d-4336-b99e-0e31aee13047"
                }
            ]
        },
        {
            "id": "14cdd32c-0209-4973-b251-95a44d7acfd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dfd62bb-226a-4657-8069-1c585b6be11d",
            "compositeImage": {
                "id": "b04e8bc5-eb1a-4a43-8068-d942688ebd53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14cdd32c-0209-4973-b251-95a44d7acfd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "175a7214-4faf-4e02-9eb9-18ec4bc61440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14cdd32c-0209-4973-b251-95a44d7acfd2",
                    "LayerId": "a6326ce4-808d-4336-b99e-0e31aee13047"
                }
            ]
        },
        {
            "id": "8578f61e-0383-4e19-aa08-e1e2203cbdc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dfd62bb-226a-4657-8069-1c585b6be11d",
            "compositeImage": {
                "id": "cb57a8be-4074-44ea-93d2-0379b114af86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8578f61e-0383-4e19-aa08-e1e2203cbdc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "175e0499-7101-435d-8b38-dddbd1ed7540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8578f61e-0383-4e19-aa08-e1e2203cbdc8",
                    "LayerId": "a6326ce4-808d-4336-b99e-0e31aee13047"
                }
            ]
        },
        {
            "id": "94945704-a031-47a6-90c7-21b3b5a397f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dfd62bb-226a-4657-8069-1c585b6be11d",
            "compositeImage": {
                "id": "b29adf35-da6f-485e-902a-2739d5e19ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94945704-a031-47a6-90c7-21b3b5a397f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc28a685-cd83-4b08-b355-962f488b5316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94945704-a031-47a6-90c7-21b3b5a397f3",
                    "LayerId": "a6326ce4-808d-4336-b99e-0e31aee13047"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "a6326ce4-808d-4336-b99e-0e31aee13047",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dfd62bb-226a-4657-8069-1c585b6be11d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 45
}