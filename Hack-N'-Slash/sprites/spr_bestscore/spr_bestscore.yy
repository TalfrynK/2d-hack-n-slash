{
    "id": "66ad163a-3bc0-4537-b839-01d8ba5d19fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bestscore",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 27,
    "bbox_right": 474,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b198ae9f-d3ac-425d-97eb-2562ba68d582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ad163a-3bc0-4537-b839-01d8ba5d19fa",
            "compositeImage": {
                "id": "23d63c05-101c-464c-8d23-29c2233d0c20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b198ae9f-d3ac-425d-97eb-2562ba68d582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce99f153-02cf-4020-a23f-576d24e1b35d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b198ae9f-d3ac-425d-97eb-2562ba68d582",
                    "LayerId": "818c3db2-0737-4dcb-8553-fea77a95a618"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "818c3db2-0737-4dcb-8553-fea77a95a618",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66ad163a-3bc0-4537-b839-01d8ba5d19fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}