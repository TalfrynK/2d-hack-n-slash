{
    "id": "dc7ea5b4-4b35-42fb-8d09-395d0e0cf5fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_staminaBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8598c057-86a4-4cb5-8a2b-caa9b6e83b00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc7ea5b4-4b35-42fb-8d09-395d0e0cf5fe",
            "compositeImage": {
                "id": "9068a6de-8e81-4edb-815c-7dd4bfadc738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8598c057-86a4-4cb5-8a2b-caa9b6e83b00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df3ea035-7fb2-4045-b84c-0010f5a10995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8598c057-86a4-4cb5-8a2b-caa9b6e83b00",
                    "LayerId": "335ea4dd-b6e2-40aa-9baa-7d1589d68231"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "335ea4dd-b6e2-40aa-9baa-7d1589d68231",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc7ea5b4-4b35-42fb-8d09-395d0e0cf5fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}