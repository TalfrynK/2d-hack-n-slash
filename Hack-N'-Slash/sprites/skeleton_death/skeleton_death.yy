{
    "id": "745f80b4-c67a-42cc-9d64-af38cd810774",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeleton_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4675fe6-32e6-4ea5-983c-cc02a039f4ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "aa47717d-925d-4bab-9301-72e322a18e3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4675fe6-32e6-4ea5-983c-cc02a039f4ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3949d134-151e-4170-8f50-8a2a9cb2dbe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4675fe6-32e6-4ea5-983c-cc02a039f4ed",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "89d6f783-cf92-4df1-9b77-07520a36482e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "464fc5d5-5b12-473d-b527-84ce4da69aa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d6f783-cf92-4df1-9b77-07520a36482e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e517eefc-af84-4d42-b1b9-861f5fba95ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d6f783-cf92-4df1-9b77-07520a36482e",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "8029e050-e314-4f2e-99f3-5c52a13f0e4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "ca128eaa-82a9-43d0-90a3-95a1db650871",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8029e050-e314-4f2e-99f3-5c52a13f0e4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bd185c8-002b-4733-8f59-ef57fa3b7373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8029e050-e314-4f2e-99f3-5c52a13f0e4f",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "3035745c-0987-437c-ab4b-556b67cc9599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "9023f206-30e0-4fc2-9687-4696c867c2c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3035745c-0987-437c-ab4b-556b67cc9599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a835907c-1fc1-4bf4-8ab9-c1709fca55bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3035745c-0987-437c-ab4b-556b67cc9599",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "19df51ae-7fb1-404e-8b27-691ee69da1ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "068a8621-d266-4acd-af38-fbd301a5f152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19df51ae-7fb1-404e-8b27-691ee69da1ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0cccfc7-a592-4eb6-9ea0-c360c0720d28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19df51ae-7fb1-404e-8b27-691ee69da1ca",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "6ec75ce6-ec2b-47ad-a882-47d7766d5c2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "9fe45ec1-bc17-46d3-ad34-6b2c80ce0c2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec75ce6-ec2b-47ad-a882-47d7766d5c2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae13cec-1db2-46b6-89aa-fa8dd12a36ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec75ce6-ec2b-47ad-a882-47d7766d5c2b",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "0e805b9d-19a1-430e-a9f1-48967f81f292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "399b2044-9547-4950-9861-a77aa61da215",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e805b9d-19a1-430e-a9f1-48967f81f292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "568b5beb-bb74-4248-8176-7155279c432e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e805b9d-19a1-430e-a9f1-48967f81f292",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "82bcea29-2bf2-469e-8ecd-af39de26a00e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "b70040ad-ce6e-4598-9286-ec9b0fcf5a56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82bcea29-2bf2-469e-8ecd-af39de26a00e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33790c49-ed15-4a51-b0de-de2143c758d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82bcea29-2bf2-469e-8ecd-af39de26a00e",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "7a5be748-f296-499d-ab86-d281707efc77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "dfdfbbdf-7507-44b6-8643-903a33119e36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a5be748-f296-499d-ab86-d281707efc77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cef2c2c-0528-4561-af2f-6ae6c18c9a79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a5be748-f296-499d-ab86-d281707efc77",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "80fbf0cb-69a7-43f0-ad8e-57cc2b961668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "edefa2ae-4f3d-42ba-ac61-3b448d6cebd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80fbf0cb-69a7-43f0-ad8e-57cc2b961668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "126ac7db-05a0-47af-a145-d68fff541260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80fbf0cb-69a7-43f0-ad8e-57cc2b961668",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "52ee7aff-3bd5-4f5f-8065-1998d0125115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "208117a6-6f5d-4d1c-afd5-e1c59067859c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52ee7aff-3bd5-4f5f-8065-1998d0125115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32f46523-3483-4fd0-8e17-2c93a41ff8f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52ee7aff-3bd5-4f5f-8065-1998d0125115",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "0d1a50c5-0e9b-4772-94d0-6010972ac70c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "35abd295-3a9f-41aa-8f4f-2c55b002f8f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d1a50c5-0e9b-4772-94d0-6010972ac70c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d233d494-90fc-4ca7-a92e-e056efe61b49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d1a50c5-0e9b-4772-94d0-6010972ac70c",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "ca71cf08-e0c8-4884-9d1f-44f1a6ef90e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "8a877558-bfeb-4401-9214-636161413267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca71cf08-e0c8-4884-9d1f-44f1a6ef90e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25c3a602-f38b-40d2-aa13-f569446a740f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca71cf08-e0c8-4884-9d1f-44f1a6ef90e3",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "69bab342-4a38-4000-b6ea-ebb104cf6cad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "3e72ae92-618f-4df5-8496-40cf234ab8b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69bab342-4a38-4000-b6ea-ebb104cf6cad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cafaf931-56ed-42cc-8bc8-13476eab4990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69bab342-4a38-4000-b6ea-ebb104cf6cad",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        },
        {
            "id": "1bb76761-2127-448f-944c-97c97b8d2422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "compositeImage": {
                "id": "3d9b9cb3-8c30-4f36-820d-700b24f572c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bb76761-2127-448f-944c-97c97b8d2422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9493e5-8f54-4fc5-b56f-bdd5f07fea33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bb76761-2127-448f-944c-97c97b8d2422",
                    "LayerId": "464559e0-4812-48c4-84f4-5d06124a4d62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "464559e0-4812-48c4-84f4-5d06124a4d62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "745f80b4-c67a-42cc-9d64-af38cd810774",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 31
}