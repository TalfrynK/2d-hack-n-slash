{
    "id": "eabdf17c-7fdc-4d46-888a-5734f385fa9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ts_Ground",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a1df756-3a57-4a21-a72a-ad88a74d241a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eabdf17c-7fdc-4d46-888a-5734f385fa9e",
            "compositeImage": {
                "id": "79e93abe-9881-4451-bc30-6f693980664b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a1df756-3a57-4a21-a72a-ad88a74d241a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8268f371-bb7f-4e8f-b384-ad9f2a77af01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a1df756-3a57-4a21-a72a-ad88a74d241a",
                    "LayerId": "a7ca9f32-a3f9-4396-b862-07b75d7b14f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "a7ca9f32-a3f9-4396-b862-07b75d7b14f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eabdf17c-7fdc-4d46-888a-5734f385fa9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}