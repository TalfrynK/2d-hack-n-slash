{
    "id": "68eefc75-3adf-42fe-8d69-43e5cba3fb54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_restartgame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 27,
    "bbox_right": 474,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b74a9ae-830d-492f-9fa6-2bab24d7db1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68eefc75-3adf-42fe-8d69-43e5cba3fb54",
            "compositeImage": {
                "id": "d72cae8f-1802-42fc-9bbb-14a9a6d3bd6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b74a9ae-830d-492f-9fa6-2bab24d7db1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ef7e13e-70b1-4f4f-bf16-44fa333d79db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b74a9ae-830d-492f-9fa6-2bab24d7db1f",
                    "LayerId": "cec25ea4-ea99-4742-9ab6-cdb9f6f22724"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "cec25ea4-ea99-4742-9ab6-cdb9f6f22724",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68eefc75-3adf-42fe-8d69-43e5cba3fb54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}