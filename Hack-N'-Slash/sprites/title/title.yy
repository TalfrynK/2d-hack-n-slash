{
    "id": "470d5712-d4dd-4fc9-bec7-0f34b1b8aef3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 167,
    "bbox_left": 80,
    "bbox_right": 667,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "285afdac-e9f8-457d-a606-49341c2f0190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470d5712-d4dd-4fc9-bec7-0f34b1b8aef3",
            "compositeImage": {
                "id": "b3c7e625-11e4-4593-9b45-e4412a7736ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "285afdac-e9f8-457d-a606-49341c2f0190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e5cb266-326e-4778-9f6e-240979087cc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "285afdac-e9f8-457d-a606-49341c2f0190",
                    "LayerId": "1a518bc1-6b22-427b-b722-ffeac66e73e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 350,
    "layers": [
        {
            "id": "1a518bc1-6b22-427b-b722-ffeac66e73e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "470d5712-d4dd-4fc9-bec7-0f34b1b8aef3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 720,
    "xorig": 0,
    "yorig": 0
}