{
    "id": "f9fc6e67-03fb-4c84-9710-a2f55cf174e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 7,
    "bbox_right": 37,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b7786b2-a605-439d-b896-033369c2f936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9fc6e67-03fb-4c84-9710-a2f55cf174e0",
            "compositeImage": {
                "id": "eb3ee30d-de72-4449-a640-b5790117843b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b7786b2-a605-439d-b896-033369c2f936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a642b22-d9d7-4bbd-8caf-6274baf331b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b7786b2-a605-439d-b896-033369c2f936",
                    "LayerId": "9d6c8f5d-4cee-4718-8fc4-4224e028202e"
                }
            ]
        },
        {
            "id": "9a06257a-faa2-43e4-8a1e-ab19701d7667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9fc6e67-03fb-4c84-9710-a2f55cf174e0",
            "compositeImage": {
                "id": "58a5f06b-e5d6-4173-a43e-04a6b616174d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a06257a-faa2-43e4-8a1e-ab19701d7667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff41c41b-bb55-4f5e-a59a-3ccbc34d792c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a06257a-faa2-43e4-8a1e-ab19701d7667",
                    "LayerId": "9d6c8f5d-4cee-4718-8fc4-4224e028202e"
                }
            ]
        },
        {
            "id": "3c9299d6-fdc2-4acf-a296-9ce39eaf28af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9fc6e67-03fb-4c84-9710-a2f55cf174e0",
            "compositeImage": {
                "id": "5b76cfe7-f985-40b1-a42e-28a9fffcc472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c9299d6-fdc2-4acf-a296-9ce39eaf28af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae74cd7-6be6-409d-b055-9d254e6e6532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c9299d6-fdc2-4acf-a296-9ce39eaf28af",
                    "LayerId": "9d6c8f5d-4cee-4718-8fc4-4224e028202e"
                }
            ]
        },
        {
            "id": "6763c201-385c-45be-b743-f4f3ad59c66d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9fc6e67-03fb-4c84-9710-a2f55cf174e0",
            "compositeImage": {
                "id": "f4b1a9d9-1aa3-49d1-8a7b-96140c74b1b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6763c201-385c-45be-b743-f4f3ad59c66d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c95f75d-753d-4652-a7d5-6adbdab8e26e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6763c201-385c-45be-b743-f4f3ad59c66d",
                    "LayerId": "9d6c8f5d-4cee-4718-8fc4-4224e028202e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "9d6c8f5d-4cee-4718-8fc4-4224e028202e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9fc6e67-03fb-4c84-9710-a2f55cf174e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 45
}