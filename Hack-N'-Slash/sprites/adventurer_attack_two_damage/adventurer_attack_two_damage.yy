{
    "id": "35f27019-44b6-4f03-96d1-687714987f4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_attack_two_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 11,
    "bbox_right": 47,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38a57587-a212-403f-8a83-476fca6e0c77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35f27019-44b6-4f03-96d1-687714987f4e",
            "compositeImage": {
                "id": "cad8f97f-94d9-4f72-bf6c-987db58596ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38a57587-a212-403f-8a83-476fca6e0c77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69df416b-71ef-4953-95ff-b1b18faffbf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38a57587-a212-403f-8a83-476fca6e0c77",
                    "LayerId": "55ec7b23-254b-4190-8e7a-047f231e1498"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "55ec7b23-254b-4190-8e7a-047f231e1498",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35f27019-44b6-4f03-96d1-687714987f4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 24,
    "yorig": 36
}