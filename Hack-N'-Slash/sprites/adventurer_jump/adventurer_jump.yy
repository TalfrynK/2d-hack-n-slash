{
    "id": "d30b62e2-ae0c-4d04-8a4d-ecfe4e70a9c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 14,
    "bbox_right": 35,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54fa07e3-3a44-435c-ae7b-f8d3e27e7be3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30b62e2-ae0c-4d04-8a4d-ecfe4e70a9c6",
            "compositeImage": {
                "id": "200d8304-7836-4d20-80ef-435057fd6fc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54fa07e3-3a44-435c-ae7b-f8d3e27e7be3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6da2cf9-dcdf-473d-a47a-745e7c0b237b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54fa07e3-3a44-435c-ae7b-f8d3e27e7be3",
                    "LayerId": "5827f06a-48c2-41aa-a1c5-3b4b2bac4450"
                }
            ]
        },
        {
            "id": "13c1ebd3-aa38-4fe7-a4af-66e53376ccae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30b62e2-ae0c-4d04-8a4d-ecfe4e70a9c6",
            "compositeImage": {
                "id": "97decad1-383d-4a65-88d4-a6a6a1ac65ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13c1ebd3-aa38-4fe7-a4af-66e53376ccae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5514a13b-3549-44f0-b259-1070d383fd66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13c1ebd3-aa38-4fe7-a4af-66e53376ccae",
                    "LayerId": "5827f06a-48c2-41aa-a1c5-3b4b2bac4450"
                }
            ]
        },
        {
            "id": "8fd865b5-6bdb-4ff2-bbb2-6c23204aa069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30b62e2-ae0c-4d04-8a4d-ecfe4e70a9c6",
            "compositeImage": {
                "id": "705306c4-e404-417b-a91b-7a0d96240d97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd865b5-6bdb-4ff2-bbb2-6c23204aa069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd18109a-51f6-4f3e-ab5b-71850db84267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd865b5-6bdb-4ff2-bbb2-6c23204aa069",
                    "LayerId": "5827f06a-48c2-41aa-a1c5-3b4b2bac4450"
                }
            ]
        },
        {
            "id": "caf74221-058a-4bb5-829c-ab12019b4e5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30b62e2-ae0c-4d04-8a4d-ecfe4e70a9c6",
            "compositeImage": {
                "id": "51a0c6e2-0b71-4271-9da6-4f92b73ed7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caf74221-058a-4bb5-829c-ab12019b4e5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d0f98a2-7435-46e1-979b-9d44f320e517",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caf74221-058a-4bb5-829c-ab12019b4e5b",
                    "LayerId": "5827f06a-48c2-41aa-a1c5-3b4b2bac4450"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "5827f06a-48c2-41aa-a1c5-3b4b2bac4450",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d30b62e2-ae0c-4d04-8a4d-ecfe4e70a9c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 36
}