{
    "id": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bandit_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 7,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42ede894-5b61-464c-9797-4e857ec0feee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
            "compositeImage": {
                "id": "5c786f90-df69-4615-b71c-62529cdbfaad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42ede894-5b61-464c-9797-4e857ec0feee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acf0ebec-dad3-47aa-9ec1-4df94a6a4bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42ede894-5b61-464c-9797-4e857ec0feee",
                    "LayerId": "beef0348-a6b1-4f50-b5f0-5307f61c083a"
                }
            ]
        },
        {
            "id": "a715acdf-9df5-4046-93a6-6aaa1ddf0bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
            "compositeImage": {
                "id": "b7883520-179a-49a0-9366-bdcb587a775a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a715acdf-9df5-4046-93a6-6aaa1ddf0bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f80b3697-48af-4589-b7e9-de485402db1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a715acdf-9df5-4046-93a6-6aaa1ddf0bd8",
                    "LayerId": "beef0348-a6b1-4f50-b5f0-5307f61c083a"
                }
            ]
        },
        {
            "id": "81dd1ffb-cc66-4f7b-b197-a37d9d2b5ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
            "compositeImage": {
                "id": "27d263df-369d-4f84-9c55-fbe1b751fa2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81dd1ffb-cc66-4f7b-b197-a37d9d2b5ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aba58ba0-5114-4155-b0cd-b2fa84e639ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81dd1ffb-cc66-4f7b-b197-a37d9d2b5ab3",
                    "LayerId": "beef0348-a6b1-4f50-b5f0-5307f61c083a"
                }
            ]
        },
        {
            "id": "46a683b0-0064-484c-800a-0cdee6e62448",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
            "compositeImage": {
                "id": "07d76d95-e103-4c99-a6cf-81222699d3a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a683b0-0064-484c-800a-0cdee6e62448",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "262dfa9b-74cf-4c6a-8382-d1597c2fa8a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a683b0-0064-484c-800a-0cdee6e62448",
                    "LayerId": "beef0348-a6b1-4f50-b5f0-5307f61c083a"
                }
            ]
        },
        {
            "id": "15a0746a-9edc-4585-afe1-5ce9fc1410f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
            "compositeImage": {
                "id": "a2559c3e-0c8e-4556-8109-0be066f1816c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a0746a-9edc-4585-afe1-5ce9fc1410f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4afd5c-fe38-4973-b21e-0435405ab403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a0746a-9edc-4585-afe1-5ce9fc1410f6",
                    "LayerId": "beef0348-a6b1-4f50-b5f0-5307f61c083a"
                }
            ]
        },
        {
            "id": "4500ec57-a403-4795-ad1e-fc650436fe76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
            "compositeImage": {
                "id": "c6127aed-d485-437e-bfac-873e0b96585c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4500ec57-a403-4795-ad1e-fc650436fe76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9298d069-437c-456b-915e-c72e5176fd5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4500ec57-a403-4795-ad1e-fc650436fe76",
                    "LayerId": "beef0348-a6b1-4f50-b5f0-5307f61c083a"
                }
            ]
        },
        {
            "id": "6839c629-ab4b-4d82-8b59-df78d26b7623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
            "compositeImage": {
                "id": "40b5e307-6ee9-4a21-8ad8-9e15f369e6ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6839c629-ab4b-4d82-8b59-df78d26b7623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb23e79-e516-4b1b-9169-8e86e97d75d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6839c629-ab4b-4d82-8b59-df78d26b7623",
                    "LayerId": "beef0348-a6b1-4f50-b5f0-5307f61c083a"
                }
            ]
        },
        {
            "id": "d8812faf-11b5-424d-8986-57b78028cebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
            "compositeImage": {
                "id": "de80e6f1-d060-47c5-b317-4d36a831668e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8812faf-11b5-424d-8986-57b78028cebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5e5f7c1-8506-44c4-ad22-2fae8e30c3d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8812faf-11b5-424d-8986-57b78028cebb",
                    "LayerId": "beef0348-a6b1-4f50-b5f0-5307f61c083a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "beef0348-a6b1-4f50-b5f0-5307f61c083a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31fd3855-3db0-4d53-b3fc-ee0fcf5c3e14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 45
}