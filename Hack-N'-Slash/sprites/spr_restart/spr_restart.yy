{
    "id": "f04f2c7b-7dcb-4860-8739-af50812d16db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_restart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 27,
    "bbox_right": 474,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4043440a-c49e-448a-9096-605dd645caf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04f2c7b-7dcb-4860-8739-af50812d16db",
            "compositeImage": {
                "id": "28a83f99-20b9-4f66-ae49-ade3688e50cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4043440a-c49e-448a-9096-605dd645caf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f54a4833-92d7-4652-a712-51b35011dc1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4043440a-c49e-448a-9096-605dd645caf2",
                    "LayerId": "8da21942-9181-48d5-8f17-1acf683ec0c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "8da21942-9181-48d5-8f17-1acf683ec0c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f04f2c7b-7dcb-4860-8739-af50812d16db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}