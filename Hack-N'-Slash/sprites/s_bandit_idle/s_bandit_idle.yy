{
    "id": "83aec54f-60e6-4205-94a5-2e64e6b31d3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bandit_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 6,
    "bbox_right": 31,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fb28221-cf47-443c-b8c8-dba3d5b7a30d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83aec54f-60e6-4205-94a5-2e64e6b31d3f",
            "compositeImage": {
                "id": "af8cd8b8-1737-4ccf-a6e9-b591eb3b675f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fb28221-cf47-443c-b8c8-dba3d5b7a30d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3a1ff3-22c1-4f60-aac1-24a7f20b3d40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fb28221-cf47-443c-b8c8-dba3d5b7a30d",
                    "LayerId": "8ff8c2a1-d5f3-4d69-b879-909bc6139a55"
                }
            ]
        },
        {
            "id": "51121cd3-f443-471a-a3db-291145cb2002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83aec54f-60e6-4205-94a5-2e64e6b31d3f",
            "compositeImage": {
                "id": "8baeecc5-d1e6-4233-9fcc-3987b3b945cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51121cd3-f443-471a-a3db-291145cb2002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fef680a5-a24e-4792-b7b5-f2bf52ceafc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51121cd3-f443-471a-a3db-291145cb2002",
                    "LayerId": "8ff8c2a1-d5f3-4d69-b879-909bc6139a55"
                }
            ]
        },
        {
            "id": "6924f28b-1da3-4d71-8afd-d1070c2dcf14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83aec54f-60e6-4205-94a5-2e64e6b31d3f",
            "compositeImage": {
                "id": "2819f75f-6e68-4389-a2d7-14c4bbf2f38c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6924f28b-1da3-4d71-8afd-d1070c2dcf14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6feebaf-404e-4409-98cc-a1474eaae884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6924f28b-1da3-4d71-8afd-d1070c2dcf14",
                    "LayerId": "8ff8c2a1-d5f3-4d69-b879-909bc6139a55"
                }
            ]
        },
        {
            "id": "626d04a8-f9e3-40b4-a5fe-9bd42be2131d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83aec54f-60e6-4205-94a5-2e64e6b31d3f",
            "compositeImage": {
                "id": "550ef6aa-bcae-4dea-a2d6-b204601d176a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "626d04a8-f9e3-40b4-a5fe-9bd42be2131d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bd9dd47-63b4-43f9-aa1c-d2126d2a8c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "626d04a8-f9e3-40b4-a5fe-9bd42be2131d",
                    "LayerId": "8ff8c2a1-d5f3-4d69-b879-909bc6139a55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "8ff8c2a1-d5f3-4d69-b879-909bc6139a55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83aec54f-60e6-4205-94a5-2e64e6b31d3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 47
}