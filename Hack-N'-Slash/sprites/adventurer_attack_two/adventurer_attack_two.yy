{
    "id": "cced7717-18f0-444b-9615-972e2507f016",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_attack_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 2,
    "bbox_right": 47,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd1c05de-4eea-46d2-a889-e6a99d7e4e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cced7717-18f0-444b-9615-972e2507f016",
            "compositeImage": {
                "id": "c48ddef6-95b5-41b6-9af8-5d831484573e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd1c05de-4eea-46d2-a889-e6a99d7e4e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b646f9-436c-4c2f-a675-31715de72b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd1c05de-4eea-46d2-a889-e6a99d7e4e2b",
                    "LayerId": "4d3d6e30-c139-461b-9850-576bc88a2cc7"
                }
            ]
        },
        {
            "id": "c15ad2a9-10ab-4372-ac83-a34d6b8625ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cced7717-18f0-444b-9615-972e2507f016",
            "compositeImage": {
                "id": "39ac7905-8b97-43e4-9e74-1adcf247135e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c15ad2a9-10ab-4372-ac83-a34d6b8625ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45992d51-981d-431b-ac6b-3dd690c402df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c15ad2a9-10ab-4372-ac83-a34d6b8625ac",
                    "LayerId": "4d3d6e30-c139-461b-9850-576bc88a2cc7"
                }
            ]
        },
        {
            "id": "042dca51-c85a-4ed7-9435-c4a869d63e9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cced7717-18f0-444b-9615-972e2507f016",
            "compositeImage": {
                "id": "f30a8953-2b77-4773-bf1f-3809ba67d223",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "042dca51-c85a-4ed7-9435-c4a869d63e9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaeb8b79-c0a7-41c2-94d5-e4b741dc6919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "042dca51-c85a-4ed7-9435-c4a869d63e9d",
                    "LayerId": "4d3d6e30-c139-461b-9850-576bc88a2cc7"
                }
            ]
        },
        {
            "id": "0b120e24-011f-4c6b-954a-32e25671308c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cced7717-18f0-444b-9615-972e2507f016",
            "compositeImage": {
                "id": "df783c18-ed5e-43c8-9a03-608d871ffac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b120e24-011f-4c6b-954a-32e25671308c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d5ab9f-4c9a-4d6a-9f54-8a78551c878a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b120e24-011f-4c6b-954a-32e25671308c",
                    "LayerId": "4d3d6e30-c139-461b-9850-576bc88a2cc7"
                }
            ]
        },
        {
            "id": "4f840172-1d85-4473-93bb-477c8f0b350f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cced7717-18f0-444b-9615-972e2507f016",
            "compositeImage": {
                "id": "cb91a930-fdcb-4145-8026-e3ce49d0589d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f840172-1d85-4473-93bb-477c8f0b350f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af245fc5-0346-4944-81e4-bbfd1ec9a6ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f840172-1d85-4473-93bb-477c8f0b350f",
                    "LayerId": "4d3d6e30-c139-461b-9850-576bc88a2cc7"
                }
            ]
        },
        {
            "id": "a207790c-2dbe-4fb5-94e7-06353475edac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cced7717-18f0-444b-9615-972e2507f016",
            "compositeImage": {
                "id": "44df6c94-a2ff-49f5-b6a8-d2e971b40ba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a207790c-2dbe-4fb5-94e7-06353475edac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9968faf-ab01-4bc6-83cf-e04b3ffc247c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a207790c-2dbe-4fb5-94e7-06353475edac",
                    "LayerId": "4d3d6e30-c139-461b-9850-576bc88a2cc7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "4d3d6e30-c139-461b-9850-576bc88a2cc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cced7717-18f0-444b-9615-972e2507f016",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 24,
    "yorig": 36
}