{
    "id": "da3289b7-099f-48c1-9b0f-5d2444bda660",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "game_over_text",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 0,
    "bbox_right": 457,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ddc6f22-df06-418c-b7d3-3deb2f7bc679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da3289b7-099f-48c1-9b0f-5d2444bda660",
            "compositeImage": {
                "id": "5b8bb6db-a9e8-4f1c-b3ef-0b4a281c09c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ddc6f22-df06-418c-b7d3-3deb2f7bc679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e01c6a91-41e6-480e-8d68-4efbc31a2bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ddc6f22-df06-418c-b7d3-3deb2f7bc679",
                    "LayerId": "31834b27-e55e-40d0-a590-2b909e5903d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 142,
    "layers": [
        {
            "id": "31834b27-e55e-40d0-a590-2b909e5903d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da3289b7-099f-48c1-9b0f-5d2444bda660",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 458,
    "xorig": 0,
    "yorig": 0
}