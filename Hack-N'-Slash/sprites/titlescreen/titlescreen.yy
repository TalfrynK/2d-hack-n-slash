{
    "id": "5cfaba52-ce11-4917-bbbc-8be1e2628418",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "titlescreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d338f7cd-8284-4263-a7af-27600d871bb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cfaba52-ce11-4917-bbbc-8be1e2628418",
            "compositeImage": {
                "id": "49ae3911-08b3-405b-a8d7-c6ca82f07fdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d338f7cd-8284-4263-a7af-27600d871bb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c30e36fc-3eb7-4e32-b925-bbc4f4a4a784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d338f7cd-8284-4263-a7af-27600d871bb4",
                    "LayerId": "3f397e19-3f11-43b4-a923-97bab01b157b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "3f397e19-3f11-43b4-a923-97bab01b157b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cfaba52-ce11-4917-bbbc-8be1e2628418",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}