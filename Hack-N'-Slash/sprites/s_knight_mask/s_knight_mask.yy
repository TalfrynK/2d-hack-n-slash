{
    "id": "a58e0eb3-fa55-4677-bd1f-01f853c370b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3616e65f-53fd-4d4c-9c1f-dd3e720546b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a58e0eb3-fa55-4677-bd1f-01f853c370b6",
            "compositeImage": {
                "id": "062bfe47-6d7a-4845-94d4-29bca6731f80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3616e65f-53fd-4d4c-9c1f-dd3e720546b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce7dad3-b26a-410a-8770-2249a9312ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3616e65f-53fd-4d4c-9c1f-dd3e720546b7",
                    "LayerId": "2633558b-4f23-4dbe-893f-67b10ee45aa2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "2633558b-4f23-4dbe-893f-67b10ee45aa2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a58e0eb3-fa55-4677-bd1f-01f853c370b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 38
}