{
    "id": "4d44ca2f-bc39-4bdd-a289-cb704c788227",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeleton_attack_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 3,
    "bbox_right": 42,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fdba602-60c7-46e4-a575-0ac9427a9092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d44ca2f-bc39-4bdd-a289-cb704c788227",
            "compositeImage": {
                "id": "130abd94-17f1-4066-a2b7-63d90d8e8880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fdba602-60c7-46e4-a575-0ac9427a9092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27cb805e-af73-4357-8e47-add20770d268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fdba602-60c7-46e4-a575-0ac9427a9092",
                    "LayerId": "da5a4676-40ee-4766-adec-7a7b65fc9f51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "da5a4676-40ee-4766-adec-7a7b65fc9f51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d44ca2f-bc39-4bdd-a289-cb704c788227",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 43,
    "xorig": 21,
    "yorig": 36
}