{
    "id": "9cce59c1-200c-400f-b0e1-8ae7e5ee1d6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_damaged",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 15,
    "bbox_right": 36,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed788dd0-3f11-446f-9072-fc22a0921297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cce59c1-200c-400f-b0e1-8ae7e5ee1d6b",
            "compositeImage": {
                "id": "9068e182-4aa1-4006-88f1-dd4d3e595b2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed788dd0-3f11-446f-9072-fc22a0921297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aebf43d3-d026-44b3-9cbc-7f0f653d5ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed788dd0-3f11-446f-9072-fc22a0921297",
                    "LayerId": "4c5e824f-87e3-4f9c-9bcf-55fd6ab8e2e0"
                }
            ]
        },
        {
            "id": "23905297-7f7b-48bd-be71-0247abe8700e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cce59c1-200c-400f-b0e1-8ae7e5ee1d6b",
            "compositeImage": {
                "id": "0d9ded3b-a120-4317-8b10-6820c2731b75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23905297-7f7b-48bd-be71-0247abe8700e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83b8fb83-693a-4d04-bef0-22f60674823c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23905297-7f7b-48bd-be71-0247abe8700e",
                    "LayerId": "4c5e824f-87e3-4f9c-9bcf-55fd6ab8e2e0"
                }
            ]
        },
        {
            "id": "a657b1a0-1f31-4352-a1c1-db355977ae46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cce59c1-200c-400f-b0e1-8ae7e5ee1d6b",
            "compositeImage": {
                "id": "a76d7fee-b3d3-44bd-9494-a79fed2bef48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a657b1a0-1f31-4352-a1c1-db355977ae46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e84f309-8ae4-4a0a-a263-53536f7de78a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a657b1a0-1f31-4352-a1c1-db355977ae46",
                    "LayerId": "4c5e824f-87e3-4f9c-9bcf-55fd6ab8e2e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "4c5e824f-87e3-4f9c-9bcf-55fd6ab8e2e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cce59c1-200c-400f-b0e1-8ae7e5ee1d6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 36
}