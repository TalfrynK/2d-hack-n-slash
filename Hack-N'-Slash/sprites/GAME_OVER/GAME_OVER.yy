{
    "id": "94a70ded-e140-41a7-8723-0d112658e1e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "GAME_OVER",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "457ca769-fc03-42dc-b88d-1ebd79006922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94a70ded-e140-41a7-8723-0d112658e1e2",
            "compositeImage": {
                "id": "926c2b1f-7b58-46a7-98e2-f36272b55996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "457ca769-fc03-42dc-b88d-1ebd79006922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e766fd57-09ca-4612-92ce-bbd1a09e7762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "457ca769-fc03-42dc-b88d-1ebd79006922",
                    "LayerId": "3dfdf4d0-fd3d-4e44-b4ca-d9337df368d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "3dfdf4d0-fd3d-4e44-b4ca-d9337df368d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94a70ded-e140-41a7-8723-0d112658e1e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}