{
    "id": "a2d977aa-ab93-46a1-936a-c06d91e7bf3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bandit_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47a3dbf4-cc47-413c-8e0a-b696e4db1d84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d977aa-ab93-46a1-936a-c06d91e7bf3a",
            "compositeImage": {
                "id": "96cdf019-a5f3-4e00-8bad-fd79093b6a8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47a3dbf4-cc47-413c-8e0a-b696e4db1d84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0356393a-8cc1-4fe7-9a26-681243e862e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47a3dbf4-cc47-413c-8e0a-b696e4db1d84",
                    "LayerId": "d9c3ff62-de24-49ec-b871-eee0425ba6c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "d9c3ff62-de24-49ec-b871-eee0425ba6c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2d977aa-ab93-46a1-936a-c06d91e7bf3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 38
}