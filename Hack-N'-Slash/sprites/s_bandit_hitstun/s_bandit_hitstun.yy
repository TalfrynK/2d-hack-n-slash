{
    "id": "886ac5d7-2760-4f1b-8c69-d5145e8dd5e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bandit_hitstun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e3c90f4-371d-43b4-b92f-2b7860bd54c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "886ac5d7-2760-4f1b-8c69-d5145e8dd5e0",
            "compositeImage": {
                "id": "b1e6cefe-c296-4d0e-9318-7ec407ff6367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e3c90f4-371d-43b4-b92f-2b7860bd54c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff1ad23b-b42d-407b-96d7-53a1b955ab3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e3c90f4-371d-43b4-b92f-2b7860bd54c3",
                    "LayerId": "d1f04e84-9a0e-4a2c-88b8-422fdfef343c"
                }
            ]
        },
        {
            "id": "b9153900-3cd3-4849-b1b8-2848718069ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "886ac5d7-2760-4f1b-8c69-d5145e8dd5e0",
            "compositeImage": {
                "id": "49f67ee9-b004-47e7-a90b-c91d8cba9883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9153900-3cd3-4849-b1b8-2848718069ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c673cd58-7d03-44fb-bbce-9a03399a0193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9153900-3cd3-4849-b1b8-2848718069ea",
                    "LayerId": "d1f04e84-9a0e-4a2c-88b8-422fdfef343c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "d1f04e84-9a0e-4a2c-88b8-422fdfef343c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "886ac5d7-2760-4f1b-8c69-d5145e8dd5e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 45
}