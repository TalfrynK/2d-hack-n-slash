{
    "id": "010fa0e1-e6c1-4c7b-990e-f247949f20af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_attack_one_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 22,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa42a702-f416-4aa5-9b56-5afdccad80ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "010fa0e1-e6c1-4c7b-990e-f247949f20af",
            "compositeImage": {
                "id": "f9ca63c6-59a8-4ec4-9549-12536cf9eddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa42a702-f416-4aa5-9b56-5afdccad80ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f940a83-a60f-4c7f-a58f-3a930ae490a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa42a702-f416-4aa5-9b56-5afdccad80ca",
                    "LayerId": "9c47bd1f-251e-418f-8e98-5ae6cde2702a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "9c47bd1f-251e-418f-8e98-5ae6cde2702a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "010fa0e1-e6c1-4c7b-990e-f247949f20af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 24,
    "yorig": 36
}