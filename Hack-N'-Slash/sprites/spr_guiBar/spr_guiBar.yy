{
    "id": "41d791c0-5c55-4128-bf8c-b71cc4c71356",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guiBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 219,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "239781fa-8c19-4863-8109-4fb0e6b765a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d791c0-5c55-4128-bf8c-b71cc4c71356",
            "compositeImage": {
                "id": "64e1643e-2293-4be0-ac8f-3df02e7fbe90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "239781fa-8c19-4863-8109-4fb0e6b765a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b620a9d-b6e6-47d7-a477-67b1faf3c57c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "239781fa-8c19-4863-8109-4fb0e6b765a2",
                    "LayerId": "63f284ac-de00-44d1-8348-896ea50029b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "63f284ac-de00-44d1-8348-896ea50029b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41d791c0-5c55-4128-bf8c-b71cc4c71356",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 220,
    "xorig": 0,
    "yorig": 0
}