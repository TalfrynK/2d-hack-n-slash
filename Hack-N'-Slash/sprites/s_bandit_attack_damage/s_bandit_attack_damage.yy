{
    "id": "317694e2-4565-4a5b-944b-50c485d0286b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bandit_attack_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 11,
    "bbox_right": 60,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a32f520-8578-4c01-96c9-d4c4106c8b07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "317694e2-4565-4a5b-944b-50c485d0286b",
            "compositeImage": {
                "id": "e794bec1-9de7-42b7-84cd-455c3d262bdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a32f520-8578-4c01-96c9-d4c4106c8b07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d46d908-53f8-4ec6-ae32-25e04d641fe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a32f520-8578-4c01-96c9-d4c4106c8b07",
                    "LayerId": "1d33b70f-809b-42d5-bed8-762db1a687d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "1d33b70f-809b-42d5-bed8-762db1a687d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "317694e2-4565-4a5b-944b-50c485d0286b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 47
}