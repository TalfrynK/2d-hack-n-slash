{
    "id": "df220f2e-d34a-4618-a1bd-8fc1303ba3b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_gameover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b692125c-eef0-4f19-bb98-6e5a6b12278e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df220f2e-d34a-4618-a1bd-8fc1303ba3b4",
            "compositeImage": {
                "id": "1e92dd10-4d31-4a2f-84c8-a196f91cbdbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b692125c-eef0-4f19-bb98-6e5a6b12278e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91d80f2e-0bcb-47ae-990d-652649c4634b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b692125c-eef0-4f19-bb98-6e5a6b12278e",
                    "LayerId": "ef071a35-a950-4179-acb4-66e74dc9e0d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "ef071a35-a950-4179-acb4-66e74dc9e0d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df220f2e-d34a-4618-a1bd-8fc1303ba3b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}