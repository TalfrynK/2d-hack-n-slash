{
    "id": "2a37e23a-9217-405a-8b15-aedadceb6fa2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 16,
    "bbox_right": 39,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ffb21d5-3097-4f9c-89dd-842382531aae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a37e23a-9217-405a-8b15-aedadceb6fa2",
            "compositeImage": {
                "id": "0c976d8d-fb12-4e97-a48e-40683139d0e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ffb21d5-3097-4f9c-89dd-842382531aae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3683b34b-e590-421f-aba5-e9bbe79bffff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ffb21d5-3097-4f9c-89dd-842382531aae",
                    "LayerId": "0cba0744-c4d7-4c64-b62e-10c6366ab2bb"
                }
            ]
        },
        {
            "id": "801c76c1-b356-4b61-aa68-07962d309711",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a37e23a-9217-405a-8b15-aedadceb6fa2",
            "compositeImage": {
                "id": "1ee01706-1f10-46d7-a9ec-afb9d98f7623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "801c76c1-b356-4b61-aa68-07962d309711",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac187254-26c8-4f7e-b9d8-0b5bd88d2974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "801c76c1-b356-4b61-aa68-07962d309711",
                    "LayerId": "0cba0744-c4d7-4c64-b62e-10c6366ab2bb"
                }
            ]
        },
        {
            "id": "3309684c-ea0c-434c-b3ab-222bc7090deb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a37e23a-9217-405a-8b15-aedadceb6fa2",
            "compositeImage": {
                "id": "9fdc13a7-1a81-4d77-9d19-01ef5d7f39ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3309684c-ea0c-434c-b3ab-222bc7090deb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6edf9946-56ae-4d57-acd9-40978bad4114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3309684c-ea0c-434c-b3ab-222bc7090deb",
                    "LayerId": "0cba0744-c4d7-4c64-b62e-10c6366ab2bb"
                }
            ]
        },
        {
            "id": "f6ecd56d-8823-4629-9202-a998e681204c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a37e23a-9217-405a-8b15-aedadceb6fa2",
            "compositeImage": {
                "id": "1c0a4d4c-9278-4484-9613-609d4371a4e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ecd56d-8823-4629-9202-a998e681204c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69a90aec-239a-4be7-a1a9-5a95b564fa12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ecd56d-8823-4629-9202-a998e681204c",
                    "LayerId": "0cba0744-c4d7-4c64-b62e-10c6366ab2bb"
                }
            ]
        },
        {
            "id": "a2dd4530-9532-4e9a-8a5e-b6438799bcb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a37e23a-9217-405a-8b15-aedadceb6fa2",
            "compositeImage": {
                "id": "cfce0667-526b-4c20-aadd-284744b0810c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2dd4530-9532-4e9a-8a5e-b6438799bcb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87729f7a-f76d-4233-b836-38551ceaba37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2dd4530-9532-4e9a-8a5e-b6438799bcb4",
                    "LayerId": "0cba0744-c4d7-4c64-b62e-10c6366ab2bb"
                }
            ]
        },
        {
            "id": "81e59274-43ab-485f-9786-ca15daf2f90b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a37e23a-9217-405a-8b15-aedadceb6fa2",
            "compositeImage": {
                "id": "af066cad-486c-4f50-9acc-f27004f69d34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e59274-43ab-485f-9786-ca15daf2f90b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9932a399-5b77-4602-85c1-bdebe23c37d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e59274-43ab-485f-9786-ca15daf2f90b",
                    "LayerId": "0cba0744-c4d7-4c64-b62e-10c6366ab2bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "0cba0744-c4d7-4c64-b62e-10c6366ab2bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a37e23a-9217-405a-8b15-aedadceb6fa2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 24,
    "yorig": 36
}