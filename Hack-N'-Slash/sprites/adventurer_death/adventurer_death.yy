{
    "id": "2c3c4b1a-39b8-474a-a0cf-7cce15e3a4b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 15,
    "bbox_right": 36,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71c9b8c0-f18f-435b-a10d-39f7de706553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3c4b1a-39b8-474a-a0cf-7cce15e3a4b8",
            "compositeImage": {
                "id": "92266d57-94be-4960-9595-bb82947a6587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71c9b8c0-f18f-435b-a10d-39f7de706553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5984956e-9597-4ac0-8e46-913819596b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c9b8c0-f18f-435b-a10d-39f7de706553",
                    "LayerId": "600e6951-0147-4500-a60a-32a20671da06"
                }
            ]
        },
        {
            "id": "6a3c2697-738f-4129-ac29-2b5b45dd18fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3c4b1a-39b8-474a-a0cf-7cce15e3a4b8",
            "compositeImage": {
                "id": "867b0337-d601-497c-862a-c5cf835ff76b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3c2697-738f-4129-ac29-2b5b45dd18fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e21454-9b7d-416a-8e1f-81a5220b9f52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3c2697-738f-4129-ac29-2b5b45dd18fa",
                    "LayerId": "600e6951-0147-4500-a60a-32a20671da06"
                }
            ]
        },
        {
            "id": "ae2cf82a-c416-48d6-b4be-87326b20fa79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3c4b1a-39b8-474a-a0cf-7cce15e3a4b8",
            "compositeImage": {
                "id": "a7bc0587-89a0-4d0a-a00b-2c945c2ca039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae2cf82a-c416-48d6-b4be-87326b20fa79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ba01fe-2ebf-4b99-a8a5-f6e28638d406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae2cf82a-c416-48d6-b4be-87326b20fa79",
                    "LayerId": "600e6951-0147-4500-a60a-32a20671da06"
                }
            ]
        },
        {
            "id": "3fce9d14-d1b0-4779-8f2b-92b410480c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3c4b1a-39b8-474a-a0cf-7cce15e3a4b8",
            "compositeImage": {
                "id": "ee4e41fe-cdd4-4051-91f3-f1f27685041b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fce9d14-d1b0-4779-8f2b-92b410480c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23fe83f3-c396-4886-9fda-11e5d3bda0c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fce9d14-d1b0-4779-8f2b-92b410480c2e",
                    "LayerId": "600e6951-0147-4500-a60a-32a20671da06"
                }
            ]
        },
        {
            "id": "0460a6dd-35bc-449b-a747-c9b1769d4e91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3c4b1a-39b8-474a-a0cf-7cce15e3a4b8",
            "compositeImage": {
                "id": "dd5be0f7-f35d-4057-b86a-5c15e522951f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0460a6dd-35bc-449b-a747-c9b1769d4e91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4fe78df-f085-4884-ba70-e4cba1199570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0460a6dd-35bc-449b-a747-c9b1769d4e91",
                    "LayerId": "600e6951-0147-4500-a60a-32a20671da06"
                }
            ]
        },
        {
            "id": "554b533b-a5f1-44a8-8033-ac713974648c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3c4b1a-39b8-474a-a0cf-7cce15e3a4b8",
            "compositeImage": {
                "id": "01383d51-1e3f-4c96-b474-36ed4be7b75c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554b533b-a5f1-44a8-8033-ac713974648c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5369d222-dab4-4a04-a4a3-5a297f840fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554b533b-a5f1-44a8-8033-ac713974648c",
                    "LayerId": "600e6951-0147-4500-a60a-32a20671da06"
                }
            ]
        },
        {
            "id": "c40e9625-d7d3-4265-b3fa-6784c419593f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c3c4b1a-39b8-474a-a0cf-7cce15e3a4b8",
            "compositeImage": {
                "id": "52a04622-e1f6-4b60-8bd1-b6878d85e4d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c40e9625-d7d3-4265-b3fa-6784c419593f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce70ce5-d3c6-42ea-ad7d-0823aa824313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c40e9625-d7d3-4265-b3fa-6784c419593f",
                    "LayerId": "600e6951-0147-4500-a60a-32a20671da06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "600e6951-0147-4500-a60a-32a20671da06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c3c4b1a-39b8-474a-a0cf-7cce15e3a4b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 36
}