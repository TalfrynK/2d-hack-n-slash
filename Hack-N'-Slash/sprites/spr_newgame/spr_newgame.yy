{
    "id": "c28a075f-adaf-4339-858b-f08f28cd4dd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_newgame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 27,
    "bbox_right": 474,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac2ea45e-506d-4659-a086-2d5d1e396034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c28a075f-adaf-4339-858b-f08f28cd4dd9",
            "compositeImage": {
                "id": "4ea709ca-9a1a-43fd-aa79-1245d483735c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac2ea45e-506d-4659-a086-2d5d1e396034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72c5bbd6-29e7-4a60-b042-cf4d05e195a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac2ea45e-506d-4659-a086-2d5d1e396034",
                    "LayerId": "1b46f9eb-f0d1-478c-aa71-85bcb4464477"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "1b46f9eb-f0d1-478c-aa71-85bcb4464477",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c28a075f-adaf-4339-858b-f08f28cd4dd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}