{
    "id": "a8b54c71-d830-419f-b3c8-5c6d75ebcabf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bk_Sky",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 831,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f35c6f4f-79e8-4a43-a688-7205d1a4f6a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8b54c71-d830-419f-b3c8-5c6d75ebcabf",
            "compositeImage": {
                "id": "24ff1d07-556d-420b-a82d-547b1ac349b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f35c6f4f-79e8-4a43-a688-7205d1a4f6a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8390ff0-bc72-4e97-8f64-02116836cd06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f35c6f4f-79e8-4a43-a688-7205d1a4f6a6",
                    "LayerId": "970ae6bd-896a-4e6d-8c15-aec22c74c99a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 832,
    "layers": [
        {
            "id": "970ae6bd-896a-4e6d-8c15-aec22c74c99a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8b54c71-d830-419f-b3c8-5c6d75ebcabf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}