{
    "id": "16094447-41df-4298-b8a9-73d14977775e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cyberpunk_street",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 607,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1687f826-0dc8-40ac-a62c-d9afede68a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16094447-41df-4298-b8a9-73d14977775e",
            "compositeImage": {
                "id": "9b894b0c-70c9-4014-8d3a-52147934f326",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1687f826-0dc8-40ac-a62c-d9afede68a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10335b90-33dc-4594-bfe5-6347fd5c5bdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1687f826-0dc8-40ac-a62c-d9afede68a9b",
                    "LayerId": "e4341ea4-7ba9-46d5-b144-66077c8e039a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "e4341ea4-7ba9-46d5-b144-66077c8e039a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16094447-41df-4298-b8a9-73d14977775e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 608,
    "xorig": 0,
    "yorig": 0
}