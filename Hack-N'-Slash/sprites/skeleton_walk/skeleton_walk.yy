{
    "id": "16439abe-3621-4266-b44e-828d5a4c73a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeleton_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bfb1954-a62d-4036-bcc1-e9d2ac086e26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "450dee0f-4e14-44d5-a604-14c96058e35b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bfb1954-a62d-4036-bcc1-e9d2ac086e26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ede0139e-0f59-4623-a8f4-074c293c434e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bfb1954-a62d-4036-bcc1-e9d2ac086e26",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "89d316a3-344d-433b-ba27-d2f7ef9d4df3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "52c545d1-cde4-4b4b-8730-42736b55462e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d316a3-344d-433b-ba27-d2f7ef9d4df3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0708bca3-25f5-4dfd-957c-edb98f68e7c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d316a3-344d-433b-ba27-d2f7ef9d4df3",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "fe685cb1-9583-4fcc-8589-1ed0025c91ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "8f40c6c1-2a0b-49e3-b395-7b6c9cb75f92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe685cb1-9583-4fcc-8589-1ed0025c91ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b45d542-3941-40c2-87e6-e9a71509f4a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe685cb1-9583-4fcc-8589-1ed0025c91ae",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "3d65de5b-7353-4304-9373-09dda49cc58a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "6f8ae5f4-5b2f-4611-a2fd-ad980b1f617f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d65de5b-7353-4304-9373-09dda49cc58a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f8cb45-a3bb-4f6e-963c-a466f7b8be6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d65de5b-7353-4304-9373-09dda49cc58a",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "0c766476-a57b-47dd-afa1-6b60f02baa44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "48eeee8c-25d9-4060-997d-86732ec85803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c766476-a57b-47dd-afa1-6b60f02baa44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1590fe79-038b-4c13-a784-b2c6aef57a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c766476-a57b-47dd-afa1-6b60f02baa44",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "44132edb-f9ec-42da-9f12-fbe83333f062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "42541bfa-b57a-4496-b941-e9664b575161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44132edb-f9ec-42da-9f12-fbe83333f062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39dc4c37-abaf-439f-915f-0132b2b473a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44132edb-f9ec-42da-9f12-fbe83333f062",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "2c92aef5-4b3b-4a99-a0c1-b4302403e4c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "ac367b4a-6612-450a-8f71-c9493ddcd1b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c92aef5-4b3b-4a99-a0c1-b4302403e4c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26a79543-a033-4ac1-a817-3a7cc808e48c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c92aef5-4b3b-4a99-a0c1-b4302403e4c7",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "ef6772e8-c31c-4827-ae49-6564200e4556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "8c40a9c7-5a2e-4e1d-b8f1-01f740e93f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef6772e8-c31c-4827-ae49-6564200e4556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d028a1-3c50-458c-b8fa-c24d3588b6ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef6772e8-c31c-4827-ae49-6564200e4556",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "6e79b183-d6e3-4b4b-a6ea-bbfb8d26d7a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "d33fde06-9064-4749-8118-9158b171906b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e79b183-d6e3-4b4b-a6ea-bbfb8d26d7a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b61939b-5e42-455c-9db5-0ef242c62e4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e79b183-d6e3-4b4b-a6ea-bbfb8d26d7a3",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "9623263b-1e17-4fdf-877a-a69ff0611b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "67655a0f-b1c4-4be4-ace4-d4466a50b8e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9623263b-1e17-4fdf-877a-a69ff0611b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e85bc7d-bea3-4ab8-a27c-9972eeb27b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9623263b-1e17-4fdf-877a-a69ff0611b83",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "4bf66590-509f-410e-99f5-1e67b82f6dc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "dca02f11-a17d-446f-9fa0-e80720d76166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bf66590-509f-410e-99f5-1e67b82f6dc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19011ac4-7ec4-4a90-b409-16d7c35bd0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bf66590-509f-410e-99f5-1e67b82f6dc4",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "238a836c-5896-461d-9fde-639cd1812d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "349f8b1e-7dcc-4667-b721-fb46abed2aef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "238a836c-5896-461d-9fde-639cd1812d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1916133-172c-4e4a-bd4a-d5e65ad3c1af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "238a836c-5896-461d-9fde-639cd1812d6e",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        },
        {
            "id": "f4119b9b-ebde-4f95-841a-45060388889c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "compositeImage": {
                "id": "c66b0b0c-5821-4b37-b911-56e708a38951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4119b9b-ebde-4f95-841a-45060388889c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7388d640-1e9b-4b57-8a67-fbf282eb52fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4119b9b-ebde-4f95-841a-45060388889c",
                    "LayerId": "317ed819-eea2-4e97-98b5-6a5934dfaf87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "317ed819-eea2-4e97-98b5-6a5934dfaf87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16439abe-3621-4266-b44e-828d5a4c73a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 10,
    "yorig": 32
}