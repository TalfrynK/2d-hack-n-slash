{
    "id": "be05eaef-cb83-4e16-b626-7479d287ace3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "back_buildings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8225c37f-7e1f-4b60-b137-1ee14baa325b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be05eaef-cb83-4e16-b626-7479d287ace3",
            "compositeImage": {
                "id": "c4f1c5ec-cc02-4586-bc6d-9ecbedbc0717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8225c37f-7e1f-4b60-b137-1ee14baa325b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "524d350b-39ae-455b-b423-d4d3ed5b345d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8225c37f-7e1f-4b60-b137-1ee14baa325b",
                    "LayerId": "3876f9e2-5710-436f-9a75-8313f33f75a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "3876f9e2-5710-436f-9a75-8313f33f75a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be05eaef-cb83-4e16-b626-7479d287ace3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}