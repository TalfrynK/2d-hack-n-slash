{
    "id": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 7,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee23fb3c-90fc-477b-a4cf-75e50d05fa21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
            "compositeImage": {
                "id": "e73d58b5-5231-4b5d-8411-0e53a78866ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee23fb3c-90fc-477b-a4cf-75e50d05fa21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c845e171-3b16-4220-bb9e-38245c33b363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee23fb3c-90fc-477b-a4cf-75e50d05fa21",
                    "LayerId": "ffec453a-9461-4438-8fee-0a6cc92ed524"
                }
            ]
        },
        {
            "id": "7ed2ccf3-b5ac-43d3-a964-147b44067730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
            "compositeImage": {
                "id": "d47b763d-96d6-48ca-83ca-e0b1ad92057d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ed2ccf3-b5ac-43d3-a964-147b44067730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4b7b763-92c3-410f-b893-1d987a67a2dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ed2ccf3-b5ac-43d3-a964-147b44067730",
                    "LayerId": "ffec453a-9461-4438-8fee-0a6cc92ed524"
                }
            ]
        },
        {
            "id": "cc909f8e-1221-4ca4-a02d-2d5d85e159c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
            "compositeImage": {
                "id": "b90f5b44-c7c0-4a70-8219-2883d987c21d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc909f8e-1221-4ca4-a02d-2d5d85e159c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "094eba2a-fb66-47d2-8b30-9767b7f60b40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc909f8e-1221-4ca4-a02d-2d5d85e159c3",
                    "LayerId": "ffec453a-9461-4438-8fee-0a6cc92ed524"
                }
            ]
        },
        {
            "id": "c8dc5ef6-d72a-4293-8589-aa50a822c41b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
            "compositeImage": {
                "id": "f6ed3920-30d5-45d8-8931-3574a7036f68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8dc5ef6-d72a-4293-8589-aa50a822c41b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "328d65bb-0244-4fec-b347-7ae1925f9ca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8dc5ef6-d72a-4293-8589-aa50a822c41b",
                    "LayerId": "ffec453a-9461-4438-8fee-0a6cc92ed524"
                }
            ]
        },
        {
            "id": "021e223b-c370-4da5-b9a1-e0b060d6aa45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
            "compositeImage": {
                "id": "b1e4e668-1ac8-4925-bf72-13b4c7d9692c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "021e223b-c370-4da5-b9a1-e0b060d6aa45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c4cc12d-bc6a-4b42-8813-eb8fe5ac7fad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021e223b-c370-4da5-b9a1-e0b060d6aa45",
                    "LayerId": "ffec453a-9461-4438-8fee-0a6cc92ed524"
                }
            ]
        },
        {
            "id": "dd879dc8-fae4-4b54-ba95-d85f3b99d5d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
            "compositeImage": {
                "id": "7752e986-3ff7-4393-8951-7b4592fa2ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd879dc8-fae4-4b54-ba95-d85f3b99d5d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fc39b01-b801-4a28-a2a5-473f04cda0e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd879dc8-fae4-4b54-ba95-d85f3b99d5d5",
                    "LayerId": "ffec453a-9461-4438-8fee-0a6cc92ed524"
                }
            ]
        },
        {
            "id": "5f2354de-a49a-42c0-90fc-ad2f093bfbcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
            "compositeImage": {
                "id": "3914cc85-f125-4576-a559-d096901ea449",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f2354de-a49a-42c0-90fc-ad2f093bfbcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0f27d0e-5507-4544-af78-470b38a12869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f2354de-a49a-42c0-90fc-ad2f093bfbcb",
                    "LayerId": "ffec453a-9461-4438-8fee-0a6cc92ed524"
                }
            ]
        },
        {
            "id": "417d72b2-80dc-4b45-915a-db411a98757c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
            "compositeImage": {
                "id": "f5bc88b1-1896-472b-b720-b4c9efe3366e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417d72b2-80dc-4b45-915a-db411a98757c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5436077b-14d0-49d8-b85e-4e5132107213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417d72b2-80dc-4b45-915a-db411a98757c",
                    "LayerId": "ffec453a-9461-4438-8fee-0a6cc92ed524"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "ffec453a-9461-4438-8fee-0a6cc92ed524",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2b23ba7-5cc0-4684-9bfd-769c3fa19288",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 45
}