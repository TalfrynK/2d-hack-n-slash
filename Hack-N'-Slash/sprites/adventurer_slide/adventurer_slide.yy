{
    "id": "f3b226e6-a602-4fff-afa8-a802cb2ab981",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_slide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 5,
    "bbox_right": 38,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4db83229-c353-494c-95b6-6ebc1f0e2ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b226e6-a602-4fff-afa8-a802cb2ab981",
            "compositeImage": {
                "id": "150b81cc-ec57-4695-beea-bec71c58554f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db83229-c353-494c-95b6-6ebc1f0e2ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42a6a96-219a-48cb-aa25-8da0f4341a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db83229-c353-494c-95b6-6ebc1f0e2ff1",
                    "LayerId": "8a0cbd26-9be3-489b-9c34-df7cd53b6b46"
                }
            ]
        },
        {
            "id": "24c4d001-87b9-4f0d-a530-83dfee9905b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b226e6-a602-4fff-afa8-a802cb2ab981",
            "compositeImage": {
                "id": "63eb0589-623a-4f9b-8623-19ee33c7a2df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24c4d001-87b9-4f0d-a530-83dfee9905b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf46f42-3978-4e58-aa12-b700cc83a6b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24c4d001-87b9-4f0d-a530-83dfee9905b8",
                    "LayerId": "8a0cbd26-9be3-489b-9c34-df7cd53b6b46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "8a0cbd26-9be3-489b-9c34-df7cd53b6b46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3b226e6-a602-4fff-afa8-a802cb2ab981",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 36
}