{
    "id": "96b57bcb-1c41-44de-bb48-795697f0a473",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeleton_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fcfc169c-12ea-46c8-838d-439b2b711033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "3292276b-25ff-453a-8586-425520f10033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcfc169c-12ea-46c8-838d-439b2b711033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5b456fc-2aa0-4f1f-b074-18d7e7fa2b51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcfc169c-12ea-46c8-838d-439b2b711033",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "d586862c-2db7-47f2-8c5e-38207d60feb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "52583584-f20b-4507-b6ec-c1f3fa814fd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d586862c-2db7-47f2-8c5e-38207d60feb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3044d249-cf0e-4b8a-b677-561cb38cb7e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d586862c-2db7-47f2-8c5e-38207d60feb9",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "9192b078-340e-4843-b874-429b5c56257f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "2fb97879-925b-4545-8503-fb8955533bdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9192b078-340e-4843-b874-429b5c56257f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fbd022d-46a3-4d2c-9f60-407c2cf8cab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9192b078-340e-4843-b874-429b5c56257f",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "53948ced-e6b3-4ed0-9655-c1c7e64e1765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "78eb37c0-61b3-496e-a7c3-a78ef997370f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53948ced-e6b3-4ed0-9655-c1c7e64e1765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f157eb7-e04b-4f99-aeca-22c908203811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53948ced-e6b3-4ed0-9655-c1c7e64e1765",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "b3467b33-f57e-4955-a051-213892a95b39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "a7825ef3-e404-4f71-b541-40e8ca526111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3467b33-f57e-4955-a051-213892a95b39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faece815-7895-4eb1-ba41-784584e635e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3467b33-f57e-4955-a051-213892a95b39",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "bf4e6466-08f1-450e-b857-e36289f10e7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "0cd86271-b396-48cf-a85c-31232a961b78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf4e6466-08f1-450e-b857-e36289f10e7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1634e70b-76c8-4f1b-aaa9-f84e6b067fd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf4e6466-08f1-450e-b857-e36289f10e7e",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "ff9e3cb2-d4f1-49ea-aaba-70a933fc151c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "2ed97c06-b6e0-430d-9433-e3f3c47a1978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff9e3cb2-d4f1-49ea-aaba-70a933fc151c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9731a60-8db7-486f-adce-d5aa554ebc86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff9e3cb2-d4f1-49ea-aaba-70a933fc151c",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "5911f56f-3e12-4954-a099-20f1295bd42a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "012cc379-3ffe-41dc-b069-cba62b80be00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5911f56f-3e12-4954-a099-20f1295bd42a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40448c83-54a4-40e9-9ce0-613750fe78d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5911f56f-3e12-4954-a099-20f1295bd42a",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "2991db60-3b94-421c-bdf2-edc1ac054502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "c521cb72-f9e8-440d-bb62-c624a8674c74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2991db60-3b94-421c-bdf2-edc1ac054502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1ab638d-7bdc-4da5-8f11-687a51908985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2991db60-3b94-421c-bdf2-edc1ac054502",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "6b2bea55-ee8f-48ea-a252-f716a8517e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "42d311f5-f52f-45c2-a8a5-bc411b2aacde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b2bea55-ee8f-48ea-a252-f716a8517e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89af776c-fbe4-43ee-827d-20d6a3687180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b2bea55-ee8f-48ea-a252-f716a8517e82",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "03c9e61c-2884-420d-b95a-4ecfa4ff9b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "8c625ed1-516b-4082-866b-b38369e74c8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03c9e61c-2884-420d-b95a-4ecfa4ff9b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a84e7864-9e35-4184-ba56-38572c215a4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c9e61c-2884-420d-b95a-4ecfa4ff9b0d",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "c8af35e9-ef31-4bcf-b6f6-872eb264383c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "04a62735-37ce-4e68-b0fc-bd9f153a940d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8af35e9-ef31-4bcf-b6f6-872eb264383c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b851efa0-f143-49ad-be7b-1eef87a1d402",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8af35e9-ef31-4bcf-b6f6-872eb264383c",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "06c39faa-c137-495f-a3f9-aa6c2d131d32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "44f1a93c-079a-4b22-bb13-6ad93c47bace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06c39faa-c137-495f-a3f9-aa6c2d131d32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf3458d-1bac-4fc1-9ee7-4235167f7aa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06c39faa-c137-495f-a3f9-aa6c2d131d32",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "4f31466a-74e1-423c-86fb-7cb0622aba2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "fc58265c-4a81-4fde-8ea6-c6fe948ee247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f31466a-74e1-423c-86fb-7cb0622aba2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c9d7416-d599-4174-a3a5-b413f1dc70b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f31466a-74e1-423c-86fb-7cb0622aba2f",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "9355cd68-02af-468a-a32f-b040b0a863e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "627c9cd1-b68b-4d1c-b4e4-db98d679fb3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9355cd68-02af-468a-a32f-b040b0a863e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9830add-075d-49f1-948d-56934b30706b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9355cd68-02af-468a-a32f-b040b0a863e7",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "8329d50d-8f0e-4dca-930e-db3eb38554c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "27117811-c864-48f0-8719-9c8c48bcbbe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8329d50d-8f0e-4dca-930e-db3eb38554c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b0b30e-2fd8-4609-a2da-7a5a11db67c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8329d50d-8f0e-4dca-930e-db3eb38554c3",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "2d169282-09f9-47d7-9a66-23af6ace7886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "642b033a-615a-4c39-bbe6-9e3d31c15736",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d169282-09f9-47d7-9a66-23af6ace7886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "178e4f73-c057-4178-9125-76974e3eefcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d169282-09f9-47d7-9a66-23af6ace7886",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        },
        {
            "id": "e1dbd3a4-92b1-483c-a229-a7b51b150557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "compositeImage": {
                "id": "0733fa52-a8ab-4c23-8438-dfa4a29355d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1dbd3a4-92b1-483c-a229-a7b51b150557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef20154-c5f7-4622-a7c5-0201aafea490",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1dbd3a4-92b1-483c-a229-a7b51b150557",
                    "LayerId": "0f45b36f-6955-41be-9951-c80e4de59f8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "0f45b36f-6955-41be-9951-c80e4de59f8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96b57bcb-1c41-44de-bb48-795697f0a473",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 43,
    "xorig": 17,
    "yorig": 36
}