{
    "id": "812cafa6-952f-41e6-9c49-dbf5a25cab33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ts_Decor",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 90,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b9609b9-99f9-4156-ba66-0775382bc3ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "812cafa6-952f-41e6-9c49-dbf5a25cab33",
            "compositeImage": {
                "id": "ad21c5e9-a908-4534-b1e1-ee95c092deec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b9609b9-99f9-4156-ba66-0775382bc3ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01249068-fecd-4869-835f-b80cc70489dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b9609b9-99f9-4156-ba66-0775382bc3ef",
                    "LayerId": "beb41bd6-431f-4971-adc2-1ccf4c33052b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "beb41bd6-431f-4971-adc2-1ccf4c33052b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "812cafa6-952f-41e6-9c49-dbf5a25cab33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}