{
    "id": "327fe24e-1aa4-410f-b736-c9f00bd2922b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_attack_three_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 2,
    "bbox_right": 49,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8c307cf-ace6-426a-bead-0f121fc7f4a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "327fe24e-1aa4-410f-b736-c9f00bd2922b",
            "compositeImage": {
                "id": "392fbd9c-e314-47bc-a24c-a9c175b96580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8c307cf-ace6-426a-bead-0f121fc7f4a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81755b69-53c7-41e1-9647-38510501edbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8c307cf-ace6-426a-bead-0f121fc7f4a7",
                    "LayerId": "eac588c2-9dd2-4bb7-be32-dc03473537bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "eac588c2-9dd2-4bb7-be32-dc03473537bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "327fe24e-1aa4-410f-b736-c9f00bd2922b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 24,
    "yorig": 36
}