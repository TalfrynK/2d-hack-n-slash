{
    "id": "5bc13a57-2c01-4cd9-acbf-c9159113f78d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_attack_one",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 7,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1d44046-f556-45ff-9fd9-870b93fa8155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bc13a57-2c01-4cd9-acbf-c9159113f78d",
            "compositeImage": {
                "id": "f84d5d54-20f1-46cf-a19f-9a486c4fc1f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1d44046-f556-45ff-9fd9-870b93fa8155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4191245-6511-4bd5-9526-8daab52a00a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1d44046-f556-45ff-9fd9-870b93fa8155",
                    "LayerId": "2a1b1ee2-8796-4da1-90b7-28d34aee859e"
                }
            ]
        },
        {
            "id": "4f1d12b9-c5dc-4fcd-a619-ac6db069980d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bc13a57-2c01-4cd9-acbf-c9159113f78d",
            "compositeImage": {
                "id": "69218f1d-e7ab-4fa3-8159-73c0a259a596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f1d12b9-c5dc-4fcd-a619-ac6db069980d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e597385-8bd4-49b2-ae77-545319194c1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1d12b9-c5dc-4fcd-a619-ac6db069980d",
                    "LayerId": "2a1b1ee2-8796-4da1-90b7-28d34aee859e"
                }
            ]
        },
        {
            "id": "c5ba5a0f-58c9-4fad-bc65-3b63a688dce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bc13a57-2c01-4cd9-acbf-c9159113f78d",
            "compositeImage": {
                "id": "d7e1920f-0dcf-40b9-b090-089c48faa5c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5ba5a0f-58c9-4fad-bc65-3b63a688dce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af3afd20-f9ea-4f38-8782-64d12527a596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5ba5a0f-58c9-4fad-bc65-3b63a688dce6",
                    "LayerId": "2a1b1ee2-8796-4da1-90b7-28d34aee859e"
                }
            ]
        },
        {
            "id": "0067ef2b-9142-4206-8a65-6766e0b45d02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bc13a57-2c01-4cd9-acbf-c9159113f78d",
            "compositeImage": {
                "id": "077e52fe-400b-4838-a0b3-af2d1e07cc50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0067ef2b-9142-4206-8a65-6766e0b45d02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e1bd69e-57fa-480d-a139-350364538310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0067ef2b-9142-4206-8a65-6766e0b45d02",
                    "LayerId": "2a1b1ee2-8796-4da1-90b7-28d34aee859e"
                }
            ]
        },
        {
            "id": "44103b67-5847-4e02-b732-0bbfa8fc7ae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bc13a57-2c01-4cd9-acbf-c9159113f78d",
            "compositeImage": {
                "id": "5fdb5841-4eee-44b4-bc02-d81457b80067",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44103b67-5847-4e02-b732-0bbfa8fc7ae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b8a0108-2ce4-4680-bb3a-fd11755c188a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44103b67-5847-4e02-b732-0bbfa8fc7ae0",
                    "LayerId": "2a1b1ee2-8796-4da1-90b7-28d34aee859e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "2a1b1ee2-8796-4da1-90b7-28d34aee859e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bc13a57-2c01-4cd9-acbf-c9159113f78d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 24,
    "yorig": 36
}