{
    "id": "8c21da2e-4775-42b5-b11f-d8415ced414a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_Coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 12,
    "bbox_right": 21,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e57fc5cd-3695-4831-9936-1345312cb2b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "compositeImage": {
                "id": "37034311-6aa1-46af-a80f-e3025ae1b254",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e57fc5cd-3695-4831-9936-1345312cb2b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66a846a9-d908-432e-bb44-7ab3c9a1e603",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e57fc5cd-3695-4831-9936-1345312cb2b4",
                    "LayerId": "acc57336-c924-4678-ac93-b8b4a7fdbdb1"
                }
            ]
        },
        {
            "id": "33c2685e-0057-42bd-8448-34e32b7fe968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "compositeImage": {
                "id": "a16605cf-47f0-4c6f-b685-0299e3933f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c2685e-0057-42bd-8448-34e32b7fe968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0ff1120-64d0-4e85-b938-53fd8eb92b67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c2685e-0057-42bd-8448-34e32b7fe968",
                    "LayerId": "acc57336-c924-4678-ac93-b8b4a7fdbdb1"
                }
            ]
        },
        {
            "id": "8006ac48-f42b-41f0-9e7f-78b8d3ec58cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "compositeImage": {
                "id": "ff849a9a-7d39-43e7-a8c0-f0f9425fefaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8006ac48-f42b-41f0-9e7f-78b8d3ec58cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42dc3744-0d64-4905-8f19-9db532341718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8006ac48-f42b-41f0-9e7f-78b8d3ec58cb",
                    "LayerId": "acc57336-c924-4678-ac93-b8b4a7fdbdb1"
                }
            ]
        },
        {
            "id": "bdfd0077-83fd-4d19-b1df-e6f80c2336b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "compositeImage": {
                "id": "19c3a4c8-50f8-4d6b-8175-4cb0cae0e61e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdfd0077-83fd-4d19-b1df-e6f80c2336b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16c87c6b-2dbb-4309-be1c-6927e44ea73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdfd0077-83fd-4d19-b1df-e6f80c2336b6",
                    "LayerId": "acc57336-c924-4678-ac93-b8b4a7fdbdb1"
                }
            ]
        },
        {
            "id": "77bc3a58-cd27-47ab-8a10-684dbdfafcb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "compositeImage": {
                "id": "60ac03d2-c495-4c88-aa03-d5577512c3df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77bc3a58-cd27-47ab-8a10-684dbdfafcb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1222ab87-ad24-4146-8514-efb3fb65f5de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77bc3a58-cd27-47ab-8a10-684dbdfafcb9",
                    "LayerId": "acc57336-c924-4678-ac93-b8b4a7fdbdb1"
                }
            ]
        },
        {
            "id": "d5789ef3-94eb-46b0-8764-c2cfe3b23847",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "compositeImage": {
                "id": "8bea62c1-a964-47f8-9d93-b808e06385bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5789ef3-94eb-46b0-8764-c2cfe3b23847",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31e22b8e-28ab-4b86-b0cf-9eda48db05ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5789ef3-94eb-46b0-8764-c2cfe3b23847",
                    "LayerId": "acc57336-c924-4678-ac93-b8b4a7fdbdb1"
                }
            ]
        },
        {
            "id": "76ca02a2-c6fa-4a1c-b5a8-2ac6fa3c2df1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "compositeImage": {
                "id": "7d5a8516-b8b7-4d7d-b92b-367a64afef9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ca02a2-c6fa-4a1c-b5a8-2ac6fa3c2df1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cbebaef-d864-405e-a952-f2dc5517ebc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ca02a2-c6fa-4a1c-b5a8-2ac6fa3c2df1",
                    "LayerId": "acc57336-c924-4678-ac93-b8b4a7fdbdb1"
                }
            ]
        },
        {
            "id": "43901d2d-a581-43b8-9a55-487230c9741c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "compositeImage": {
                "id": "c9361056-e579-47b8-8ea9-58134856eeb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43901d2d-a581-43b8-9a55-487230c9741c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13c1e071-df19-4a0f-99fc-9bffaf416760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43901d2d-a581-43b8-9a55-487230c9741c",
                    "LayerId": "acc57336-c924-4678-ac93-b8b4a7fdbdb1"
                }
            ]
        },
        {
            "id": "e9f22f6e-17af-4cf7-8105-4ac32231635b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "compositeImage": {
                "id": "71e6d830-adea-42a3-9179-d72f1dc03bbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f22f6e-17af-4cf7-8105-4ac32231635b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fab8c98-4b11-46d1-8650-12f6de8e513c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f22f6e-17af-4cf7-8105-4ac32231635b",
                    "LayerId": "acc57336-c924-4678-ac93-b8b4a7fdbdb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "acc57336-c924-4678-ac93-b8b4a7fdbdb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c21da2e-4775-42b5-b11f-d8415ced414a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}