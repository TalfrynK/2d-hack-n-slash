{
    "id": "dc000485-f78c-4171-8f85-ee6409cd87e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "foreground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 351,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8505351c-ecc3-4a04-be06-353e53ffc4c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc000485-f78c-4171-8f85-ee6409cd87e4",
            "compositeImage": {
                "id": "15511c55-4d04-4277-953f-881b149049aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8505351c-ecc3-4a04-be06-353e53ffc4c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "152a5348-725f-46bd-9962-e15f26099d29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8505351c-ecc3-4a04-be06-353e53ffc4c3",
                    "LayerId": "79716b68-5bda-4313-a819-b637ff9f9d8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "79716b68-5bda-4313-a819-b637ff9f9d8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc000485-f78c-4171-8f85-ee6409cd87e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 352,
    "xorig": 0,
    "yorig": 0
}