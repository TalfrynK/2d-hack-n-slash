{
    "id": "f9b7b4d2-4012-4b5c-b605-59fa6e149eae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeleton_reaction",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2880a2ee-b821-4d89-8a79-3d0fc2753e0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9b7b4d2-4012-4b5c-b605-59fa6e149eae",
            "compositeImage": {
                "id": "74618000-e789-4134-8260-7517007f4c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2880a2ee-b821-4d89-8a79-3d0fc2753e0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c95d93-92cf-45df-a668-8dcec8c31c39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2880a2ee-b821-4d89-8a79-3d0fc2753e0f",
                    "LayerId": "a0c70c22-e28e-44b4-86fb-7a5635952147"
                }
            ]
        },
        {
            "id": "fea8cbb3-ee7b-499f-ba8d-e32144c6435e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9b7b4d2-4012-4b5c-b605-59fa6e149eae",
            "compositeImage": {
                "id": "f75efcd2-1402-4fb2-9c0f-776185c28c0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fea8cbb3-ee7b-499f-ba8d-e32144c6435e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b47ab6d9-62e1-40a3-bb73-b512bf6a01d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fea8cbb3-ee7b-499f-ba8d-e32144c6435e",
                    "LayerId": "a0c70c22-e28e-44b4-86fb-7a5635952147"
                }
            ]
        },
        {
            "id": "d754da53-4179-4831-8342-e6eb98c7a434",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9b7b4d2-4012-4b5c-b605-59fa6e149eae",
            "compositeImage": {
                "id": "a1c4e306-bd59-478a-bfe7-042f87b3fd04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d754da53-4179-4831-8342-e6eb98c7a434",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc4a552c-d82c-4655-99e4-7c5a2cfdbece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d754da53-4179-4831-8342-e6eb98c7a434",
                    "LayerId": "a0c70c22-e28e-44b4-86fb-7a5635952147"
                }
            ]
        },
        {
            "id": "bb4e169e-5806-43ae-87cd-92c9d114214a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9b7b4d2-4012-4b5c-b605-59fa6e149eae",
            "compositeImage": {
                "id": "0a4b1bd2-9257-4748-bf64-e6c1ed5af4c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb4e169e-5806-43ae-87cd-92c9d114214a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1b4cfa6-7018-4976-a0e8-984c90ace654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb4e169e-5806-43ae-87cd-92c9d114214a",
                    "LayerId": "a0c70c22-e28e-44b4-86fb-7a5635952147"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a0c70c22-e28e-44b4-86fb-7a5635952147",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9b7b4d2-4012-4b5c-b605-59fa6e149eae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 31
}