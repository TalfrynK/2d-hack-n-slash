{
    "id": "90811f66-5dff-484a-8c20-09e74c125ae1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ts_Objects",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 121,
    "bbox_left": 0,
    "bbox_right": 125,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "033a711a-168a-4cc6-b70e-5eb5bea3e87a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90811f66-5dff-484a-8c20-09e74c125ae1",
            "compositeImage": {
                "id": "d02d55c1-f9e1-49fd-ba15-9ff9c025b084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "033a711a-168a-4cc6-b70e-5eb5bea3e87a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f743582-42a7-4a24-a2be-321ef362350e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033a711a-168a-4cc6-b70e-5eb5bea3e87a",
                    "LayerId": "3628dbb8-ce04-47ca-a0db-15a3d5805f04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3628dbb8-ce04-47ca-a0db-15a3d5805f04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90811f66-5dff-484a-8c20-09e74c125ae1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}