{
    "id": "c36eb9f3-99c1-4619-b74f-34b88b231a7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_attack_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 11,
    "bbox_right": 60,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "526e9e4f-37eb-4a31-abfa-11fe50dfcfd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c36eb9f3-99c1-4619-b74f-34b88b231a7b",
            "compositeImage": {
                "id": "de334c90-302d-4378-b4fa-9be88a0ba8f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "526e9e4f-37eb-4a31-abfa-11fe50dfcfd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8561300-8c10-4bcf-b772-a3e8cd1d4363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "526e9e4f-37eb-4a31-abfa-11fe50dfcfd6",
                    "LayerId": "f3d08f3a-48f1-41d4-9646-d24091efbd05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f3d08f3a-48f1-41d4-9646-d24091efbd05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c36eb9f3-99c1-4619-b74f-34b88b231a7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 47
}