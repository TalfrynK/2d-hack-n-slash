{
    "id": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeleton_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e5de695-0c82-4125-b60f-26f93a7e7a23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
            "compositeImage": {
                "id": "0376fea5-4697-4614-8f15-aa709bce59b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e5de695-0c82-4125-b60f-26f93a7e7a23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84780a21-9b30-4043-8de4-b76ed392de63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e5de695-0c82-4125-b60f-26f93a7e7a23",
                    "LayerId": "d416c508-de5a-4473-b09b-6ea0dae9ab2b"
                }
            ]
        },
        {
            "id": "4abb2943-3b53-4782-96a9-4b94ab96e8f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
            "compositeImage": {
                "id": "396c558a-eb98-4553-847b-1a9ad74afab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4abb2943-3b53-4782-96a9-4b94ab96e8f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c3021f-f61f-4789-8942-53dbfd8e6703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4abb2943-3b53-4782-96a9-4b94ab96e8f0",
                    "LayerId": "d416c508-de5a-4473-b09b-6ea0dae9ab2b"
                }
            ]
        },
        {
            "id": "1d1a9dc4-dce7-4a0f-a9fd-94bc90a192b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
            "compositeImage": {
                "id": "5a0e9c35-05e2-42e2-99b9-dd20245bc1d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d1a9dc4-dce7-4a0f-a9fd-94bc90a192b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ee3aa83-d805-4cac-babb-fd93ac145a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d1a9dc4-dce7-4a0f-a9fd-94bc90a192b5",
                    "LayerId": "d416c508-de5a-4473-b09b-6ea0dae9ab2b"
                }
            ]
        },
        {
            "id": "80b88c58-7585-4557-924b-131f856e6d86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
            "compositeImage": {
                "id": "b0dc6408-892c-495e-8336-129b8c5c2179",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b88c58-7585-4557-924b-131f856e6d86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e922f5a6-ac13-43cb-9c19-11cbef8d2011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b88c58-7585-4557-924b-131f856e6d86",
                    "LayerId": "d416c508-de5a-4473-b09b-6ea0dae9ab2b"
                }
            ]
        },
        {
            "id": "b11243b3-ec2f-4d90-83a8-91ab6736812c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
            "compositeImage": {
                "id": "2aa833ed-8f5c-46d5-89c6-5178ed7d9899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b11243b3-ec2f-4d90-83a8-91ab6736812c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a427b1d9-aeba-4b6b-b812-b85ba7902c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11243b3-ec2f-4d90-83a8-91ab6736812c",
                    "LayerId": "d416c508-de5a-4473-b09b-6ea0dae9ab2b"
                }
            ]
        },
        {
            "id": "f334e06f-f602-4701-8c49-1222cc039fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
            "compositeImage": {
                "id": "96182058-3557-442e-b80a-d7d128c1d864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f334e06f-f602-4701-8c49-1222cc039fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54976ed0-1644-4877-9423-58b4649b9cb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f334e06f-f602-4701-8c49-1222cc039fde",
                    "LayerId": "d416c508-de5a-4473-b09b-6ea0dae9ab2b"
                }
            ]
        },
        {
            "id": "2cab74cc-5322-451f-a25c-32ad159d6d0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
            "compositeImage": {
                "id": "593df250-132d-45f9-9b9b-91354aca00c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cab74cc-5322-451f-a25c-32ad159d6d0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3951cd4b-9855-4080-a077-9acfc2fb8e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cab74cc-5322-451f-a25c-32ad159d6d0d",
                    "LayerId": "d416c508-de5a-4473-b09b-6ea0dae9ab2b"
                }
            ]
        },
        {
            "id": "13372325-366c-49b8-9178-418c4cd6b74f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
            "compositeImage": {
                "id": "39f1edf6-6f3b-4955-880c-7dc05fd5f4f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13372325-366c-49b8-9178-418c4cd6b74f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e773f643-61cb-414a-846d-dfa907e14356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13372325-366c-49b8-9178-418c4cd6b74f",
                    "LayerId": "d416c508-de5a-4473-b09b-6ea0dae9ab2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d416c508-de5a-4473-b09b-6ea0dae9ab2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11c12ea9-11e6-4b75-af78-4ae911eb59d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 31
}