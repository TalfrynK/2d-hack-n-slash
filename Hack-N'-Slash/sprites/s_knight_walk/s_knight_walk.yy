{
    "id": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 5,
    "bbox_right": 38,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "517b14a8-8288-467b-8167-cc0385ecf85a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
            "compositeImage": {
                "id": "63b0ba23-68a8-4b66-843e-58e8e382be07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "517b14a8-8288-467b-8167-cc0385ecf85a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a19df1-29cf-4d8f-b6f4-d0ed8a366425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "517b14a8-8288-467b-8167-cc0385ecf85a",
                    "LayerId": "37ba333e-270a-4cb0-8b89-516fcb280832"
                }
            ]
        },
        {
            "id": "df212c8b-db38-4fd8-a1f5-8b81a6c26e49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
            "compositeImage": {
                "id": "b8d4a445-0d49-4700-840d-909db214262e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df212c8b-db38-4fd8-a1f5-8b81a6c26e49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d9ef4d7-3295-4d13-85ba-a06394c0cb09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df212c8b-db38-4fd8-a1f5-8b81a6c26e49",
                    "LayerId": "37ba333e-270a-4cb0-8b89-516fcb280832"
                }
            ]
        },
        {
            "id": "9936861c-6a03-465a-bb07-e082f64d886a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
            "compositeImage": {
                "id": "dbda23a6-5ebb-4476-8447-c01501473760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9936861c-6a03-465a-bb07-e082f64d886a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37facee2-d075-4ac6-b8ae-f3c940e3b371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9936861c-6a03-465a-bb07-e082f64d886a",
                    "LayerId": "37ba333e-270a-4cb0-8b89-516fcb280832"
                }
            ]
        },
        {
            "id": "5e362d77-69cb-410c-8c2a-cbe917dcfd18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
            "compositeImage": {
                "id": "814ccc46-a137-428b-86f4-8c3c06bf17e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e362d77-69cb-410c-8c2a-cbe917dcfd18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc15882-e7e2-4116-a324-3414c0e4d386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e362d77-69cb-410c-8c2a-cbe917dcfd18",
                    "LayerId": "37ba333e-270a-4cb0-8b89-516fcb280832"
                }
            ]
        },
        {
            "id": "ab625b9b-4aa5-4a3c-a1b7-313ae6bb20a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
            "compositeImage": {
                "id": "471894b4-3f21-4ba0-8be6-4ebf2e312d84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab625b9b-4aa5-4a3c-a1b7-313ae6bb20a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e50a13e6-5781-4da0-b0dd-b2c2614af932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab625b9b-4aa5-4a3c-a1b7-313ae6bb20a1",
                    "LayerId": "37ba333e-270a-4cb0-8b89-516fcb280832"
                }
            ]
        },
        {
            "id": "fd3188fa-4c3a-45f7-bc2b-bba4a29855da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
            "compositeImage": {
                "id": "88cab58b-bd56-48e2-abbe-f7cf7bbe7be0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd3188fa-4c3a-45f7-bc2b-bba4a29855da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80f608a6-b88b-40f7-8070-f8d22611c279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3188fa-4c3a-45f7-bc2b-bba4a29855da",
                    "LayerId": "37ba333e-270a-4cb0-8b89-516fcb280832"
                }
            ]
        },
        {
            "id": "a51074a9-d319-4d82-bd14-675a0ccc48e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
            "compositeImage": {
                "id": "68f41231-e0cf-42b5-9c4e-6db8a1dce18d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a51074a9-d319-4d82-bd14-675a0ccc48e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d118bed-09c0-43ad-9923-1462f67054f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a51074a9-d319-4d82-bd14-675a0ccc48e0",
                    "LayerId": "37ba333e-270a-4cb0-8b89-516fcb280832"
                }
            ]
        },
        {
            "id": "6c446efb-461f-41d4-93d4-a6e6532f7152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
            "compositeImage": {
                "id": "2188a380-225f-4ac3-8da0-6b0120604cb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c446efb-461f-41d4-93d4-a6e6532f7152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dde59415-174e-4aa6-9e3f-d8380e3ed26a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c446efb-461f-41d4-93d4-a6e6532f7152",
                    "LayerId": "37ba333e-270a-4cb0-8b89-516fcb280832"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "37ba333e-270a-4cb0-8b89-516fcb280832",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "198ea8c4-306c-4ec7-8f1f-5a71e98b4128",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 45
}