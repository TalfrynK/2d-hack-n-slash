{
    "id": "e869eb62-2899-4879-af28-50359a317da5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Game_Over_1_",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cb0a5c7-b946-4396-b8f9-305261e65755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e869eb62-2899-4879-af28-50359a317da5",
            "compositeImage": {
                "id": "ffb3f92a-af7f-47bd-a696-7435dafc7fbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cb0a5c7-b946-4396-b8f9-305261e65755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "006d0fc7-4f10-4873-8607-3b822053f749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cb0a5c7-b946-4396-b8f9-305261e65755",
                    "LayerId": "4e3f6cd9-c2ab-4baf-ad98-0df902ac023b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "4e3f6cd9-c2ab-4baf-ad98-0df902ac023b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e869eb62-2899-4879-af28-50359a317da5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}