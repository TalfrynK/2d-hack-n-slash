{
    "id": "64345cd0-751f-45ca-aaac-43525e7b5d12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "play_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 67,
    "bbox_right": 444,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5e6c560-615b-44a7-87ce-03200abe0f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64345cd0-751f-45ca-aaac-43525e7b5d12",
            "compositeImage": {
                "id": "1a0400e3-8cca-4297-a8b6-2d05b600de66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5e6c560-615b-44a7-87ce-03200abe0f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1827f3d0-9e8d-4565-ab09-f8b05e3cb5a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5e6c560-615b-44a7-87ce-03200abe0f11",
                    "LayerId": "2eb44d0e-7f91-4cbe-ab0b-7a1d74d3f4bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "2eb44d0e-7f91-4cbe-ab0b-7a1d74d3f4bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64345cd0-751f-45ca-aaac-43525e7b5d12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}