{
    "id": "29f66918-0f47-4736-a35e-b230f6e9bf6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_healthbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3bf5c39-0045-4ef4-b55c-a57001f28fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29f66918-0f47-4736-a35e-b230f6e9bf6b",
            "compositeImage": {
                "id": "6e3e5334-5334-405c-8d0f-381762ce6a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3bf5c39-0045-4ef4-b55c-a57001f28fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1bfca67-7b4a-4e54-a324-24890ba2d640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3bf5c39-0045-4ef4-b55c-a57001f28fac",
                    "LayerId": "b35d217f-c872-4d3c-8d43-d5de99a93f42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "b35d217f-c872-4d3c-8d43-d5de99a93f42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29f66918-0f47-4736-a35e-b230f6e9bf6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}