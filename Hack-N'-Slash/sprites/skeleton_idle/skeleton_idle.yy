{
    "id": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skeleton_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6be34683-28ed-4b47-b729-20fd632ce80b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "8352b779-12d1-4694-bc17-b8605a06dd47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6be34683-28ed-4b47-b729-20fd632ce80b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e555b16f-168a-478b-b9ef-d618f113a5ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6be34683-28ed-4b47-b729-20fd632ce80b",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "82c91388-632e-4eec-9e4b-545d7f92732b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "f6af0a4b-b975-417d-85ca-03f31e8d2d32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c91388-632e-4eec-9e4b-545d7f92732b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef8c42bc-4df9-495e-9daf-8b85bac3701e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c91388-632e-4eec-9e4b-545d7f92732b",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "1ec50ff3-83d0-4c5a-b6d7-d8d633c1f5f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "6ad95701-61d3-4b8b-992c-b0fc02754171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec50ff3-83d0-4c5a-b6d7-d8d633c1f5f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b86c6aba-c0e6-42fb-b06c-18479c661c38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec50ff3-83d0-4c5a-b6d7-d8d633c1f5f9",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "9c8c3e16-7d75-49fe-93f4-7e083ef399ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "3b045b21-06e8-4fd7-be96-e4315b571f54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c8c3e16-7d75-49fe-93f4-7e083ef399ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98a98bb1-fad3-4028-a1bd-71da87ba3bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8c3e16-7d75-49fe-93f4-7e083ef399ef",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "9f5ffe6f-8bae-469f-8033-458f92c55328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "19bdcb29-dc8f-479d-b5b2-f4ae08f22dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f5ffe6f-8bae-469f-8033-458f92c55328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc9ed85a-e8ab-4fff-bd56-e2340d7c4588",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f5ffe6f-8bae-469f-8033-458f92c55328",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "cd14a8e8-3ab6-4d10-a520-e52945e361b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "04812031-6715-4f50-95f3-79d7fbe7648d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd14a8e8-3ab6-4d10-a520-e52945e361b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80a7a79d-8972-45e7-af6e-c48f46a7ea3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd14a8e8-3ab6-4d10-a520-e52945e361b7",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "e8c639fd-8a47-405b-9a1d-edd0a4065917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "0f61d456-59a9-4d68-ac4a-f4ae51f057a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8c639fd-8a47-405b-9a1d-edd0a4065917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44743810-02bd-426b-a6ed-5dc20ae9b952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c639fd-8a47-405b-9a1d-edd0a4065917",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "0016bec8-27c9-4362-a38b-1781462b3070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "20969ba5-118a-4c11-b1ca-6f6664aa7740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0016bec8-27c9-4362-a38b-1781462b3070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b5169c3-a2de-40e2-8319-aafe511945e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0016bec8-27c9-4362-a38b-1781462b3070",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "c8ac761f-29aa-4307-95a1-44dd1a2206d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "f614d51a-09a0-477e-a866-310ee98fcc2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8ac761f-29aa-4307-95a1-44dd1a2206d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea63f9bd-f481-4134-b9e9-995cb86deca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8ac761f-29aa-4307-95a1-44dd1a2206d6",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "d8b016c9-6bd1-4407-80ef-f7ff429fdc15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "3bf3c817-ee88-43a7-a7e6-5d015be7e26f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b016c9-6bd1-4407-80ef-f7ff429fdc15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0b34554-ccd8-4259-9320-07fc76a45742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b016c9-6bd1-4407-80ef-f7ff429fdc15",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        },
        {
            "id": "6553d684-989f-49e9-9886-e19789fc240f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "compositeImage": {
                "id": "0cf21961-7589-483f-9db2-d02ce1c205df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6553d684-989f-49e9-9886-e19789fc240f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5db4135-ffce-4575-8c8b-d6a407d41a3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6553d684-989f-49e9-9886-e19789fc240f",
                    "LayerId": "41188d2f-0ed8-4487-a722-5be594d12655"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "41188d2f-0ed8-4487-a722-5be594d12655",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ecf5612-12c5-4459-9e8b-e85c103479aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 10,
    "yorig": 31
}