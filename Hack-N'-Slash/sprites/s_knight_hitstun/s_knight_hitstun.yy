{
    "id": "726fce0d-1b49-4000-b5b8-305b1d359282",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_hitstun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90e1ffdc-26f2-474e-8f46-8b3bff0437a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "726fce0d-1b49-4000-b5b8-305b1d359282",
            "compositeImage": {
                "id": "87ed49ee-3371-4e70-ba57-faaad5f4a797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90e1ffdc-26f2-474e-8f46-8b3bff0437a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d9f5a0d-f9da-4eaf-933f-c12655a8170d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90e1ffdc-26f2-474e-8f46-8b3bff0437a3",
                    "LayerId": "4d2f2052-e4a0-4ff2-b915-9ca351b3cd4c"
                }
            ]
        },
        {
            "id": "1d4b467e-9de7-4899-8aac-3dfe61c06170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "726fce0d-1b49-4000-b5b8-305b1d359282",
            "compositeImage": {
                "id": "62249600-427e-4825-9559-d515b928e1fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d4b467e-9de7-4899-8aac-3dfe61c06170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb164e09-3a69-4716-b7d3-fd20da4234ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d4b467e-9de7-4899-8aac-3dfe61c06170",
                    "LayerId": "4d2f2052-e4a0-4ff2-b915-9ca351b3cd4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "4d2f2052-e4a0-4ff2-b915-9ca351b3cd4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "726fce0d-1b49-4000-b5b8-305b1d359282",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 46
}