{
    "id": "3b9ba07d-3684-4fc8-8332-8d596556646a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bandit_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 4,
    "bbox_right": 38,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b988dea1-3141-4a1c-932f-3255d90a7922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9ba07d-3684-4fc8-8332-8d596556646a",
            "compositeImage": {
                "id": "0bc3013b-053a-487a-901e-0d85a1adc39c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b988dea1-3141-4a1c-932f-3255d90a7922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ace50b0-71b4-4c45-b627-1801a5cabe31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b988dea1-3141-4a1c-932f-3255d90a7922",
                    "LayerId": "db29c694-074e-49fd-a1a5-a248168d36ab"
                }
            ]
        },
        {
            "id": "1cbb4746-c2eb-4c8a-9740-74f45c40784f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9ba07d-3684-4fc8-8332-8d596556646a",
            "compositeImage": {
                "id": "e0919a60-d44a-456e-b7f4-9fc8f5da6445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cbb4746-c2eb-4c8a-9740-74f45c40784f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "719892dc-ef20-4eef-8799-52e3ba8a997c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cbb4746-c2eb-4c8a-9740-74f45c40784f",
                    "LayerId": "db29c694-074e-49fd-a1a5-a248168d36ab"
                }
            ]
        },
        {
            "id": "8e9453ca-25a6-4ba9-9187-8b81111ea01a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9ba07d-3684-4fc8-8332-8d596556646a",
            "compositeImage": {
                "id": "9d4f0c61-0dd4-4a4e-8f74-9216efb79fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e9453ca-25a6-4ba9-9187-8b81111ea01a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b2912a6-d284-4f66-9abf-a1984b9acb4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e9453ca-25a6-4ba9-9187-8b81111ea01a",
                    "LayerId": "db29c694-074e-49fd-a1a5-a248168d36ab"
                }
            ]
        },
        {
            "id": "20263c2f-8c50-462f-b5ab-8eb0d68be2d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9ba07d-3684-4fc8-8332-8d596556646a",
            "compositeImage": {
                "id": "381408d3-1fa2-4e78-9a30-57c4d15468be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20263c2f-8c50-462f-b5ab-8eb0d68be2d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69a6fb1c-472e-4bc6-baf7-52aba93dff1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20263c2f-8c50-462f-b5ab-8eb0d68be2d9",
                    "LayerId": "db29c694-074e-49fd-a1a5-a248168d36ab"
                }
            ]
        },
        {
            "id": "de41730d-9058-4115-9d98-b6122f7b5801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9ba07d-3684-4fc8-8332-8d596556646a",
            "compositeImage": {
                "id": "242d36a0-0f5b-4b0c-92ca-78f51c7ecce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de41730d-9058-4115-9d98-b6122f7b5801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b8a882-fdc6-4963-ae44-2f585682769f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de41730d-9058-4115-9d98-b6122f7b5801",
                    "LayerId": "db29c694-074e-49fd-a1a5-a248168d36ab"
                }
            ]
        },
        {
            "id": "27f9b7ed-d458-4b4a-a30c-3ed32d5eab84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9ba07d-3684-4fc8-8332-8d596556646a",
            "compositeImage": {
                "id": "32853be6-ebcf-4ccb-81d0-9d567925df31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27f9b7ed-d458-4b4a-a30c-3ed32d5eab84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9fcff6-82d5-4f02-bec7-23241856d620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27f9b7ed-d458-4b4a-a30c-3ed32d5eab84",
                    "LayerId": "db29c694-074e-49fd-a1a5-a248168d36ab"
                }
            ]
        },
        {
            "id": "dfd2b905-3ad9-4195-acb1-3be8ffa86e1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9ba07d-3684-4fc8-8332-8d596556646a",
            "compositeImage": {
                "id": "41464754-3b9e-4904-9633-7b6366e9a698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfd2b905-3ad9-4195-acb1-3be8ffa86e1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e010b34-090f-4bde-8dee-6fe106ece223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfd2b905-3ad9-4195-acb1-3be8ffa86e1b",
                    "LayerId": "db29c694-074e-49fd-a1a5-a248168d36ab"
                }
            ]
        },
        {
            "id": "3df7852e-6686-4442-9e4c-4d2bb0cecd84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9ba07d-3684-4fc8-8332-8d596556646a",
            "compositeImage": {
                "id": "b513e280-fb5f-4b82-8643-c3fbf32637f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3df7852e-6686-4442-9e4c-4d2bb0cecd84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "278adf22-cea6-4b86-9dad-0cc57ca4653e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3df7852e-6686-4442-9e4c-4d2bb0cecd84",
                    "LayerId": "db29c694-074e-49fd-a1a5-a248168d36ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "db29c694-074e-49fd-a1a5-a248168d36ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b9ba07d-3684-4fc8-8332-8d596556646a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 45
}