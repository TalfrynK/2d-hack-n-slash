{
    "id": "3c98880b-4db8-4f0d-841e-655b69d4a2fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_play",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 27,
    "bbox_right": 474,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21f6427d-faeb-48ea-9413-b369f17f132d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c98880b-4db8-4f0d-841e-655b69d4a2fc",
            "compositeImage": {
                "id": "de979f80-ba75-4f75-b9fa-230ffab80bba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f6427d-faeb-48ea-9413-b369f17f132d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b321986e-9381-462d-964b-f5190cffa039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f6427d-faeb-48ea-9413-b369f17f132d",
                    "LayerId": "8c14e34b-61da-49b0-b0ba-13f86bf078ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "8c14e34b-61da-49b0-b0ba-13f86bf078ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c98880b-4db8-4f0d-841e-655b69d4a2fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}