{
    "id": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_die",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35f5640a-764a-4b20-8c66-1f02267eb7fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "compositeImage": {
                "id": "97914e49-e203-4dfa-810d-28178b37c3ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35f5640a-764a-4b20-8c66-1f02267eb7fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee6b2b4b-a88e-468f-bdf1-c0ccff6bb3d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35f5640a-764a-4b20-8c66-1f02267eb7fd",
                    "LayerId": "0667c022-0be9-4924-8075-7a9f8fff23ce"
                }
            ]
        },
        {
            "id": "72d1b317-3794-410b-8ab6-5e156c261ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "compositeImage": {
                "id": "018b83d1-5f0e-40b5-adae-bdca2d6e6c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72d1b317-3794-410b-8ab6-5e156c261ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e362917-dcb5-43a2-b3fd-886b89336b89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72d1b317-3794-410b-8ab6-5e156c261ed6",
                    "LayerId": "0667c022-0be9-4924-8075-7a9f8fff23ce"
                }
            ]
        },
        {
            "id": "0ebf3833-8b45-41e2-bb94-01a14b27ade6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "compositeImage": {
                "id": "0dc1e3a7-97ea-43f1-be54-5d7e7b4ee46f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ebf3833-8b45-41e2-bb94-01a14b27ade6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491f618e-4a5a-466f-8740-fe5136c8c926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ebf3833-8b45-41e2-bb94-01a14b27ade6",
                    "LayerId": "0667c022-0be9-4924-8075-7a9f8fff23ce"
                }
            ]
        },
        {
            "id": "4be57c41-f2c8-4d45-ad58-f685c68d5012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "compositeImage": {
                "id": "e22d89b0-da3d-4e6c-b179-d77f16474e8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4be57c41-f2c8-4d45-ad58-f685c68d5012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0192519-b40e-4b41-806f-100c2d129c00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4be57c41-f2c8-4d45-ad58-f685c68d5012",
                    "LayerId": "0667c022-0be9-4924-8075-7a9f8fff23ce"
                }
            ]
        },
        {
            "id": "60efdc09-d534-40ba-a668-101cda1fbfa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "compositeImage": {
                "id": "65a0381f-9cc8-4542-a3b2-23d87a555e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60efdc09-d534-40ba-a668-101cda1fbfa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0a018d9-a76a-4399-a311-585bf9320366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60efdc09-d534-40ba-a668-101cda1fbfa5",
                    "LayerId": "0667c022-0be9-4924-8075-7a9f8fff23ce"
                }
            ]
        },
        {
            "id": "4f136fb9-7b2d-43cc-bb61-a8257878c4ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "compositeImage": {
                "id": "ba1af407-d7e3-4a2f-aae6-b12d5f4ba352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f136fb9-7b2d-43cc-bb61-a8257878c4ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7c9b62-6c79-4883-8f37-f91e7456cdd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f136fb9-7b2d-43cc-bb61-a8257878c4ab",
                    "LayerId": "0667c022-0be9-4924-8075-7a9f8fff23ce"
                }
            ]
        },
        {
            "id": "c03690ad-5130-4c5e-bf0b-c25ab18f3eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "compositeImage": {
                "id": "af617a2f-80d9-4ce1-9342-1a25a4e6fd18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c03690ad-5130-4c5e-bf0b-c25ab18f3eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b24ab6-0a09-4ca1-be09-bee6f6c9efc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c03690ad-5130-4c5e-bf0b-c25ab18f3eae",
                    "LayerId": "0667c022-0be9-4924-8075-7a9f8fff23ce"
                }
            ]
        },
        {
            "id": "f60cb83a-a4d6-4723-b710-df1b8f56c77f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "compositeImage": {
                "id": "cb221e47-b6a0-4bae-84f3-b15d4f07d15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60cb83a-a4d6-4723-b710-df1b8f56c77f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d4ec9c0-a5e0-4db7-916c-7df73c13e23b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60cb83a-a4d6-4723-b710-df1b8f56c77f",
                    "LayerId": "0667c022-0be9-4924-8075-7a9f8fff23ce"
                }
            ]
        },
        {
            "id": "d529172c-6000-4b94-a686-c1dff1abeb7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "compositeImage": {
                "id": "c69de08f-a0bf-4f15-b592-56ceab15cb8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d529172c-6000-4b94-a686-c1dff1abeb7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1cba06-dcaf-42c6-ad3b-67b2d0d54fb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d529172c-6000-4b94-a686-c1dff1abeb7a",
                    "LayerId": "0667c022-0be9-4924-8075-7a9f8fff23ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "0667c022-0be9-4924-8075-7a9f8fff23ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d16eb4c0-4720-4e57-b13c-3a308d20efe8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 46
}