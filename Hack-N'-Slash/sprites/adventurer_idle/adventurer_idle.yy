{
    "id": "b02653b2-5558-403d-ab37-ddeb892e750d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 9,
    "bbox_right": 33,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05f2e3a7-1293-4389-9a8e-290d5ed9bfa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b02653b2-5558-403d-ab37-ddeb892e750d",
            "compositeImage": {
                "id": "6bda7cd6-aa3c-4fb5-87ef-1543ea3a37d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05f2e3a7-1293-4389-9a8e-290d5ed9bfa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e431de3b-a2e3-4982-ae31-9528195080b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05f2e3a7-1293-4389-9a8e-290d5ed9bfa0",
                    "LayerId": "9203d1e7-0f25-46e6-90b8-ffda46db3262"
                }
            ]
        },
        {
            "id": "ab3e4af2-6fde-4e3d-bb48-9d3acaa423b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b02653b2-5558-403d-ab37-ddeb892e750d",
            "compositeImage": {
                "id": "1cae9563-8749-4d38-8432-ff2716024b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab3e4af2-6fde-4e3d-bb48-9d3acaa423b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c919bac3-a36c-45e9-9838-6ef12ad8809e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab3e4af2-6fde-4e3d-bb48-9d3acaa423b8",
                    "LayerId": "9203d1e7-0f25-46e6-90b8-ffda46db3262"
                }
            ]
        },
        {
            "id": "c48b1275-4c75-4bc7-be24-dac79e7fe38c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b02653b2-5558-403d-ab37-ddeb892e750d",
            "compositeImage": {
                "id": "932b47c5-dd51-4aa8-bfae-e19aac286522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c48b1275-4c75-4bc7-be24-dac79e7fe38c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a058580-99fa-4780-a045-dff4f3abd0e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c48b1275-4c75-4bc7-be24-dac79e7fe38c",
                    "LayerId": "9203d1e7-0f25-46e6-90b8-ffda46db3262"
                }
            ]
        },
        {
            "id": "6abd5f63-5982-4b47-a0cf-d9c24f8f8bff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b02653b2-5558-403d-ab37-ddeb892e750d",
            "compositeImage": {
                "id": "204aefd8-7848-4837-98a2-4c8bd1b481c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6abd5f63-5982-4b47-a0cf-d9c24f8f8bff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64466745-cedc-4df9-885c-d6ee5411414b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6abd5f63-5982-4b47-a0cf-d9c24f8f8bff",
                    "LayerId": "9203d1e7-0f25-46e6-90b8-ffda46db3262"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "9203d1e7-0f25-46e6-90b8-ffda46db3262",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b02653b2-5558-403d-ab37-ddeb892e750d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 36
}