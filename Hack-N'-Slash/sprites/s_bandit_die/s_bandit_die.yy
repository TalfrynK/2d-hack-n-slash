{
    "id": "04f7ca19-3397-48cb-8859-ba0bf2010913",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bandit_die",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 1,
    "bbox_right": 43,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e5d25b3-5c2c-4fcd-9ead-e4ed54914b71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "compositeImage": {
                "id": "32136b6a-5e68-481c-9ab6-6944fa5b0b45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e5d25b3-5c2c-4fcd-9ead-e4ed54914b71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7faf7da8-bf18-4df7-9dcb-c9ddcc14f52f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e5d25b3-5c2c-4fcd-9ead-e4ed54914b71",
                    "LayerId": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0"
                }
            ]
        },
        {
            "id": "ede1316b-69bf-48c7-beab-dade3a1e71a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "compositeImage": {
                "id": "59d9e221-1899-4e17-b069-245e5d1db154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ede1316b-69bf-48c7-beab-dade3a1e71a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9061d0d-56aa-44b6-a308-904d8abc2ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ede1316b-69bf-48c7-beab-dade3a1e71a4",
                    "LayerId": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0"
                }
            ]
        },
        {
            "id": "69858031-26a0-408b-8ecf-540848f799a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "compositeImage": {
                "id": "fb414ebc-8cbd-4548-bb5c-f1b66d81f3dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69858031-26a0-408b-8ecf-540848f799a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21b53efb-002b-4172-add6-6478fa32ae42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69858031-26a0-408b-8ecf-540848f799a6",
                    "LayerId": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0"
                }
            ]
        },
        {
            "id": "4704d330-2865-4e9c-a267-6b39f8101f03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "compositeImage": {
                "id": "10be749b-4bad-435d-b40b-bf4d73b2ae19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4704d330-2865-4e9c-a267-6b39f8101f03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08711f30-c239-44c2-bf7e-9ee481f5a3b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4704d330-2865-4e9c-a267-6b39f8101f03",
                    "LayerId": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0"
                }
            ]
        },
        {
            "id": "d84a7e88-cd4b-4929-b333-cfc580bcd738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "compositeImage": {
                "id": "702c412e-f872-4849-992b-8d7d47dcf0b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d84a7e88-cd4b-4929-b333-cfc580bcd738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d844ddb-ccba-40fa-9c95-25650a62f8ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d84a7e88-cd4b-4929-b333-cfc580bcd738",
                    "LayerId": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0"
                }
            ]
        },
        {
            "id": "605368a0-07b8-4c67-a8e2-4f6249883158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "compositeImage": {
                "id": "5a727da6-482d-45d3-8685-480ce06f6623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "605368a0-07b8-4c67-a8e2-4f6249883158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4a874c1-59c6-4423-b0f2-4c38bfb0fa85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "605368a0-07b8-4c67-a8e2-4f6249883158",
                    "LayerId": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0"
                }
            ]
        },
        {
            "id": "40186e22-fccf-418d-a2e1-59fa3806c4b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "compositeImage": {
                "id": "cfd0b8c1-2a52-428f-823f-e7e99258d707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40186e22-fccf-418d-a2e1-59fa3806c4b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9be7d3b4-9404-4e3e-9b5a-519bd94738c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40186e22-fccf-418d-a2e1-59fa3806c4b4",
                    "LayerId": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0"
                }
            ]
        },
        {
            "id": "a7ef1919-9948-4a3b-8e90-4b96a9f60c74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "compositeImage": {
                "id": "8062e972-4b12-4403-b3ee-f909d80cf207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ef1919-9948-4a3b-8e90-4b96a9f60c74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45597a8-deb5-4de9-a73e-30e03b32fa76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ef1919-9948-4a3b-8e90-4b96a9f60c74",
                    "LayerId": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0"
                }
            ]
        },
        {
            "id": "3d5070cb-30dd-49b6-9cef-84cd84fa07fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "compositeImage": {
                "id": "4bf03dc4-1d0d-4535-8c51-b05a4b660c2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d5070cb-30dd-49b6-9cef-84cd84fa07fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe1f1e9d-c493-472d-bc32-a10e7cf445d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5070cb-30dd-49b6-9cef-84cd84fa07fb",
                    "LayerId": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "308c9bd4-e9dd-4032-907c-f11d81dd5cd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04f7ca19-3397-48cb-8859-ba0bf2010913",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 45
}