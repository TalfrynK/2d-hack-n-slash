{
    "id": "869ed739-5cff-4d70-9db4-34e9bf9807d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bandit_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f825c8c1-fe12-41cf-944a-0173892463e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869ed739-5cff-4d70-9db4-34e9bf9807d5",
            "compositeImage": {
                "id": "e1fb2adb-832c-4b8f-9c1f-d4114f82a9fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f825c8c1-fe12-41cf-944a-0173892463e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ca1bc0-509c-472a-8b42-7754766f7459",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f825c8c1-fe12-41cf-944a-0173892463e2",
                    "LayerId": "d675d6a0-0d2c-49cb-97be-ad299907e3ce"
                }
            ]
        },
        {
            "id": "12fec25a-8545-4257-a663-782136bb3bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869ed739-5cff-4d70-9db4-34e9bf9807d5",
            "compositeImage": {
                "id": "6a4bfc6e-2eba-4f25-86ea-06f2f3e886b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12fec25a-8545-4257-a663-782136bb3bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ad88798-2bcc-41bc-9d40-6616a10eef4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12fec25a-8545-4257-a663-782136bb3bc7",
                    "LayerId": "d675d6a0-0d2c-49cb-97be-ad299907e3ce"
                }
            ]
        },
        {
            "id": "1007ee25-6cf5-47a1-8de0-b6cf7e07ff03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869ed739-5cff-4d70-9db4-34e9bf9807d5",
            "compositeImage": {
                "id": "74a20623-f4b2-4f79-9748-d68a24329669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1007ee25-6cf5-47a1-8de0-b6cf7e07ff03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e256b647-b73d-4d8d-b487-33d9d9334880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1007ee25-6cf5-47a1-8de0-b6cf7e07ff03",
                    "LayerId": "d675d6a0-0d2c-49cb-97be-ad299907e3ce"
                }
            ]
        },
        {
            "id": "52a35334-f414-442c-941d-6c530d0c2596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869ed739-5cff-4d70-9db4-34e9bf9807d5",
            "compositeImage": {
                "id": "aab75ce0-c9eb-4654-a5cf-0185424eee07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52a35334-f414-442c-941d-6c530d0c2596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a549a979-3783-4b3d-8b9c-c98fd48d10e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52a35334-f414-442c-941d-6c530d0c2596",
                    "LayerId": "d675d6a0-0d2c-49cb-97be-ad299907e3ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "d675d6a0-0d2c-49cb-97be-ad299907e3ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "869ed739-5cff-4d70-9db4-34e9bf9807d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 45
}