{
    "id": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_defend",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 9,
    "bbox_right": 41,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84b81efa-82bd-4740-bfc8-db4c80336976",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "9743ae6a-25f0-4a66-8059-faa83ddecfe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b81efa-82bd-4740-bfc8-db4c80336976",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9e37449-030b-433c-854f-618a7e22c598",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b81efa-82bd-4740-bfc8-db4c80336976",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "c987ae0a-ed23-4f2d-9bda-efa7003f376d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "6f3c2875-b2c3-4c62-b737-6d905b8382d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c987ae0a-ed23-4f2d-9bda-efa7003f376d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d00b4beb-7c98-488d-b95b-5e4f53713466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c987ae0a-ed23-4f2d-9bda-efa7003f376d",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "36ea4167-5687-4af8-b44b-e8e9566c5b62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "8679c0df-fdce-4d17-bbdd-5ae085b7d439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ea4167-5687-4af8-b44b-e8e9566c5b62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ee93bf8-b011-4db9-a50e-457e3ddbae64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ea4167-5687-4af8-b44b-e8e9566c5b62",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "a42db95e-7285-4184-bebb-d036c744d245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "cabdf2d8-834c-4b73-b196-fea76383491c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a42db95e-7285-4184-bebb-d036c744d245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6466a5d-b210-4af0-b71e-a5ae7c667c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a42db95e-7285-4184-bebb-d036c744d245",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "a079fdf2-eb47-4f33-aadd-2af4b35d73fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "d1bf4687-09ed-4829-a969-978cd408dd5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a079fdf2-eb47-4f33-aadd-2af4b35d73fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7d8a328-0e57-471e-afd1-2df611ea973c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a079fdf2-eb47-4f33-aadd-2af4b35d73fc",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "1b548b1a-40f4-495a-abb7-ee7b224aed91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "4c8da492-bada-436d-ad88-925a0831fc97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b548b1a-40f4-495a-abb7-ee7b224aed91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1a7ca21-8289-4d12-bf08-a6d6cd5b5cf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b548b1a-40f4-495a-abb7-ee7b224aed91",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "6b636117-b366-4c54-b006-c4d40c618222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "6ca86ab7-d335-467b-90dd-6ae30737f574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b636117-b366-4c54-b006-c4d40c618222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f5880fe-09ad-429f-aa89-d72b8f9580ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b636117-b366-4c54-b006-c4d40c618222",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "0e8bcf1f-0743-4976-965e-8bc7235c1832",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "a9cf0724-332b-4e07-aa89-0244f6bcb4ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e8bcf1f-0743-4976-965e-8bc7235c1832",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50f99949-91b1-4cea-9694-e84f705e0daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e8bcf1f-0743-4976-965e-8bc7235c1832",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "53412fba-fb7f-4999-af2e-4e3854c266ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "b4a830e9-1266-4de9-8176-d57b4a662187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53412fba-fb7f-4999-af2e-4e3854c266ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5a1f53-414c-4dc2-8f00-5adf8caa3daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53412fba-fb7f-4999-af2e-4e3854c266ed",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "27b51356-e4e9-412f-a046-7cff43624bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "2362171d-4608-4e5c-be92-c622a70120c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27b51356-e4e9-412f-a046-7cff43624bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c09f8b2e-395d-4083-a5a6-86697db338b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27b51356-e4e9-412f-a046-7cff43624bb1",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "020c8118-c899-45c6-a3ef-dad2561fb1cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "5e49eb9e-55ff-4f22-bc33-2ca13e5cbb96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "020c8118-c899-45c6-a3ef-dad2561fb1cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae575fe4-2a56-43ed-9a6c-84aa725cf739",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "020c8118-c899-45c6-a3ef-dad2561fb1cf",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "429f294c-aaa3-4de0-9c5d-0d00b2e2d69e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "ff8033d2-d880-4b9e-bab0-db49d238091e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "429f294c-aaa3-4de0-9c5d-0d00b2e2d69e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "476ae2d0-9492-4dac-9bb4-6e9912975265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "429f294c-aaa3-4de0-9c5d-0d00b2e2d69e",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "5644894e-ffa0-4c9f-b3dc-3a5c953708e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "5a3bfc70-9757-42ca-80a5-c5e063c50417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5644894e-ffa0-4c9f-b3dc-3a5c953708e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d040cc09-65da-41c8-bbcd-8e647157d6f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5644894e-ffa0-4c9f-b3dc-3a5c953708e3",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "559da9a2-da1a-4ed9-af20-85aa795a1e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "a6f1d5c3-6f1c-4c3d-b9e5-79f4d2753418",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "559da9a2-da1a-4ed9-af20-85aa795a1e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "979aa51a-747d-4091-9091-9b146e1b3e0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "559da9a2-da1a-4ed9-af20-85aa795a1e5c",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "04f81468-1d91-43bb-946f-cbd85e8ecee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "2c42640c-b7e4-4e7e-a4b9-3747b86aee68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f81468-1d91-43bb-946f-cbd85e8ecee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38cbe4b0-1470-4238-b448-cb6361f0c4cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f81468-1d91-43bb-946f-cbd85e8ecee2",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "22f0d73c-45ee-4b08-bf43-caa7f3157b0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "e7c7cbf2-04db-4143-b8a8-343b2f055619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f0d73c-45ee-4b08-bf43-caa7f3157b0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afc9399e-d6c4-4f45-b86e-9bd9826b86f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f0d73c-45ee-4b08-bf43-caa7f3157b0c",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "671cd61d-86be-4c6b-832b-987a77796823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "59910621-51d4-4792-b9c4-08fd87442e8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "671cd61d-86be-4c6b-832b-987a77796823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af2409da-dbf2-4824-bae5-d84600400d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "671cd61d-86be-4c6b-832b-987a77796823",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "c074643a-6bf2-4338-b527-7b5c7715a107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "3a68b7f2-0f48-4dd0-89ab-2e5630135e1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c074643a-6bf2-4338-b527-7b5c7715a107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "577d129b-306e-4ff9-b433-161a852b0630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c074643a-6bf2-4338-b527-7b5c7715a107",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "c23aa77b-c341-4806-9d2e-1a52ac62a3f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "38452b4b-858a-434f-ac66-9b5396bb3e7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c23aa77b-c341-4806-9d2e-1a52ac62a3f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89684cf-3343-4a76-a522-344ea1eddfb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c23aa77b-c341-4806-9d2e-1a52ac62a3f2",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "c19c0109-955c-4d59-9e46-cada6d1f8b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "7e63e283-1806-4d09-a135-685d7ad699ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19c0109-955c-4d59-9e46-cada6d1f8b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "729cdf39-16d8-4ea9-9712-bab285c1a2d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19c0109-955c-4d59-9e46-cada6d1f8b08",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "4d9a7158-c3dc-4f0b-b498-40c55acd6cb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "b368b1a9-a79f-4520-a800-f340534d1bea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d9a7158-c3dc-4f0b-b498-40c55acd6cb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bfe58c0-a14c-48a7-9fb7-30a5a6584052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d9a7158-c3dc-4f0b-b498-40c55acd6cb7",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "069c6511-d433-48b3-abe9-3cdeaf56efb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "6229d66d-7895-4c2f-8f95-0e60d8159611",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "069c6511-d433-48b3-abe9-3cdeaf56efb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c674c7bd-815d-4cee-9812-094501759ad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "069c6511-d433-48b3-abe9-3cdeaf56efb0",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "845c9e7a-84c8-4c0d-8094-701893de056c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "e7691ac1-c669-45f5-adaa-6a831ce4228e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "845c9e7a-84c8-4c0d-8094-701893de056c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abe89e12-3747-4d6e-8477-a610cb2596f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "845c9e7a-84c8-4c0d-8094-701893de056c",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "5c1dab19-1d75-4a79-af20-20e57d9f808d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "9705f433-2256-4dd3-b2a6-2116ba27cdfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c1dab19-1d75-4a79-af20-20e57d9f808d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b7a9a1-580e-4753-a5ce-958a24ce957b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c1dab19-1d75-4a79-af20-20e57d9f808d",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "53face26-fa8c-4040-b917-4be1083d668e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "60fcad35-f96f-4a13-9745-4986e21328d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53face26-fa8c-4040-b917-4be1083d668e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c34bdcc-0fd7-4c9b-8891-07714e80fc2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53face26-fa8c-4040-b917-4be1083d668e",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "027b8063-d857-4f8a-9278-31c135319cfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "938c2a64-eafe-4a32-bbf9-0842c7ac0352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027b8063-d857-4f8a-9278-31c135319cfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b99ce164-8bca-49dc-8094-b0a35822e54b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027b8063-d857-4f8a-9278-31c135319cfe",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "ac8e9ab2-d392-4e15-979c-b86f78395f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "722e7558-6ca1-4890-b07f-69d976eb2707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac8e9ab2-d392-4e15-979c-b86f78395f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6200baad-e0c5-47ea-bda9-c4965f024f8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac8e9ab2-d392-4e15-979c-b86f78395f75",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "7f1be53e-f444-4f04-90b1-21d4a69fb610",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "9b93e4d8-1b2d-4344-88d0-3eb57ab45f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f1be53e-f444-4f04-90b1-21d4a69fb610",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e47a1d09-713a-49e6-8fd7-9625e8b96a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f1be53e-f444-4f04-90b1-21d4a69fb610",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "46e07935-93b7-479f-bb63-afea38d29ff3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "d0bc22f1-ca97-42d0-a981-e1f119925cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46e07935-93b7-479f-bb63-afea38d29ff3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "726b99e6-a8d6-4c1b-b392-4ac1efdff401",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46e07935-93b7-479f-bb63-afea38d29ff3",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        },
        {
            "id": "ee339423-dd43-47e2-aed3-7811b1cc1f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "compositeImage": {
                "id": "5974435f-f81f-4501-862e-cb4bad204d08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee339423-dd43-47e2-aed3-7811b1cc1f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62f747a8-c5b3-42aa-8fa4-7f1fecbe26d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee339423-dd43-47e2-aed3-7811b1cc1f62",
                    "LayerId": "283875e8-9523-417a-a24c-8f90d978b1f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "283875e8-9523-417a-a24c-8f90d978b1f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68f44c47-fbc1-4c7d-92a7-e98a66378c12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 36
}