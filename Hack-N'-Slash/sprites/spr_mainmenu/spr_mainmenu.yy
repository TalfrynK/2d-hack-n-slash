{
    "id": "8abade0a-24dd-4460-b10a-4cb4f617b551",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mainmenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 27,
    "bbox_right": 474,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd5a7823-085c-457d-9680-095a4bfcead2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8abade0a-24dd-4460-b10a-4cb4f617b551",
            "compositeImage": {
                "id": "c3f214ff-b053-4ee7-b753-71da61b94b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd5a7823-085c-457d-9680-095a4bfcead2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ef5f7f4-1b83-49f7-9f68-4b5415337ba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd5a7823-085c-457d-9680-095a4bfcead2",
                    "LayerId": "e35d480d-2bb9-43ed-bdd0-17b689aafaea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "e35d480d-2bb9-43ed-bdd0-17b689aafaea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8abade0a-24dd-4460-b10a-4cb4f617b551",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}