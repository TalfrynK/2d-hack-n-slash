{
    "id": "929b5ce7-59ba-4f20-98eb-756a23a40563",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 27,
    "bbox_right": 474,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aee9bc96-4c01-46cd-8d2c-e45f2d2f3352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929b5ce7-59ba-4f20-98eb-756a23a40563",
            "compositeImage": {
                "id": "2fe34943-694d-40c5-9756-60f836713620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee9bc96-4c01-46cd-8d2c-e45f2d2f3352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6f6fc7f-202d-4f11-8fcb-1aa74845153c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee9bc96-4c01-46cd-8d2c-e45f2d2f3352",
                    "LayerId": "cdc1200d-1ae4-4903-98ca-2499f95f4b3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "cdc1200d-1ae4-4903-98ca-2499f95f4b3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "929b5ce7-59ba-4f20-98eb-756a23a40563",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}