{
    "id": "c0d477e5-5ebb-43cb-a6b3-053544a5d920",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a29c86d-617c-4e6f-b37e-4008de740ea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0d477e5-5ebb-43cb-a6b3-053544a5d920",
            "compositeImage": {
                "id": "05fb5f81-b5f3-48b3-bff0-5526c3a64bd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a29c86d-617c-4e6f-b37e-4008de740ea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b2d268c-b017-4b53-9f68-7498bded2b29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a29c86d-617c-4e6f-b37e-4008de740ea0",
                    "LayerId": "bf8f9a8d-28d5-4d9a-bcf9-19fbe6d690e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bf8f9a8d-28d5-4d9a-bcf9-19fbe6d690e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0d477e5-5ebb-43cb-a6b3-053544a5d920",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}