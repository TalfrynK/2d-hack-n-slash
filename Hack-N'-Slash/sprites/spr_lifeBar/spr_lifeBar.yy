{
    "id": "1d9c0ccb-2d41-4055-ac56-845a9c47f704",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lifeBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fd65d31-d570-4d14-95b8-68d84938729b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d9c0ccb-2d41-4055-ac56-845a9c47f704",
            "compositeImage": {
                "id": "04046e25-8191-4b88-a4d4-c68b75118ab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fd65d31-d570-4d14-95b8-68d84938729b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe4e4a12-4724-4be8-95b6-59fe521dfb06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fd65d31-d570-4d14-95b8-68d84938729b",
                    "LayerId": "77480a9c-dbf9-4a9a-a72c-709c8549fa9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "77480a9c-dbf9-4a9a-a72c-709c8549fa9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d9c0ccb-2d41-4055-ac56-845a9c47f704",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}