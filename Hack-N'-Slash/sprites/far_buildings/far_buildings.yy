{
    "id": "b5bef311-c89f-4adc-bc2b-9b60a0be328c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "far_buildings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eed929a5-049a-4769-85a8-763baee55ca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5bef311-c89f-4adc-bc2b-9b60a0be328c",
            "compositeImage": {
                "id": "3cbd73fe-109a-4d1a-9ebc-9a7e73deeeec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eed929a5-049a-4769-85a8-763baee55ca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de6bc1a6-104e-43cd-a91a-f8877b22b8c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eed929a5-049a-4769-85a8-763baee55ca0",
                    "LayerId": "c0aed53e-97f3-4c4f-9052-8370fc0bc27d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "c0aed53e-97f3-4c4f-9052-8370fc0bc27d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5bef311-c89f-4adc-bc2b-9b60a0be328c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}