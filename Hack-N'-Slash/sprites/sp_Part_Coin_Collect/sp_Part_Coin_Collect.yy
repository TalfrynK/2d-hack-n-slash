{
    "id": "8e1e148b-ac80-466a-aa60-27f513cf397f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_Part_Coin_Collect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c9d38e1-6a59-4249-ab62-8a0ee31a286d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "compositeImage": {
                "id": "022678ea-0342-4110-963c-c5b0304851bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9d38e1-6a59-4249-ab62-8a0ee31a286d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c2a9e81-4833-420a-bf76-46711bafb92a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9d38e1-6a59-4249-ab62-8a0ee31a286d",
                    "LayerId": "30b734f4-6977-4f58-b942-ae2d8586052b"
                }
            ]
        },
        {
            "id": "7758fa48-b53a-446e-9bb5-462444736c2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "compositeImage": {
                "id": "97e335b9-89c5-4c8a-a212-a809d1392a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7758fa48-b53a-446e-9bb5-462444736c2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e7a6a98-91c0-4c70-a4e7-15c74c400977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7758fa48-b53a-446e-9bb5-462444736c2b",
                    "LayerId": "30b734f4-6977-4f58-b942-ae2d8586052b"
                }
            ]
        },
        {
            "id": "1f8769c8-b866-4ca1-9adc-2ca5000eaed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "compositeImage": {
                "id": "b7699ba4-c9f6-49c2-9b57-d2b58c4eb0b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f8769c8-b866-4ca1-9adc-2ca5000eaed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a406fc44-023d-4395-b3a7-2b82aeeab9e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f8769c8-b866-4ca1-9adc-2ca5000eaed9",
                    "LayerId": "30b734f4-6977-4f58-b942-ae2d8586052b"
                }
            ]
        },
        {
            "id": "3b54970e-f240-47a8-9419-bbae014150ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "compositeImage": {
                "id": "39f66d57-75dd-4991-abb3-9862ab6d81c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b54970e-f240-47a8-9419-bbae014150ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d9e22cd-cc95-4c8d-8ec4-16f36c918811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b54970e-f240-47a8-9419-bbae014150ac",
                    "LayerId": "30b734f4-6977-4f58-b942-ae2d8586052b"
                }
            ]
        },
        {
            "id": "e9786700-b33b-4893-b99b-c4a10b17c33a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "compositeImage": {
                "id": "ebd57d38-2fe8-46a5-95d2-a35ff9cf81d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9786700-b33b-4893-b99b-c4a10b17c33a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3b78103-b4c7-474a-ae3f-568bebf884f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9786700-b33b-4893-b99b-c4a10b17c33a",
                    "LayerId": "30b734f4-6977-4f58-b942-ae2d8586052b"
                }
            ]
        },
        {
            "id": "368450a4-b9a8-4ad1-aafc-b9a1626ada64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "compositeImage": {
                "id": "325aba1b-b4f5-49c2-ad56-aad50a93c80f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368450a4-b9a8-4ad1-aafc-b9a1626ada64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf75bb5b-8548-4444-b5e8-e1234bc04801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368450a4-b9a8-4ad1-aafc-b9a1626ada64",
                    "LayerId": "30b734f4-6977-4f58-b942-ae2d8586052b"
                }
            ]
        },
        {
            "id": "bd1d3879-e2ce-4e98-9a7a-5958ec5b6ebe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "compositeImage": {
                "id": "855efd12-dadd-4376-8cd3-1c775da52fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd1d3879-e2ce-4e98-9a7a-5958ec5b6ebe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fc14123-839c-4518-9444-6a0334c18679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd1d3879-e2ce-4e98-9a7a-5958ec5b6ebe",
                    "LayerId": "30b734f4-6977-4f58-b942-ae2d8586052b"
                }
            ]
        },
        {
            "id": "3d040be3-f7d3-468f-91c1-0d86cf9b00fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "compositeImage": {
                "id": "6e02ac04-0edc-4da3-8166-8dc57a0eca53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d040be3-f7d3-468f-91c1-0d86cf9b00fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b93b1d09-88dd-48eb-93b5-576a10a88e8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d040be3-f7d3-468f-91c1-0d86cf9b00fc",
                    "LayerId": "30b734f4-6977-4f58-b942-ae2d8586052b"
                }
            ]
        },
        {
            "id": "62e65ba9-f3b6-4757-9f78-0d8a694f4ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "compositeImage": {
                "id": "ae97a3c9-c714-490a-8f5c-ce231ca4b9c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e65ba9-f3b6-4757-9f78-0d8a694f4ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be894296-4c68-46e8-8d42-d71f007709fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e65ba9-f3b6-4757-9f78-0d8a694f4ca1",
                    "LayerId": "30b734f4-6977-4f58-b942-ae2d8586052b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "30b734f4-6977-4f58-b942-ae2d8586052b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e1e148b-ac80-466a-aa60-27f513cf397f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}