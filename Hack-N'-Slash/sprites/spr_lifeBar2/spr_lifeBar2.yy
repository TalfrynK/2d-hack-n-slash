{
    "id": "8e56ce03-e52d-4db2-9a8e-4778e88aa6a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lifeBar2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "535a8cc8-209e-4a75-9072-f5ca347d613d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e56ce03-e52d-4db2-9a8e-4778e88aa6a4",
            "compositeImage": {
                "id": "3cf649eb-0aac-43b2-9440-8dd2b2d4bdb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "535a8cc8-209e-4a75-9072-f5ca347d613d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e04b3fb-9a3d-45ad-a34d-0fc43b744dfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "535a8cc8-209e-4a75-9072-f5ca347d613d",
                    "LayerId": "b677b05e-41e2-45d1-8e6c-0128b68bc0f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "b677b05e-41e2-45d1-8e6c-0128b68bc0f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e56ce03-e52d-4db2-9a8e-4778e88aa6a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}