{
    "id": "bec4b7ee-215e-4e44-8287-defe502ff2c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_roll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 14,
    "bbox_right": 38,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fba34bb2-2a17-4492-980a-5c23a77b7066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bec4b7ee-215e-4e44-8287-defe502ff2c6",
            "compositeImage": {
                "id": "b0b5eac3-d7db-491e-a057-2000562a79ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba34bb2-2a17-4492-980a-5c23a77b7066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2494a347-2a17-470a-a94e-3300739ee963",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba34bb2-2a17-4492-980a-5c23a77b7066",
                    "LayerId": "7927a0d8-5362-4de8-91e2-4ae30ba5e8ab"
                }
            ]
        },
        {
            "id": "fd6006b6-57e3-4caf-b1f3-0b16f9d1592f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bec4b7ee-215e-4e44-8287-defe502ff2c6",
            "compositeImage": {
                "id": "3815d398-c7f2-4ae2-a1ec-c73d4d1f8454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd6006b6-57e3-4caf-b1f3-0b16f9d1592f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6abb3542-292d-4deb-bec5-65b97ee0c5a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd6006b6-57e3-4caf-b1f3-0b16f9d1592f",
                    "LayerId": "7927a0d8-5362-4de8-91e2-4ae30ba5e8ab"
                }
            ]
        },
        {
            "id": "6126eb71-0392-474d-88c7-2f828249f524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bec4b7ee-215e-4e44-8287-defe502ff2c6",
            "compositeImage": {
                "id": "481c630e-37aa-4047-a722-85feff60e29f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6126eb71-0392-474d-88c7-2f828249f524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "763b32b2-cd29-48a2-96f8-d07ee42ae995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6126eb71-0392-474d-88c7-2f828249f524",
                    "LayerId": "7927a0d8-5362-4de8-91e2-4ae30ba5e8ab"
                }
            ]
        },
        {
            "id": "1d5fc4f1-cbb9-45a4-bb82-c6f8a4a4d7e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bec4b7ee-215e-4e44-8287-defe502ff2c6",
            "compositeImage": {
                "id": "d3319663-48fa-4707-aeb3-d981eadf684e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d5fc4f1-cbb9-45a4-bb82-c6f8a4a4d7e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a96527c-4f11-4d30-8a0c-711ff50d5136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d5fc4f1-cbb9-45a4-bb82-c6f8a4a4d7e5",
                    "LayerId": "7927a0d8-5362-4de8-91e2-4ae30ba5e8ab"
                }
            ]
        },
        {
            "id": "4e0da1b3-602d-4ffb-80bd-131de3766a83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bec4b7ee-215e-4e44-8287-defe502ff2c6",
            "compositeImage": {
                "id": "79a6c241-96b1-4cf0-a38a-fcdab1ad538d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e0da1b3-602d-4ffb-80bd-131de3766a83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26996883-e4b2-4d10-ad3a-8d353edf4922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e0da1b3-602d-4ffb-80bd-131de3766a83",
                    "LayerId": "7927a0d8-5362-4de8-91e2-4ae30ba5e8ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "7927a0d8-5362-4de8-91e2-4ae30ba5e8ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bec4b7ee-215e-4e44-8287-defe502ff2c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 27
}