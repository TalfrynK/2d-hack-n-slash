{
    "id": "c1b68538-f19e-41f0-93ff-ee7b748e34b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "adventurer_attack_three",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f624fe1-0199-410f-94b8-2e6409571d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1b68538-f19e-41f0-93ff-ee7b748e34b4",
            "compositeImage": {
                "id": "31fad540-54e6-4c66-b432-db7367c9dedf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f624fe1-0199-410f-94b8-2e6409571d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f53bfa5b-1fc2-49a2-a24d-d243f540e30d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f624fe1-0199-410f-94b8-2e6409571d63",
                    "LayerId": "deffce43-8257-4e6e-a4b4-20825ff8e8f1"
                }
            ]
        },
        {
            "id": "c82e8d42-af19-41b5-aa8d-45d43e127443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1b68538-f19e-41f0-93ff-ee7b748e34b4",
            "compositeImage": {
                "id": "23462205-cc8d-4121-b1e5-1bd05a0615ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c82e8d42-af19-41b5-aa8d-45d43e127443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50869fdf-d672-49b6-908d-5a1793cae79f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c82e8d42-af19-41b5-aa8d-45d43e127443",
                    "LayerId": "deffce43-8257-4e6e-a4b4-20825ff8e8f1"
                }
            ]
        },
        {
            "id": "f85e2591-7fa8-44a9-a320-bd9ce923fc47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1b68538-f19e-41f0-93ff-ee7b748e34b4",
            "compositeImage": {
                "id": "b9ad8883-c9f9-4cc3-aa4d-95e894a74b4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f85e2591-7fa8-44a9-a320-bd9ce923fc47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0ef5901-0adb-4651-860b-5bceb613ba6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f85e2591-7fa8-44a9-a320-bd9ce923fc47",
                    "LayerId": "deffce43-8257-4e6e-a4b4-20825ff8e8f1"
                }
            ]
        },
        {
            "id": "64504207-2dfa-4e4f-868f-0749ee78e077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1b68538-f19e-41f0-93ff-ee7b748e34b4",
            "compositeImage": {
                "id": "e66edfb8-2494-44a4-99c2-963db3ea82a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64504207-2dfa-4e4f-868f-0749ee78e077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9588f410-2e5b-4313-b04a-e00dcf8e540e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64504207-2dfa-4e4f-868f-0749ee78e077",
                    "LayerId": "deffce43-8257-4e6e-a4b4-20825ff8e8f1"
                }
            ]
        },
        {
            "id": "870b4fa1-66aa-45d5-85b3-6157d14acb96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1b68538-f19e-41f0-93ff-ee7b748e34b4",
            "compositeImage": {
                "id": "45d99c36-4309-4c14-a188-f55466b323be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "870b4fa1-66aa-45d5-85b3-6157d14acb96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56634e90-5737-4ff8-bb80-5860292bc8c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "870b4fa1-66aa-45d5-85b3-6157d14acb96",
                    "LayerId": "deffce43-8257-4e6e-a4b4-20825ff8e8f1"
                }
            ]
        },
        {
            "id": "3fca21a3-3481-4420-9dfb-71c9b3727c6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1b68538-f19e-41f0-93ff-ee7b748e34b4",
            "compositeImage": {
                "id": "5d9accba-9cb0-4710-bfa3-da686e36de97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fca21a3-3481-4420-9dfb-71c9b3727c6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43dcd8c1-be03-4272-9363-4104ffedd82e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fca21a3-3481-4420-9dfb-71c9b3727c6f",
                    "LayerId": "deffce43-8257-4e6e-a4b4-20825ff8e8f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "deffce43-8257-4e6e-a4b4-20825ff8e8f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1b68538-f19e-41f0-93ff-ee7b748e34b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 24,
    "yorig": 36
}