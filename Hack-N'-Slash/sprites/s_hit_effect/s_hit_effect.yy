{
    "id": "c9f99c77-562f-479f-9565-ac4d9a7120c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hit_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 3,
    "bbox_right": 20,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adcff9af-caf3-41dd-9993-632c81491085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9f99c77-562f-479f-9565-ac4d9a7120c5",
            "compositeImage": {
                "id": "83b94875-c0e9-4398-8d59-cd4f9b86fb23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adcff9af-caf3-41dd-9993-632c81491085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28d5ecbe-7094-4ad9-b14e-e613d0642e3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adcff9af-caf3-41dd-9993-632c81491085",
                    "LayerId": "216d4544-01ee-4d4c-a556-4c39bc4a6b41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "216d4544-01ee-4d4c-a556-4c39bc4a6b41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9f99c77-562f-479f-9565-ac4d9a7120c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 2
}