{
    "id": "258990aa-7858-4a7e-b4bc-279043fa2975",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_crown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ef6e4ce-ac09-479f-8413-08305d6702e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "258990aa-7858-4a7e-b4bc-279043fa2975",
            "compositeImage": {
                "id": "15d9ecf3-2e82-4d44-9729-b9d8273b53b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ef6e4ce-ac09-479f-8413-08305d6702e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f5b2b51-658d-4f5a-843e-18c83bfeccc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ef6e4ce-ac09-479f-8413-08305d6702e4",
                    "LayerId": "73ceec05-0dea-4b73-bd92-1ebee374d8e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "73ceec05-0dea-4b73-bd92-1ebee374d8e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "258990aa-7858-4a7e-b4bc-279043fa2975",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}