{
    "id": "9906ce45-caaa-4e05-b604-e6a67a1abcdc",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "ts_Ground_tileset",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 5,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "eabdf17c-7fdc-4d46-888a-5734f385fa9e",
    "sprite_no_export": false,
    "textureGroupId": "00000000-0000-0000-0000-000000000000",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 25,
    "tileheight": 32,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 32,
    "tilexoff": 0,
    "tileyoff": 0
}