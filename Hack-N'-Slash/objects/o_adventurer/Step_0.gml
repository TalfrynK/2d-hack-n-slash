switch (state)
{
	case "Move":
		#region Move State
		
		if input.right
		{ 
			move_and_collide(run_speed, 0);
			sprite_index = adventurer_run;
			image_xscale = 1;
			image_speed = 0.6;
		}

		if input.left
		{ 
			move_and_collide(-run_speed, 0);
			sprite_index = adventurer_run;
			image_xscale = -1;
			image_speed = 0.6;
		}

		if not input.right and not input.left
		{
			sprite_index = adventurer_idle;
			image_speed = 0.4;
		}
		else
		{
			if animation_hit_frame(2) or animation_hit_frame(5) { audio_play_sound(a_footstep, 1, false); }	
		}
	
		if input.roll
		{
			state = "Roll";
		}
		
		if input.slide
		{
			state = "Slide";
		}
		
		if input.attack
		{
			state = "Attack One";
		}
		
		if input.block and stamina >= 15
		{
			state = "Invincible";
		}
		
		if input.guard and stamina >= 5
		{
			state = "Guard";	
		}
		
		#endregion Move State
		break;
		
	case "Roll":
		#region Roll State
		set_state_sprite(adventurer_roll, 0.7, 0)
	
		if image_xscale == 1
		{
			move_and_collide(roll_speed, 0);
		}
	
		if image_xscale == -1
		{
			move_and_collide(-roll_speed, 0);
		}
		
		if animation_end() 
		{ 
			state = "Move";
		}
		
		#endregion Roll State
		break;
		
	case "Slide":
		#region Slide State
		set_state_sprite(adventurer_slide, 0.25, 0)
		
		if image_xscale == 1
		{
			move_and_collide(slide_speed, 0);
		}
	
		if image_xscale == -1
		{
			move_and_collide(-slide_speed, 0);
		}
		
		if animation_end() 
		{ 
			state = "Move";
		}
		
		#endregion Slide State
		break;
	
	case "Attack One":
		#region Attack One State
		set_state_sprite(adventurer_attack_one, attack_speed + 0.3, 0)
		
		if animation_hit_frame(2)
		{
			create_hitbox(x, y, o_adventurer, adventurer_attack_one_damage, 2, 1, strength, image_xscale)	
			audio_play_sound(a_miss, 2, false);
		}
		
		if input.attack and animation_hit_frame_range(0, 4)
		{
			state = "Attack Two";
		}
		
		if animation_end() 
		{ 
			state = "Move";
		}
		
		#endregion Attack One State
		break;
		
	case "Attack Two":
		#region Attack Two State
		set_state_sprite(adventurer_attack_two, attack_speed + 0.1, 0)
		
		if animation_hit_frame(3)
		{
			audio_play_sound(a_miss, 2, false);
			create_hitbox(x, y, o_adventurer, adventurer_attack_two_damage, 1, 1, strength * 1.5, image_xscale)	
		}
		
		if input.attack and animation_hit_frame_range(3, 5)
		{
			state = "Attack Three";
		}
		
		if animation_end() 
		{ 
			state = "Move";
		}
		
		#endregion Attack Two State
		break;
	
	case "Attack Three":
		#region Attack Three State
		set_state_sprite(adventurer_attack_three, attack_speed + 0.5, 0)
		
		if animation_hit_frame(2)
		{
			audio_play_sound(a_miss, 2, false);
			create_hitbox(x, y, o_adventurer, adventurer_attack_three_damage, 4, 1, strength * 2, image_xscale)	
		}
		
		if animation_end() 
		{ 
			state = "Move";
		}
		
		#endregion Attack Three State
		break;
		
	case "Knockback":
		#region Knockback State
		knockback_state(adventurer_damaged, 0.2, adventurer_damaged)
		add_screenshake(0.5, 3)
		#endregion Knockback State
		break;
		
	case "Invincible":
		#region Invincible State
		alarm[0] = invulnerable_time;
		stamina -= 15;
		current_state = "Invulnerable";
		state = "Move";
		#endregion Invincible State
		break;
		
	case "Guard":
		#region Guard State
		set_state_sprite(adventurer_defend, 0.4, 0);
		if keyboard_check_released(vk_rcontrol) or stamina == 0
		{
			state = "Move";	
		}
		#endregion Guard State
		break;
		
	case "Death":
		#region Death State
		set_state_sprite(deathSprite, deathSpeed, 0)
		add_screenshake(3, 7)
		if animation_end()
		{
			set_state_sprite(deathSprite, 0, 6)
			ini_open("save.ini");
			ini_write_real("Scores", "Score", o_adventurer.final_score);
			var highscore = ini_read_real("Scores", "Highscore", 0);
			if o_adventurer.final_score > highscore
			{
				ini_write_real("Scores", "Highscore", o_adventurer.final_score);	
			}
			ini_close();
			audio_pause_sound(The_Wyverns_Final_song)
			room_goto(r_gameover);
		}
		
		#endregion Death State
		break;
		
	default:
		#region Default
		show_debug_message("Unavailable")
		state = "Move";
		#endregion Default
		break;

}