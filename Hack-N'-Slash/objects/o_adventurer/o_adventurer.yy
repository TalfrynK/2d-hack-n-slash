{
    "id": "c437602f-dcd2-4d93-8428-86c1b99f7fb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_adventurer",
    "eventList": [
        {
            "id": "eeee27f6-4dc3-45a2-898a-ad826761e616",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c437602f-dcd2-4d93-8428-86c1b99f7fb0"
        },
        {
            "id": "65395553-4bf2-4330-9cc1-710d68317230",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c437602f-dcd2-4d93-8428-86c1b99f7fb0"
        },
        {
            "id": "fad2d126-3842-4335-885b-60cccdd21e52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c437602f-dcd2-4d93-8428-86c1b99f7fb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4725870f-634e-4ddd-b750-49b976faaa0a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b02653b2-5558-403d-ab37-ddeb892e750d",
    "visible": true
}