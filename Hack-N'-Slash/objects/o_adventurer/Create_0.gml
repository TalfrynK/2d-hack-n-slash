event_inherited();

deathSprite = adventurer_death;
deathSpeed = 0.7;

kills = 0;
level = 1;
experience = 0;
max_exp = 25;
stamina = 30;
max_stamina = 30;
strength = 5;
invulnerable_time = 300;
level_status = "unfin";
final_score = 0;
kill_score = 0;


image_speed = 0.7;
state = "Move";
current_state = "Vulnerable";
run_speed = 2.5;
roll_speed = 4;
slide_speed = 4;
attack_speed = 0.5;
input = instance_create_layer(0, 0, "Instances", o_input);