var hp_x = 8;
var hp_y = 8;
var hp_width = 174;
var hp_height = 10;
draw_set_font(font1)
draw_set_color(c_white)
display_set_gui_size(720, 360)

if instance_exists(o_adventurer)
{
	draw_hp = lerp(draw_hp, o_adventurer.hp, 0.25);
	draw_maxhp = o_adventurer.max_hp;
	draw_exp = lerp(draw_exp, o_adventurer.experience, 0.25);
	draw_maxexp = o_adventurer.max_exp;
	draw_stamina = lerp(draw_stamina, o_adventurer.stamina, 0.25);
	draw_maxstamina = o_adventurer.max_stamina;
}
else
{
	draw_hp = lerp (draw_hp, 0, 0.25);
}

draw_sprite(spr_guiBar, 0, hp_x, hp_y)

var hp_percent = draw_hp / draw_maxhp;

var xp_percent = draw_exp / draw_maxexp;

var stamina_percent = draw_stamina / draw_maxstamina;

if hp_percent > 0 
{ 
	if o_adventurer.current_state = "Invulnerable" { draw_sprite_stretched(spr_lifeBar2, 0, hp_x + 18, hp_y + 2, (hp_x+18)+(hp_width*hp_percent), 4); } 
	else { draw_sprite_stretched(spr_lifeBar, 0, hp_x + 18, hp_y + 2, (hp_x+18)+(hp_width*hp_percent), 4); }
}
else { instance_destroy(); }

if o_adventurer.experience > 0 { draw_sprite_stretched(spr_xpBar, 0, hp_x + 18, hp_y + 8, (hp_x+18)+(hp_width*xp_percent), 4); }

if o_adventurer.stamina > 0 { draw_sprite_stretched(spr_staminaBar, 0, hp_x + 18, hp_y + 14, (hp_x+18)+(hp_width*stamina_percent), 4); }

if not instance_exists(o_adventurer) { exit; }

var start_x = 8;
var start_y = 20;
var padding_x = 4;
var padding_y = 4;

var text = string(o_adventurer.level);
var text_width = string_width(text) / 2;
var text_height = string_height(text) / 2;
start_x += text_width + padding_x*3;

draw_text(hp_x + 6, hp_y + 3.5, text)

