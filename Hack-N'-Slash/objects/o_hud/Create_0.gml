var view_id = view_camera[0];
var view_width = camera_get_view_width(view_id);
var view_height = camera_get_view_height(view_id);

display_set_gui_size(view_width, view_height)

if not instance_exists(o_adventurer) { exit; }

draw_hp = o_adventurer.hp;
draw_maxhp = o_adventurer.max_hp;

draw_exp = o_adventurer.experience;
draw_maxexp = o_adventurer.max_exp;

draw_stamina = o_adventurer.stamina;
draw_maxstamina = o_adventurer.max_stamina;

audio_play_sound(The_Wyverns_Final_song, 4, true);