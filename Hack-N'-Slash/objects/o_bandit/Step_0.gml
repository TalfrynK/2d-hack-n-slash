switch (state)
{
	case "Chase":
		#region Chase State
		set_state_sprite(s_bandit_walk, 0.4, 0);
		
		if  not instance_exists(o_adventurer) or o_adventurer.hp <= 0 or o_adventurer.state == "Death"
		{
			set_state_sprite(s_bandit_idle, 0.7, 0);
			
			if not instance_exists(o_adventurer) break;
		}
		
		image_xscale = sign(o_adventurer.x - x)
		
		if image_xscale == 0
		{
			image_xscale = 1;
		}
		
		var direction_facing = image_xscale
		var dist_to_player = point_distance(x, y, o_adventurer.x, o_adventurer.y)
		
		
		if dist_to_player <= attack_range 
		{ 
			state = "Attack" 
		}
		else { move_and_collide(direction_facing * movement_speed, 0) }
		#endregion Chase State
		break;
	
	case "Attack":
		#region Attack State
		
		set_state_sprite(s_bandit_attack, attack_speed, 0)
		
		if animation_hit_frame(4)
		{
			audio_play_sound(a_miss, 2, false);
			create_hitbox(x, y, self, s_bandit_attack_damage, 4, 1, 10, image_xscale)	
		}
		
		if animation_end() 
		{ 
			state = "Chase";
		}
		#endregion Attack State
		break;
		
	case "Knockback":
		#region Knockback State
		knockback_state(s_bandit_hitstun, 0.15, "Chase")
		#endregion Knockback State
		break;
		
	case "Block":
		#region Block State
		block_state(s_bandit_block, 0.15, "Chase")
		#endregion Block State
		break;
		
	case "Death":
		#region Death State
		set_state_sprite(s_bandit_die, 0.4, 0)
		
		if animation_end() 
		{ 
			o_adventurer.final_score += self.kill_score;
			show_debug_message("Score: " + string(o_adventurer.final_score))
			o_adventurer.kills += 1; 
			drop_exp(exp_drop); 
			if o_adventurer.stamina < o_adventurer.max_stamina 
			{ 
				if o_adventurer.stamina > o_adventurer.max_stamina + 2.5
				{
					o_adventurer.stamina = o_adventurer.max_stamina	
				}
				else { o_adventurer.stamina += 2.5; } 
			}
			instance_destroy(); 
		}
		
		#endregion Death State
		break;
	
	default:
		show_debug_message(state + "Unavailable")
		state = "Chase";
		break;
}