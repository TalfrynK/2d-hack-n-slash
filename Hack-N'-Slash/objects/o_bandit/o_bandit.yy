{
    "id": "43f47d66-e0ba-4e7b-8657-2a0f8679773b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bandit",
    "eventList": [
        {
            "id": "be01c276-8ab4-4739-92e4-d2718ccf2aef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43f47d66-e0ba-4e7b-8657-2a0f8679773b"
        },
        {
            "id": "39752e07-9871-4995-b936-ac60f657b70c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43f47d66-e0ba-4e7b-8657-2a0f8679773b"
        }
    ],
    "maskSpriteId": "a2d977aa-ab93-46a1-936a-c06d91e7bf3a",
    "overriddenProperties": null,
    "parentObjectId": "0b96672c-75e0-4879-aa20-2c4455c1375a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "869ed739-5cff-4d70-9db4-34e9bf9807d5",
    "visible": true
}