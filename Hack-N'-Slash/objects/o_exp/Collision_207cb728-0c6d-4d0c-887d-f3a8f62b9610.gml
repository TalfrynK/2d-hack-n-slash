if not instance_exists(o_adventurer) { exit; }

with (other)
{
	experience += 1;
	
	if experience >= max_exp
	{
		level += 1;
		audio_play_sound(orchestra, 4, false)
		if o_adventurer.level >= 3 { o_knight.hp = o_knight.hp * 1.1; o_knight.max_hp = o_knight.hp; o_knight.movement_speed += 0.2;}
		o_bandit.hp = o_bandit.hp * 1.1;
		o_bandit.max_hp = o_bandit.hp;
		o_bandit.movement_speed += 0.12;
		experience = experience - max_exp;
		max_exp = max_exp * level;
		run_speed += 1;
		attack_speed += 0.1;
		max_hp += 5;
		max_stamina += 10;
		invulnerable_time = invulnerable_time * 1.25;
		hp += max_hp/2;
		if hp > max_hp { hp = max_hp; }
		strength = strength* level;
	}
}
instance_destroy();