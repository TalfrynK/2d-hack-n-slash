{
    "id": "c80a7eb1-2e18-44c2-922c-8d2027aea81a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_exp",
    "eventList": [
        {
            "id": "a9aa3376-5a01-49b2-b083-b9018837bb31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c80a7eb1-2e18-44c2-922c-8d2027aea81a"
        },
        {
            "id": "eb43f62f-bffa-4d42-a227-91e8e92da807",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c80a7eb1-2e18-44c2-922c-8d2027aea81a"
        },
        {
            "id": "184bbd1c-0398-4dbc-b833-908a0877d3d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c80a7eb1-2e18-44c2-922c-8d2027aea81a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c80a7eb1-2e18-44c2-922c-8d2027aea81a"
        },
        {
            "id": "207cb728-0c6d-4d0c-887d-f3a8f62b9610",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c437602f-dcd2-4d93-8428-86c1b99f7fb0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c80a7eb1-2e18-44c2-922c-8d2027aea81a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "74192223-aca6-45da-b639-95d9b382fa03",
    "visible": true
}