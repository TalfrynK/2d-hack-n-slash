if not instance_exists(o_adventurer) { exit; }

var dir = point_direction(x, y, o_adventurer.x, o_adventurer.y);
var acceleration = 0.20;

motion_add(dir, acceleration);