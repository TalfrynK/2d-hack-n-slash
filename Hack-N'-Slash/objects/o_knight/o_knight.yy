{
    "id": "78565821-25d8-48a3-8f8e-03b1f42e64a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_knight",
    "eventList": [
        {
            "id": "a2a1df9b-1c62-4cde-92f5-ba5bd795a803",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78565821-25d8-48a3-8f8e-03b1f42e64a1"
        },
        {
            "id": "36ecf26d-aaf4-481b-b4a3-8bbc4824f312",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78565821-25d8-48a3-8f8e-03b1f42e64a1"
        }
    ],
    "maskSpriteId": "a58e0eb3-fa55-4677-bd1f-01f853c370b6",
    "overriddenProperties": null,
    "parentObjectId": "0b96672c-75e0-4879-aa20-2c4455c1375a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3dfd62bb-226a-4657-8069-1c585b6be11d",
    "visible": true
}