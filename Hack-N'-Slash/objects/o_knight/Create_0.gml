event_inherited();

state = "Chase";
og_attack_range = 32;
attack_range = og_attack_range;
movement_speed = 1.5
attack_speed = 0.8
exp_drop = 5
kill_score = 25

hp = 60
max_hp = hp;

block_chance = 2
current_state = "Vulnerable";