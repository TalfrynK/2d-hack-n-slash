var enemy_count = instance_number(o_enemy_parent)
var enemies_spawned = 18;
var enemy = o_bandit;

if instance_exists(o_adventurer) and enemy_count < o_adventurer.kills/(3+o_adventurer.level)
{
	
	if o_adventurer.level > 1 { enemy = choose(o_bandit, o_knight); }
	var new_x = random_range(220, room_width-220)
	var new_y = o_adventurer.y;
	
	while point_distance(new_x, 0, o_adventurer.x, 0) < 220
	{
		var new_x = random_range(220, room_width-220)
	}
	
	instance_create_layer(new_x, new_y, "Instances", enemy)	
	var i = instance_number(enemy) - 1
	var enemy_id = instance_find(enemy, i)
	
	if enemy == o_bandit
	{
		enemy_id.movement_speed += random_range(0, 1.2);
		enemy_id.attack_speed += random_range(-0.5, 1.0);
		enemy_id.exp_drop += irandom_range(0, 2);
		enemy_id.kill_score += irandom_range(0, 5);
		enemy_id.hp += random_range(0, 10);
		enemy_id.block_chance += random_range(-0.5, 0.5);
		enemy_id.max_hp = enemy_id.hp;
		show_debug_message("bandit exp: " + string(enemy_id.exp_drop))
	}
	if enemy == o_knight
	{
		enemy_id.movement_speed += random_range(-1.5, 1.2);
		enemy_id.attack_speed += random_range(-0.2, 1.2);
		enemy_id.exp_drop += irandom_range(0, 2);
		enemy_id.kill_score += irandom_range(0, 10);
		enemy_id.hp += random_range(0, 20);
		enemy_id.block_chance += random_range(-0.5, 0.5);
		enemy_id.max_hp = enemy_id.hp;
		show_debug_message("knight exp: " + string(enemy_id.exp_drop))
	}

	enemies_spawned += 1;
}