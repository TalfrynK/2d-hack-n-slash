{
    "id": "eab87b1c-12c5-47f0-b5bd-6adbea514838",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_camera",
    "eventList": [
        {
            "id": "6530c5da-dbf2-46df-b65a-390e22487b96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eab87b1c-12c5-47f0-b5bd-6adbea514838"
        },
        {
            "id": "596e3aa7-2a42-48a9-8bf6-c402d342d16c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eab87b1c-12c5-47f0-b5bd-6adbea514838"
        },
        {
            "id": "c68ad253-b87d-4e1a-a332-3cd79f286cd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "eab87b1c-12c5-47f0-b5bd-6adbea514838"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}