if not instance_exists(o_adventurer) { exit; }

var target_x = o_adventurer.x + random_range (-screenshake, screenshake);
var target_y = o_adventurer.y + random_range (-screenshake, screenshake);

if o_adventurer.x > 80 and o_adventurer.x < 1200
{	
	x = lerp(x, target_x, 0.3);
	y = target_y - 80;
	camera_set_view_pos(view_camera[0], x - width/2, y - height/2);
}
else if o_adventurer.x <= 80
{
	x = lerp(x, 0, 0.3);
	y = target_y - 80;
	camera_set_view_pos(view_camera[0], x - width/2, y - height/2);
}
else if o_adventurer.x >= 1200
{
	x = lerp(x, 1200, 0.3);
	y = target_y - 80;
	camera_set_view_pos(view_camera[0], x - width/2, y - height/2);
}