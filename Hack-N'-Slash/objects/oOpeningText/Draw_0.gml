draw_set_color(c_lime);
draw_set_font(font0);

// see prompts in here for require arguments.
scrTextDisplay("Alert! Alert! Sector " + string(finalsector) + " Section " + string(finalcode) + " has been breached!" + "# Prisoners in captivity have been released!"
				+ "# # Earlier this hour, brown-robed bandits, with the aid of blue-clad undead knights, have used the forbidden magicks to escape imprisonment."
				+ "# # You must fight them and put them down. We cannot risk them fleeing into the outside world. As of know, they remain on the streets of the cyber city."
				+ "# # I hope you remember your skills. Use 'A' and 'D' to move Left and Right. 'Spacebar' is to roll and 'Left Shift' to slide." + "# And of course, press 'Enter' to attack. But don't 'Spam', to use your full power you must time your attacks perfectly to unlock the 'Combo'."
				+ "# # These are the instructions you had left for us... although we do not understand many words in there, we hope that you remember. # "
				+ "# # It is all up to you now..."
				+ "# # # Good Luck, and may the Anzel's Blessings be with you...", 50, 50, 0, true, extrac);
				