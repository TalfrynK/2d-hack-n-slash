{
    "id": "a4368ba3-dfe1-4faa-89ab-2e4266405922",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oOpeningText",
    "eventList": [
        {
            "id": "bad89343-70e7-42a0-acdd-6662293981f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a4368ba3-dfe1-4faa-89ab-2e4266405922"
        },
        {
            "id": "f17e954e-b229-4d56-9a3d-99cf601e8e52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a4368ba3-dfe1-4faa-89ab-2e4266405922"
        },
        {
            "id": "ef3c0074-c8cb-4693-b988-7d521eee6a3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a4368ba3-dfe1-4faa-89ab-2e4266405922"
        },
        {
            "id": "07e14dba-7e17-4a0d-993d-f5bce2aae6e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a4368ba3-dfe1-4faa-89ab-2e4266405922"
        },
        {
            "id": "c9457b73-a2be-4e5e-9e62-cbf6c19459a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a4368ba3-dfe1-4faa-89ab-2e4266405922"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "679cccb7-6fa5-41ba-abc3-e18f844a2847",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "82910a99-aa48-4144-afc1-1c566d5dfc0f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "81903aee-f3fe-4a5e-80b1-a6626c51cd92",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "c4775b82-5fff-4548-a6e4-8d628cf525c1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}