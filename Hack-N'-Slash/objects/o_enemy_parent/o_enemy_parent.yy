{
    "id": "0b96672c-75e0-4879-aa20-2c4455c1375a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_parent",
    "eventList": [
        {
            "id": "637b55b3-e2ac-4721-a0cb-a1500ed56a58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0b96672c-75e0-4879-aa20-2c4455c1375a"
        },
        {
            "id": "8703c1a3-348a-4caa-acbf-d2c1f922e2ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0b96672c-75e0-4879-aa20-2c4455c1375a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4725870f-634e-4ddd-b750-49b976faaa0a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}