{
    "id": "dc4ac78c-465c-4eec-a490-1bf0c20845a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hit_effect",
    "eventList": [
        {
            "id": "3651d3c7-1983-4027-b2e5-82436fe4c941",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc4ac78c-465c-4eec-a490-1bf0c20845a6"
        },
        {
            "id": "ba5be285-7f08-49c5-8b23-d9d4202cd984",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dc4ac78c-465c-4eec-a490-1bf0c20845a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9f99c77-562f-479f-9565-ac4d9a7120c5",
    "visible": true
}