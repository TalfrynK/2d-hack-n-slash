{
    "id": "c9be9f30-2da3-4e58-a7ab-bef951852368",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_play",
    "eventList": [
        {
            "id": "d01bf2ce-0f84-49af-a160-5ce067ef22ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c9be9f30-2da3-4e58-a7ab-bef951852368"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3c98880b-4db8-4f0d-841e-655b69d4a2fc",
    "visible": true
}