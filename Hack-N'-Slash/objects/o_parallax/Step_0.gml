if not instance_exists(o_camera) { exit; }
layer_x("Close_Buildings", o_camera.x / 2.5)
layer_x("Far_Buildings", o_camera.x / 1.75)
layer_x("Skyline", o_camera.x  / 1.25)