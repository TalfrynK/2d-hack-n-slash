if creator == noone or creator == other
{
	exit;
}

if creator != o_adventurer
{	
	if other.current_state == "Invulnerable" or (other.state == "Guard" and creator.image_xscale != other.image_xscale)
	{
		other.hp = other.hp;
		if other.state == "Guard"
		{
			other.stamina -= 5;	
		}
	}
	else
	{
		other.hp -= damage;	
		audio_play_sound(a_medium_hit, 3, false);
		other.state = "Knockback";
		other.knockback_distance = knockback * image_xscale;
		other.image_xscale = -image_xscale;
		
		repeat(6)
		{
			instance_create_layer(other.x, other.y - 12, "Effects", o_hit_effect)	
		}
	}	
}

if other.object_index != o_adventurer
{
	other.alarm[0] = 120;	
	add_screenshake(2, 4);
	
	repeat(6)
	{
		instance_create_layer(other.x, other.y - 12, "Effects", o_hit_effect)	
	}
}


show_debug_message(other.hp);