{
    "id": "ff6db7b7-3182-41fd-b340-6f152836c615",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hitbox",
    "eventList": [
        {
            "id": "24cabec6-ff03-4a38-9497-fa97d62e78e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff6db7b7-3182-41fd-b340-6f152836c615"
        },
        {
            "id": "ae886820-ecf9-4b89-b7d5-25d319e4d9ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ff6db7b7-3182-41fd-b340-6f152836c615"
        },
        {
            "id": "f99a68c4-7a77-4fdd-a8da-7d2b50ac1a68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c437602f-dcd2-4d93-8428-86c1b99f7fb0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ff6db7b7-3182-41fd-b340-6f152836c615"
        },
        {
            "id": "0bdabb6b-7da8-4b28-930a-712fdc203c89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0b96672c-75e0-4879-aa20-2c4455c1375a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ff6db7b7-3182-41fd-b340-6f152836c615"
        },
        {
            "id": "a4f74fd9-7c97-4312-a018-90cebee04e3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ff6db7b7-3182-41fd-b340-6f152836c615",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ff6db7b7-3182-41fd-b340-6f152836c615"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "3dcd3977-806d-4dc6-bb30-dc5ec0af9492",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4bc4bdf5-43c8-4968-b00e-385bfb24ae17",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "ead3172c-b99b-4f15-909f-6f2bef696192",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "11039b1b-8df0-4d9f-8e96-b3234de01910",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}