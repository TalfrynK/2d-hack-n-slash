if creator == noone or creator == other
{
	exit;
}

var block = 100 / other.block_chance
var block_check = random_range(0, 100)

if creator == o_adventurer
{
	if block_check > block and other.current_state == "Vulnerable"
	{
		other.hp -= damage;	
		audio_play_sound(a_medium_hit, 3, false);
		other.state = "Knockback";
		other.knockback_distance = knockback * image_xscale;
		other.image_xscale = -image_xscale;
	}
	else
	{
		other.hp = other.hp;
		other.state = "Block";
		other.block_distance = knockback * image_xscale;
		other.image_xscale = -image_xscale;
	}
	
	repeat(6)
	{
		instance_create_layer(other.x, other.y - 12, "Effects", o_hit_effect)	
	}

	if other.object_index != o_adventurer
	{
		other.alarm[0] = 120;	
		add_screenshake(2, 4);
	}
}




show_debug_message(other.hp);