{
    "id": "98a293cc-1a86-46b6-aff5-9c0869321f62",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bahnschrift Light",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1e3ad930-d38f-482b-9376-7f6cecb38ffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "bcc61eeb-6ff9-4a7d-9105-52b4a81a3e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 37,
                "y": 66
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "123efb6b-4eb4-46bb-a70a-dbeb6a6e4d63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 31,
                "y": 66
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b39e4363-6196-42c0-941a-37f6b7e0b6ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 21,
                "y": 66
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "cb2edfcf-e6e2-4a8f-b7f1-21470f9d3a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 12,
                "y": 66
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fa054244-ada1-409b-886f-5dd8f56a822c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6e13995c-065a-4e6f-a12a-79b76c559267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 113,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "70ccd395-9797-467b-8345-5bea37651dd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 109,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e01f665a-576f-45fb-a1ee-98a22f2b73c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 104,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f4c60e32-407a-4d29-af9e-d690cab96a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "44864b5b-23e7-4e25-9672-09d41966a9ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 41,
                "y": 66
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b53057e9-dfc7-4990-b11d-8c16763685c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2af55da7-a7cc-4ebc-ace0-ce451d03e461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 78,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0fa48b4d-7da1-4481-a63e-befb81f5212b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 71,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2e5570e1-4843-45a1-be81-ad967d5324f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 67,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "13bd3cea-2643-4921-9948-ec81129be3bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 58,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "94f2c335-5536-467c-ae58-0c425ea021e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 50,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b0dc9e5d-c3d7-4bd1-8c43-beb08ca3ae36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 45,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ba5353d4-8e49-45ab-b02a-62f40d6c3473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 37,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "135b1a0b-0f46-458f-a060-428864632363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "10e79f1b-9615-44ee-a80f-1fa375765f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d615d381-82a7-493d-8e32-fcb1416e8749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 82,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d67ba803-1d66-435b-bc81-e046f73686f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 48,
                "y": 66
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "30b6342e-c4fc-4d67-8e3f-e41299597c85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 66
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9087dda7-738a-4b2f-9921-8804833f1d8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 64,
                "y": 66
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1fd2c37c-208b-4dd1-8023-ee883a0798d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 111,
                "y": 82
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0b9f0e80-1a73-4b84-b8ea-483864c86be8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 107,
                "y": 82
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e18e6249-0c4c-4a28-a2f0-c39f18426b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 103,
                "y": 82
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a5fc0942-6b41-49c7-be4b-c25d1c3d0400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 96,
                "y": 82
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "384380c7-e118-4f30-9d1e-6e7afbd320d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 89,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "df0bea35-9d7b-49f1-b669-2facf0b349d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 82,
                "y": 82
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cdfb682d-6adf-4b81-b9b5-66599c134530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 75,
                "y": 82
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "592a81d3-7a80-4373-9da6-0576d48e39b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 63,
                "y": 82
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ba91a7ec-a7e3-4467-a8aa-536b701bc54c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 53,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "216093c8-486f-4517-a69f-ffe7c24c44cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 44,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ea395903-2f43-40aa-aa24-3d0d4e384be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 82
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9804a370-5e91-4780-a477-3630715007fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 27,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b653463f-ee89-4846-81bf-32a60507391b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 19,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "017c7644-124f-4e70-a37a-2e04660552aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 11,
                "y": 82
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "586aa184-1907-4556-ba3a-9d51f0aba0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "76c1b30f-8748-4726-bb6f-45ef73064804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 111,
                "y": 66
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "50b9df67-7416-4f89-a5d3-928ef2264599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 107,
                "y": 66
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ad4bdee9-e845-4d64-8109-fc91a35cd007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 66
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "fdc4ff35-a01d-43a2-a41e-de70cdeb0377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 90,
                "y": 66
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "086b40dd-6d56-4975-935d-d7bcac4d2e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 82,
                "y": 66
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d69c0d92-1cf2-45e5-a664-b8ab22788447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 66
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e0e4b9a1-ff6f-41e0-b8de-84f9e3abef03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a7bb1120-1ab0-4be4-acf7-010133ae3a72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3c23eae1-27e9-422d-9208-669491622466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 112,
                "y": 34
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "889fc097-fecd-4fca-92a5-624bcf364342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 52,
                "y": 18
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9cc96974-3423-400e-87ad-eb996885fa17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 18
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "622b3b99-177a-46f2-b17c-9ac16773c958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 18
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "971dd988-0174-4afa-b851-26c949f6bc2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 20,
                "y": 18
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e2af520f-cb59-4512-ac48-55570698cad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 18
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "40a0c335-ac35-4d57-bfd2-101d3169e234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 18
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c7b93c5b-851b-45c4-94a4-aff438fc2f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e9b9660b-b58f-4993-9c3f-7cfdf54dfc9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "fc04a26e-76b0-4cc2-a61b-ed3b24d0e4eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "14dcae80-a818-4547-bc7b-a9e1b388e0eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "97a1f546-d729-4bc3-9e3c-d7b5827c0eda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 18
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9e2cceba-a3ec-400c-9b47-13cbdaad5852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a4b8ffdf-75a0-4c9c-9963-5e34cf1d8f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5dfd62fa-6475-4145-a91c-86b90eeec44d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "95616e00-1724-43ef-8d1e-575f9ed8f0d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a259a9ad-bead-44be-9a71-963e163c8432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "08f23b0b-5c98-4dc0-821d-c65b8a602e83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6018762f-468a-4c0f-9753-256c0a8cb1e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f08d16da-9eca-499c-a615-4d779e1018a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9129af81-0af0-44a8-bdb9-49703ffe7514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b1d0b608-2377-4751-95f9-352cd00addf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a70f7882-605e-4bdf-8699-67fdf7b8fe0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "24859e59-dc2c-40be-8c85-0c814640229a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 62,
                "y": 18
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b3387642-3c55-4eeb-8ad6-17181c7db431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 18,
                "y": 34
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "26dd83e4-2ac0-44f3-bbee-e316d54a82b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 70,
                "y": 18
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "943edfbd-b7f6-4b4b-b8ef-b957626492f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 99,
                "y": 34
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2d8f645f-0143-4f64-8b48-795d2a883f74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 90,
                "y": 34
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8d57cb80-152a-48db-9691-c559d5dc58f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 84,
                "y": 34
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e666d7be-f0b3-45e3-a42d-f80c66bac4db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 34
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6818efae-42b8-4d07-9933-258d5025a7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 64,
                "y": 34
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b9ad5c61-3a7e-4932-9a76-a3737a00e96a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 56,
                "y": 34
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9ccea5fc-684e-43a2-b109-852fd25b992a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 48,
                "y": 34
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0286f1c3-02fa-4432-9371-6506192cc21e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 40,
                "y": 34
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "917f3655-dc91-4cbd-9f99-34302426d465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 32,
                "y": 34
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9f265cc9-03c0-4189-af1a-8bd625df0632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 104,
                "y": 34
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f8ca49a2-18de-46d3-92f2-1d7b7744ea1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 26,
                "y": 34
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b4e39b2a-af2a-4dac-b756-50e1e51a6547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 34
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8f96edf3-978e-49cb-9919-9b2c59d6b87b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4bf6dbe2-99ee-4c95-a516-3756c842336f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 116,
                "y": 18
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f8afbfc1-9055-4b8e-bb3e-77b1ad42e38e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 108,
                "y": 18
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a79d8d11-f609-42c0-993f-8daa9fca7626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 100,
                "y": 18
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "47b1471b-746e-402a-9440-9bc8ac91dd26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 92,
                "y": 18
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "af734cfc-860d-4506-a910-31bac320542a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 85,
                "y": 18
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "adcc64a3-5323-48d1-afcd-96bcdce32abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 81,
                "y": 18
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1135b577-d208-488d-87e5-875b675dfd53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 75,
                "y": 18
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "11e40ea7-3ea4-4bf7-977b-d5e72ff16a17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 119,
                "y": 82
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4e976587-d675-4bd4-b01c-f9946dd5a20c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 14,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 2,
                "y": 98
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 9,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}