{
    "id": "98a293cc-1a86-46b6-aff5-9c0869321f62",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bahnschrift Light",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9aabc348-d4ed-4e05-a4f1-8fd5b8bc4087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "52d664bb-f701-45ef-9782-9c07f68c501c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "dd61e072-58ca-4b79-a7a3-9cdd683c50ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 115,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "261235b8-092a-413b-b08f-fa6485b06725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 103,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3d2df032-2b4a-49c1-8edc-bbb591bcf434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 92,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5ff01848-56bf-490e-85f1-4fb727255bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 79,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9b20637a-77c7-4781-8284-d7825e9ccdd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 67,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b2ff2bcf-1b29-4af4-9558-9bc8d0b81ae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 62,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "21b42bb4-fa85-47ff-a6cd-43b807ffc264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 55,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c45d0720-45a1-429d-9a35-b7f5d276d23a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 48,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6bcfe12a-faf5-460d-849e-8ef6eb17b6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 126,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4f72371e-55c8-4784-a9ca-05f8cd92634b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c2006763-e858-44e6-8fe1-5fec90140148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 25,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3e931ac2-cc75-4e5a-9f94-df7e3c3848e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 17,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "99183e31-d710-411b-8c6c-fad6e3515ff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 12,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "08ef1a4a-70a4-4a50-bc74-fbfa2a32cdd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0221b4ef-cd5e-4b39-8e77-1dd97a357f36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 246,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4cb16077-0061-42b6-85ac-ee46e6672f50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 240,
                "y": 23
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "11c126ea-6c3c-4160-9bbc-a09de3ab3ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 230,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d5346223-689b-4bba-b295-76c18d18846b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 220,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "05aed9dc-86ea-4bc9-865e-d1db74bf678d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 210,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bbb66f51-afb3-47b0-b00f-ccff387de933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 30,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "70535633-bd7a-433e-97d9-12c98548fd9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 135,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7eed8892-3538-476d-897e-a7837254f1ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 145,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e1b120f6-e781-478f-8e58-8f3d935b53be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 155,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2a0d5fd0-e762-4d2e-a384-b87276e88f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7289c630-6b16-40f1-b4ce-873acc847f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 101,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6a117b49-32c9-4cfa-9e44-96244bc77fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 96,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1165f587-2105-4df6-81ab-a85d0cba3eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 88,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0040cce5-6557-40c0-86db-0b3b12c83141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 80,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fff58c57-3eaf-4316-8b0b-fdf978428bfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 72,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7eab9e3e-8b86-4bf0-97c4-87c9b759e56d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 63,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "84df04c5-734a-46e3-8f62-d766cf052c8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 48,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1e91845d-aedc-4ddb-aac2-e0ea92f01ae7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 35,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "722a954c-b98c-4d61-ac8f-30a8a26299df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "25e050ec-741b-4ff1-a508-e0e12da6f2dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "09dbe0e6-2af7-42d2-a92d-072a61d669f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "64e4c5cb-2a7c-417e-9725-434a9b9a3de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 244,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fe938bd0-27a5-427a-be95-13736a8e9b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 234,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ce216b58-4b54-482c-8a0c-9e1e9b5446ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 223,
                "y": 44
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d8730db3-ba89-4f91-a263-75429225cf2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 212,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f5705c9c-3eaf-4d71-8475-eb6325f96855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 208,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e9794d32-0fc1-4045-83fb-1f6480257714",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 199,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7d509558-73d6-406f-b109-40a5792b5b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 187,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c596ab0c-2333-4cd6-8128-d8b30efb6d0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 177,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "928242b5-dedb-4381-b669-33a4945fbd60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 164,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9636a2e1-548d-4a9b-81e8-af3ac8e3f246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 199,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5ae9db95-a11d-4e0d-9e2d-2e15250e42bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 188,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c61f48d1-d38b-456f-9aa1-0d36e105f2b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 177,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "96fe528d-d47b-4ceb-9164-ed74153d1a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fca51423-5e86-4d5e-b13b-35394e61a2f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ed4086fb-8496-48c6-95be-5ba5bf5789d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "37c542e4-e1ca-44a0-a24a-ea1233509026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2e2bb57e-aee5-4103-a1bf-5788124216d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fb42545f-723b-4e3c-a376-b3ae8c5a7d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "15ae9268-5e48-4f23-91f1-b92ac35581b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "36e20e3d-4146-4988-a4bf-3fe358a143e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0d224c41-ae80-41a2-8b3e-f6968df5e34a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "de4be126-1c69-4afb-8dac-8997a853c8ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1e78a635-997e-4746-9a68-7f812d7a9108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4a39a649-f548-49f6-95b8-6048beecab95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "cb62e931-cd01-43b2-9003-0d34bc53bd31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b61d9439-4061-41db-9e75-0848ba7553ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ad446037-2205-41c7-a498-b93ff20e5630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f3f96d69-0ca6-42fd-85c8-43e63b47459d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "34d800b1-280d-471b-ab37-e2fbbb0284e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "827d9602-a5f7-4caf-b744-9751fe43f2a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "77a5b5f0-fd34-4889-98d8-ab2a59587fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f403a21e-d77e-4bb0-983d-17ce76f99f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0ad24baa-a8d0-459d-98fb-c4d2563b13f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "418f5584-f571-4bba-afdf-295e5798cb94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ea4459a1-bbe6-471a-8e0f-81ea00b8a1ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7c7512c4-0f98-4540-98da-a96392b2e645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 71,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d4747f08-9089-4b54-94fe-25a9024c0ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3e372964-a3c3-4cf9-8c4b-a5f9e301ae55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 161,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5eb81eb0-6d63-4fba-8419-768ebd90a42f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 151,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8161e9fb-4384-49be-aad3-40809b92fa53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 145,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fd7b7c47-f0ab-4125-a2f9-fcec4655115c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 131,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2b5fc86c-be6c-41fa-8f69-98ba1af89b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 122,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d45ba19c-05f9-468b-8bd2-e6d08e175c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 113,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "15226157-f6db-4c37-8628-c6c1c2483fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 104,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "05d03cbb-8a54-480a-9250-fe6660402ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 95,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e63c909a-4533-4405-8b8f-eb7b38d8d2ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 87,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "fa54fcc5-b52c-4263-8c0e-8ffeede9ad77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 167,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6d9634c4-f055-49b0-a477-866153ad1365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 80,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "cb0c1c44-5df8-4053-a79a-60faf02255b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "246fdbaa-6b50-4e2c-83df-ac404021d8f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 52,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ebce5bae-74e5-4d9c-af5e-935dd0ed1934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 38,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8abbdacf-5b87-4820-8b71-cdbf2f54b706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 28,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fe134390-f97d-462d-bca7-e2bee203e1c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 18,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0304c41b-e61c-4bb1-9634-eedec32803ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 9,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "98aab438-5b59-4bf4-b47c-c7c5e29b8e1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f26e0fa4-0473-4047-b281-810b5ac8ae5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 246,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "10a6733d-85f3-49f3-8070-9bd768430e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4cad4100-2ffc-4af3-b094-434acdf4016c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 116,
                "y": 65
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b7e37933-4b22-44bd-a682-d6e46b861c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 125,
                "y": 65
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}