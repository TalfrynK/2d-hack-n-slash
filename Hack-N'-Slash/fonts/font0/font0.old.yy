{
    "id": "c0eaac6e-9a9a-4c04-9d77-6eacd265d211",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font0",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a65138c6-9def-4b87-9234-910d5829d831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3a57f062-bf51-4cf4-9fcd-41be0073f789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 176,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "570db5cf-1829-4ed8-8763-9948a72064a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 168,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ccfdd1c4-9262-4b1b-81df-7fb72b38edb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c89a27ea-1d96-4d08-95ed-c8b6c5930de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 146,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "69ef5842-2efb-4469-9ce3-2c11d8a8c14b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6c36cd2a-38a6-4f59-92c0-f8e04193df17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9029714c-fdc2-48bd-a78b-d06d95564c21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 119,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "958150e3-65ff-4419-810e-789c47ca14ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1cefb16a-c31f-42b4-966d-4c916652b79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 104,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0f436f0d-7514-4664-b583-33a861fdd6e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 180,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "343ca20e-f59d-45d8-8bb3-a14d1164ab99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 93,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d8b28217-0f09-49e2-8c40-04c3462a02ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 77,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "19356260-9f3b-411b-9a82-2f4b7b80bbab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 69,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ba9d39a3-225c-4813-9f0e-02c519c23541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 64,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b2378b74-af74-43bf-8c3b-03c781fb1e4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ba803c79-9afb-470a-b29c-bd60671cb1d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 43,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d34854f2-3ffc-430d-a646-303c2edabb75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 33,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "74add4c2-6912-4aa5-bfb2-6bb10abd09db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 23,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "81bc6a5d-c2ed-4f84-bca5-bf204ad490cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 14,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f5ff449f-8d62-4907-b23f-4816b05c0650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7e69a5e1-76d3-4449-a722-3c1ba281a53f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 84,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "95cb7c25-1e84-4ea5-a016-118b791fc8f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9d211559-6e87-4e94-8c01-9e61aa9341ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 200,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3390a0b5-2532-4481-a4f3-fcc789d345d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 210,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bd2acda4-8dbf-42f0-89dd-10f953606d11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 161,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5e333d95-100a-4062-a1b6-58da71eeef15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 156,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d6dfd421-e7a7-4224-a5ce-c85c7dca6231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 149,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3a713cce-4922-4b17-9642-7cbb3f117d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 139,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f39e85f5-cfa4-4da6-9653-7a24e0aef4fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "dfac8eb0-e5f6-4f9a-a238-966cfa3b66c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 120,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "429f104c-aa31-4b0f-8e55-36a815cae2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 112,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "50dfc83e-8840-4845-b621-2c80c0551cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 100,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d3f51736-3fb5-4e97-a253-fced75c10c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 88,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "dfa5c949-eeb4-4802-a0c3-496fbf231e70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 78,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3fabe0a3-7d2b-4db5-b2f0-540a52f97f92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 68,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a8fc1e54-caed-45f4-a0f6-08c0578ba09c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "69c0b853-351b-4016-8118-c2df7aa9e420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 48,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8aab17df-91a9-4d1f-9d2f-2a3d79f0dcae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 39,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b2919678-8fbd-4dcf-ad54-3c687e23a92e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 29,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f6ff639e-ece8-40ff-90d8-c70064eafffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 19,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8d5ebc5e-d4ed-41b9-81b7-28f5bc14939d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 10,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5107ff84-4d8b-41b7-80b7-e3a467653652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1a01765b-de4c-4890-a007-363e807471ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 241,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "aa5f039c-6a21-45d1-b047-80f737e050d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 232,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "bb7d6a66-a1b9-4e4b-87b7-80e2a98f551b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 220,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "48bc061c-8bb4-43a4-b8ae-b429ea284f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 236,
                "y": 26
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "648e7f05-4a02-4958-9ad8-01ff8f2be1f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 225,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c3fa1b3a-69b9-4335-bcb2-4e312f334617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 215,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5489f029-d268-4f47-b083-3927e063c373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "da99f087-555e-43b3-b904-b80080f653e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ee19ceed-545e-4e85-898c-ed704ae99a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b3be928f-4d01-4df2-a6a5-1737bc9ee2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5e3c62c9-db5f-4531-8ab1-a794125ed65d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e3daf6b5-c5ad-4094-a4ec-0b4d2df67871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "780d66ce-7014-443c-9df1-d9b7f89db0a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0dd057c7-a97d-46e8-970f-efd5f0dfd555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0857f141-c209-411a-9ee7-9e1bd34b2238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8fa47550-c361-43d4-b033-6bc3650d6b65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cca68cee-dc65-487f-a9ad-443eca6893f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5e3137ad-2303-46ce-b39a-997a7371d6dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d6500f70-ef81-4a26-9589-4ec6b71c0414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f5e70451-faf7-483b-9e1d-af11e41fe417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b2d70397-2fe2-4f13-97ea-a824ff9adc7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1917aeb1-21e4-4272-a4d5-6a6f2ae66dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 6,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1d846eb3-d929-4460-bb96-4d04d675ea8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0ec8cc2e-3df3-4a8f-aceb-2be8645e8be5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "73baf3a2-953c-400e-9f5b-44ef4d96b844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ba0b325f-3032-4eec-8f02-cafc38b11475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9f712a49-668c-417e-8172-453a71c1ed6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3aa69316-5db6-4a92-a114-fb3d68d84db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e4f0bced-66d3-41d3-a29c-b73e9c211c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1ecb8bc3-fe8f-4088-912a-2ddd7c92b1e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 96,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "27832d1f-d270-40a6-92cf-d72c8b5b7e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "08cbbf39-1a0b-4ebc-bbad-2791a420e152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 197,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "10c1ce44-15ee-4ff7-9371-53bf8e29996a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 187,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "decd0e8d-f28d-4ad4-959b-24fade2d0976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 178,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e69ac190-4690-4024-878a-115c8e57171a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 167,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0af82a25-2502-476a-b25a-db71e0f32149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 157,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2f317068-f56c-4062-85e5-d9a73d1c8ce1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 146,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0963d03a-2b4d-496b-9550-2cacd27a081f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 136,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "059d336b-ef59-4bf7-a723-badd4e895665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 126,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "dad31bab-169e-441b-b4a8-455e8ff67125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 116,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1f9ce1ae-3b3d-406d-97cf-6fc861191075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 206,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c29b4dff-05ec-4f3c-a7c0-fdb4b2bd18b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 106,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e2a5f681-717b-4007-a5f0-08be6c20384b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0576a9b4-35be-423d-b7b8-0b1237574461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 75,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f491a18c-4b5b-46e1-bd50-388eeeb61aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 63,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d4d46a78-0e2f-4384-9277-8c351aa088dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 52,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "82cae18b-29a1-4a09-83b6-936ab83a7db6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 41,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a002374b-614d-4f5b-9caf-c52ff17091da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 32,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "31bf446e-d103-405f-963a-c8ec537cf653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 24,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "646d674c-7fdb-4082-ab86-845943f1aaf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 20,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8696d413-14aa-4b6a-92d2-3198a75f0a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 11,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6eeadc95-896d-42a4-af41-46ec6dcb8a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 171,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4a9699a9-29db-491f-92a6-fc59ff16b2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 182,
                "y": 74
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}