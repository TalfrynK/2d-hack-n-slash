{
    "id": "c0eaac6e-9a9a-4c04-9d77-6eacd265d211",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font0",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "be09bcce-99a8-4a00-a9c2-ec5f7756402b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7dcd9ce8-f65f-43cc-8e3f-d48430a9c3a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 67,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "49be2f99-6478-4add-9080-911473f4979d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 58,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "151a2740-8650-4dd0-a2b8-bb7f623e48ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 92
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c745dbdd-fbdd-4beb-a6a9-f0c6a5470b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 31,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e94c967c-3d2f-425c-baf6-27efbc4987fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 16,
                "y": 92
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3268763b-a6d0-4e4a-936d-b9c0d12ef95b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8ed02be9-f7b6-4180-a491-60d9beac3445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 246,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "250e9d2e-c709-42c8-b4cd-ac8c6cf50b13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 6,
                "x": 238,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "597b5b87-cfa7-4de3-a0a4-2f0ce7ba8835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 229,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4f3b084a-e894-4b79-b7e9-13feb4a365bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 72,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3140dc66-75dc-4665-a629-18c859037ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 216,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5e6f3ffd-22b0-44d6-b3a9-38870a147ad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 5,
                "x": 198,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c89356cd-9026-4083-9913-470b6ad2ee58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 189,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a0c10132-3d85-4f20-a36c-a4d2585e05c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 184,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9d4fd510-a544-451e-870d-d24a942ddb7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 172,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d0c681c6-853a-47e3-a222-825775d79c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 159,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a375ec1e-2707-44c8-ad38-787cbf384402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 147,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ba80ef21-7c46-4c87-98ef-62d80aadcfa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 135,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2581fbda-41f7-48e7-9afd-c1bd605d0f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 123,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bbd8fd42-edac-4976-b14e-b9e6921f36f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 109,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fea221d5-880d-4c7e-8d25-22a2c9e670b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 205,
                "y": 62
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8ceb6214-4fd3-42ab-82e0-f1e3a9915233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 83,
                "y": 92
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1a08a0fa-2af1-41c0-bcc6-d17293cd8d3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 96,
                "y": 92
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4483e281-c3a2-4439-835f-3ff9c7d5b50a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 109,
                "y": 92
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "dedc1cdb-342b-48b8-bdc1-6c080a02562e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 101,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1d9e09ed-f112-4f61-acfe-b7083a8d5659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 96,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e600c73d-1d43-426f-b81c-3ef7a700035e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 88,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f05c0b31-4fcf-4680-8a46-8f5e3a6ee01b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 77,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e996c13b-4157-45ad-8148-5491e21d30e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 65,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b84e6b73-87e9-4b38-a128-205971dfe4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 53,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9e21e168-7d4e-4353-bb9a-5a1992c6651b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 44,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "950ed7d0-39d1-45de-a297-e6073fe64aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 29,
                "y": 122
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d35cb92a-8820-4d83-9bf3-1ce006affdd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 14,
                "y": 122
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "79f6f85a-50ce-4647-9d8f-831a038e4fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a34d07b3-3ddc-4a26-8884-0d5dcb1f372d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 241,
                "y": 92
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "12604065-a228-43be-9078-f05e42cc2ceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 228,
                "y": 92
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "28cefede-e0f3-4c27-b5a4-f377fc2d8222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 217,
                "y": 92
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b09e803e-17f8-4185-bb99-3f10f5ef4cf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 206,
                "y": 92
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e461536c-665d-4fae-8ed6-1b21eeeb8644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 193,
                "y": 92
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6270e550-e623-43a7-8f9c-ea511b989b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 180,
                "y": 92
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8c8435a5-3b2f-4232-abbf-a938b9816bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 169,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "00c43dfe-8235-495d-bf44-b29942b99df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 159,
                "y": 92
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6595cf1d-fb1a-4268-8dad-530225698b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 147,
                "y": 92
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d58c088b-0592-47e2-8c1f-67bae118115a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 136,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8864d06b-eb2c-47ce-9053-a96fb9fb5828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 122,
                "y": 92
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d99f7b6c-993d-4cc7-93f6-2629220f6c41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 96,
                "y": 62
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d4b063c8-7954-4363-aa8d-28eb2948439c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 82,
                "y": 62
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ec4c654d-afbd-406c-bb0c-953959f6824e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 70,
                "y": 62
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8c2efafe-a75e-49a0-b702-8d68dcc182e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 35,
                "y": 32
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d8fe908a-1034-46e8-94a8-9a0ecc222328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 15,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fdf7e973-4f2d-45b4-bbd4-ad18b00ee56e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "be23ee16-93f7-49e9-8f29-64b434b32d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "75c1ee8d-6ac5-4201-8b6b-b5abae019658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "543b6ef0-fd2a-4c4d-a4c8-95b9f5af555d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "47e23da7-72e2-47f1-964e-94996391d0dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a9274c78-ee25-4e4b-a59e-8cc72a575767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0ff63247-0ab3-490c-b851-70be98bf8637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c2d645ea-1a27-4da0-8a31-38fd3b2f77ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4db7dea2-e3d9-41fc-b215-2cf9c2bd17c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 6,
                "x": 27,
                "y": 32
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "10da6112-fc45-4c96-934f-7e563dfab0de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "823c43e6-4258-442d-ae9c-c07d7b069f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "194f4231-ab03-467c-bfa1-3803a90d2260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e08fc91a-18dd-4a91-9bdf-cf06daa8c97a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "177fa963-b6f4-4169-9f48-7b252c8ede94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9c4d2bb4-cb8a-4925-8989-8ac5ad421d92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1fc37545-31e9-4437-9129-8172d60d8c42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ee5878cc-ab45-4555-b004-494f7a9e9e4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "578e2747-7f22-40ae-b503-b6980279c194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7da559e9-61ca-4af2-9de6-54ce83ed631f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8cee46b2-ecc0-4248-982a-711fe06d41e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8c6ada1c-70f7-43dc-ac2a-f0110f8c5b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 49,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fc001505-3942-4fa3-a91b-05ac239406e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 174,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "11d645c8-a9f1-467b-8e01-0296a4c7bea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 62,
                "y": 32
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6c744922-64ba-4047-bd55-69772a895f52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 49,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "140328b6-935b-4309-bb73-a1ac8cbb33e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5e6d7ad1-d7a0-487c-8908-535ecfee251d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 26,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3b7af509-b3d1-4bd0-b4db-84d2ced428c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 13,
                "y": 62
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f7883595-39a5-41bd-9278-3072074ceade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3c76810b-03f6-4a03-8735-023b5da65876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 233,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "285f6630-bf62-4ff7-9bf4-f632e4c8d0d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 221,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c21d305d-4ea9-456c-b9e1-7f9f60133f1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 209,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "dcf1b45f-aa3b-4b5b-864b-cfad58be7b8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 197,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1371fa1c-338f-4925-9270-781ebfee4a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 59,
                "y": 62
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "eec420e9-a537-4ca1-9386-44c49d3a7f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 185,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9587b964-8fb9-4804-b4ee-d46bc1188be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 163,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e5de3187-9eff-4869-bf5c-436b05bd7cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 150,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b13ecded-52da-480d-ac29-1778df1a4629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 135,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0b73a430-3305-4f21-b909-df38ebe54ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 122,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4bc38af7-9719-45f9-85ec-c801f43f6866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 109,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1d44850c-7a49-462a-9320-109e702e0473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 98,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "eac3192c-1ae1-44ab-9876-4e1ecb2bcddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 87,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d0ed9a64-28c5-45de-9ec7-e4a6aa7e7cea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 6,
                "shift": 13,
                "w": 2,
                "x": 83,
                "y": 32
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "223a161e-5a99-4b9c-8e32-a6b5438e9413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 73,
                "y": 32
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "13776e9c-e1ba-4ee0-86ba-0b31c359c2c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 114,
                "y": 122
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "f3e0ca08-d55d-48ab-83e8-783430fa441d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 28,
                "offset": 5,
                "shift": 23,
                "w": 13,
                "x": 127,
                "y": 122
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}