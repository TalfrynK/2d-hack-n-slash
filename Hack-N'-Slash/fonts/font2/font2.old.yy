{
    "id": "fbce5bb3-472d-43d1-883e-d98c8fde086e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font2",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Century Gothic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5e1e3e07-7259-4293-81d1-163b605b2194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 39,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9331370e-b422-419c-bdc1-8267229491c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 39,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 145,
                "y": 84
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b30d47a4-039e-4877-a572-b1161abc3e61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 39,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 133,
                "y": 84
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cc095c36-1299-4bdd-92b3-6a67af6b7669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 39,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 112,
                "y": 84
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c0f28d63-114a-412c-b359-5b3a778df325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 94,
                "y": 84
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c62eaadb-06ca-47b9-8a20-343864cf4530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 39,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 67,
                "y": 84
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f3f327d5-2ad2-4b51-8b19-fe24117690dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 39,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 43,
                "y": 84
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cb353c72-91fa-4a78-8497-aef942f750a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 39,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 38,
                "y": 84
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5f5683ff-1177-4ab5-b5bb-d90d98aab87c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 39,
                "offset": 4,
                "shift": 12,
                "w": 6,
                "x": 30,
                "y": 84
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b728d240-7b7c-4127-9d7e-a032c6f176c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 7,
                "x": 21,
                "y": 84
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f9d4f241-24c2-43d2-8692-668e2002782b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 152,
                "y": 84
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0d968804-7370-47cd-95ab-a13834ed60a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 84
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b5dfbfe9-3992-4a20-a484-240677c1a1b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 39,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 483,
                "y": 43
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6de6c246-ba98-47c1-b640-50534743d58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 472,
                "y": 43
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ae8571e6-d569-4b79-88f2-8a13ff0b6fea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 39,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 465,
                "y": 43
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7ecdfefc-6c02-450d-8921-715eb927e63e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 450,
                "y": 43
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "daea5007-b278-4852-867b-e1788d2761a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 432,
                "y": 43
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5afe5bef-c104-4f21-bb46-a2d8aad9d2ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 39,
                "offset": 4,
                "shift": 18,
                "w": 7,
                "x": 423,
                "y": 43
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ba6fb61e-3130-4b8c-b1b4-f3d0516a3b4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 404,
                "y": 43
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "891fc151-fa20-4973-a8e0-56db8c890354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 386,
                "y": 43
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9f9bdccf-05e6-451b-b85d-b989be5bd850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 367,
                "y": 43
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "dba8e8b7-fcbd-42d7-9793-d0e47c00ae1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 490,
                "y": 43
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a3fe869e-3098-4335-a1d9-8a0f51a9f38c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 39,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 166,
                "y": 84
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f03c33bc-b6a5-4cf9-802e-347236a5e4b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 39,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 183,
                "y": 84
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "653bccfe-bfda-4cc6-945f-dd2fac5d7264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 200,
                "y": 84
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "95b77477-8dfb-44ea-af29-e88226f5278a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 39,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 90,
                "y": 125
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "18417424-104c-403d-b265-07c978d7ba58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 39,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 83,
                "y": 125
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2a33eae7-bafb-4ed3-ae6e-4b7df253a202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 39,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 76,
                "y": 125
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e4f91bf7-3026-40b6-a77f-60555a51b2af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 57,
                "y": 125
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b69d03a8-537b-4e7a-9c89-2c6734922006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 38,
                "y": 125
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "225987c2-6bec-4120-b423-e3c42441d7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 19,
                "y": 125
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5eba2ee2-40c1-4cc3-8497-1978d37e7117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 125
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "04d9b844-57e3-47ff-b1b0-297ff6c1cebe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 39,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 469,
                "y": 84
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5d1d5971-6076-4569-8b67-34149be56364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 39,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 444,
                "y": 84
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a20585e9-50e1-4bd5-bc85-182e041a532e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 39,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 427,
                "y": 84
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "30ffb152-48ea-44d1-9ebf-be3086f5d657",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 39,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 401,
                "y": 84
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9290d099-feed-486a-a748-3155561e9f83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 39,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 378,
                "y": 84
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9b3ae4ad-4cdc-490b-a801-b05668af30db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 39,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 362,
                "y": 84
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "66afb352-c75e-4d5e-b59d-111745639782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 39,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 347,
                "y": 84
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9efde4ff-0050-43bb-aed4-886cc316e222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 39,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 319,
                "y": 84
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "835678f1-bfd7-4c16-9271-033076a71e92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 39,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 299,
                "y": 84
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "cf3155f6-3afe-46e4-a53f-35f504eeb703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 39,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 294,
                "y": 84
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c453d6fb-98a7-4735-b8f4-18f16e508b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 280,
                "y": 84
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "03b6140a-14da-44a2-a588-df0b2120e42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 261,
                "y": 84
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "93b41613-2804-4908-80a2-02e268ad3ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 247,
                "y": 84
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "743b71c1-3158-4062-8d67-dc1a617f66c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 39,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 218,
                "y": 84
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0750e4c8-af20-41c5-9085-426c57dd358d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 39,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 346,
                "y": 43
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "63d6c68c-989e-464c-a03c-c79fb02d6396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 39,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 319,
                "y": 43
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "dd578c50-cd18-4273-a9ab-6be2d65e39dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 301,
                "y": 43
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5aacae4e-652e-4ef6-b1ff-5b871c5aee36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 39,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4d395242-7ab4-4595-80f2-0d486e5db040",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 369,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3e3885e5-15a1-49cf-b0b4-3d79b27fa709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fd0c1bb1-a546-40a9-ba50-4cdbe753e692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 39,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 337,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f0107ff5-3fe5-40aa-8b64-a748de8856ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 39,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "84ac6da5-4a40-4200-9a8f-68b71568c511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 295,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2f94a7f6-1516-47c1-87fb-89c83bb006d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 39,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "502e4818-acc4-401e-9416-0de99d1f9397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3cb9b696-0fdb-46bb-9994-5e0ab8de12f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c1d34e06-3785-4d73-ae0a-e7cbf4f7cb7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 39,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4c339fd3-120d-49bb-8849-f49097b3854a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 39,
                "offset": 4,
                "shift": 11,
                "w": 6,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ca1c9046-439f-4a76-8d24-028fc710efab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7055aaa3-0d37-44e2-996f-cae8ef85639a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 6,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "52c692b3-c1b6-495d-8dc0-9f1ec4105da9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c1914b59-2f1e-4341-885a-65aed1cca348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 39,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0e871cfc-0b86-47c0-9c7d-fb72906383a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d28c8c23-5169-4a7c-b2d2-8637fed23ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f732c4cf-05c1-42fe-8db9-1d2d7cbf4b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 39,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c9627dd3-9a0d-4afd-81fd-216751d4abd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 39,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5c99181d-7022-4722-88ae-aa061fd672ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6ad51804-0519-4413-9552-766a183f7d84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 39,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4b7a3105-ef19-4f36-b6cf-d5007365de28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "41c621b8-82b7-459d-9a15-a26e0dc295f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 423,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b79c50b5-f5c7-4ef4-ad6c-48e9b165359c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 106,
                "y": 43
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a0caa469-d6f9-4466-9019-11c103853a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 444,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a615e804-b1fe-4372-88da-5b0ea556c569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 39,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 279,
                "y": 43
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7a3a0986-c7ee-44b8-abd5-1a3de2ec2b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 39,
                "offset": 2,
                "shift": 16,
                "w": 15,
                "x": 262,
                "y": 43
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d44f7740-b56d-4c92-b3cd-1a0c98060617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 39,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 257,
                "y": 43
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e56787f7-86ea-4630-ba26-6f5231970f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 39,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 229,
                "y": 43
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f64dca95-ffb6-43fd-975f-ca2c1f6802a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 211,
                "y": 43
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "cb09b29d-77cd-49da-9d8d-48617a98b015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 39,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 190,
                "y": 43
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "385f7994-0f70-4749-8b54-3ff9e0a5f227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 39,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 169,
                "y": 43
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "70edf017-61d6-4a4c-853c-5c3a3bd26478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 148,
                "y": 43
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "da1da19a-4511-425d-bea2-19214cc0eb41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 136,
                "y": 43
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a6a1f9f2-161c-4001-aed6-acee12c49c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 288,
                "y": 43
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "538684a3-8f7c-41dc-aae5-a79bd28867f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 124,
                "y": 43
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e9a62e74-b766-4fc4-a904-b7dba8ece957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 88,
                "y": 43
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a3ff6ecc-c030-488a-9c16-0d7fe38083cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 69,
                "y": 43
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "998d0da6-ba24-4449-baa5-bf1f1c06b712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 39,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 40,
                "y": 43
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "587c4731-1369-401f-949f-5a9cf39814cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 39,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 21,
                "y": 43
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7095dda5-f108-49bb-8381-2255420074b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "56b24b1c-8030-44a8-bc6c-2c04a5d505e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 39,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 480,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3c95e012-53b1-431a-8cde-217411212ef6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 468,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3b77ab8d-4cf9-466b-989b-64f009779afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 39,
                "offset": 9,
                "shift": 22,
                "w": 3,
                "x": 463,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bedebba8-4689-4414-8665-2c13307c16bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 451,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6ffcb67d-4836-4584-9e71-a01ae7c4fa6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 107,
                "y": 125
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "0aa79ea2-a5df-4db4-800e-3489733bbf77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 39,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 126,
                "y": 125
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}