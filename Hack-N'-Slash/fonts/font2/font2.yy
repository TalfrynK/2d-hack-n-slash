{
    "id": "fbce5bb3-472d-43d1-883e-d98c8fde086e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font2",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Century Gothic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b62b284f-cd14-4a79-afd4-79222632f249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 105,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "30a0d07a-6131-4065-a3c1-ccb22221c1a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 105,
                "offset": 7,
                "shift": 25,
                "w": 11,
                "x": 713,
                "y": 216
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d2eedc22-f583-4baa-a07b-7632395660e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 105,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 688,
                "y": 216
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b1b875ed-0c85-471b-bc31-87428512c5fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 105,
                "offset": 7,
                "shift": 61,
                "w": 47,
                "x": 639,
                "y": 216
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "32d0f4b2-2b6f-4cec-b583-789188f88197",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 105,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 594,
                "y": 216
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3e59873d-2b67-4c54-b1d2-e21d79bc569a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 105,
                "offset": 1,
                "shift": 66,
                "w": 63,
                "x": 529,
                "y": 216
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "050f248c-b9be-4179-b927-bf060c805aac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 105,
                "offset": 7,
                "shift": 64,
                "w": 56,
                "x": 471,
                "y": 216
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5ca234cc-c441-4e65-96b4-1a08ab798790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 105,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 462,
                "y": 216
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5dff41ed-19a7-4523-9cd0-5430a3d89590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 105,
                "offset": 11,
                "shift": 31,
                "w": 16,
                "x": 444,
                "y": 216
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "11bf51d2-d1a7-4863-8032-594e44f24c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 105,
                "offset": 5,
                "shift": 31,
                "w": 16,
                "x": 426,
                "y": 216
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6a3cff1c-06ed-4743-bdbe-c36396a0238a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 105,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 726,
                "y": 216
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ee6a41b4-70c9-44f4-95e7-8a02c07b837e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 105,
                "offset": 4,
                "shift": 52,
                "w": 43,
                "x": 381,
                "y": 216
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6242b3b4-1967-423d-9210-a5db78a92b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 105,
                "offset": 5,
                "shift": 24,
                "w": 13,
                "x": 323,
                "y": 216
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "86e1e3aa-34d7-4019-8634-f17755eec80b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 105,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 297,
                "y": 216
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1b9c2929-ec27-4119-b6f5-cb83f7567809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 105,
                "offset": 7,
                "shift": 24,
                "w": 11,
                "x": 284,
                "y": 216
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "99de9024-852d-49e3-b5e2-95849c4e1221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 105,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 249,
                "y": 216
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e07989fe-8caf-4a98-b623-71b8fad798ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 105,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 204,
                "y": 216
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "29bfbbee-e033-4fd6-8cee-d08e89536d4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 105,
                "offset": 12,
                "shift": 47,
                "w": 16,
                "x": 186,
                "y": 216
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a7a1e61d-a9e4-4924-a865-8e652b5a637f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 105,
                "offset": 2,
                "shift": 47,
                "w": 42,
                "x": 142,
                "y": 216
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "868058ff-54e1-40e5-a729-4a585f7770bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 105,
                "offset": 2,
                "shift": 47,
                "w": 42,
                "x": 98,
                "y": 216
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "784e5986-9831-4a50-8bee-82e90c7470a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 105,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 53,
                "y": 216
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5d1f2831-f1ba-4e6f-961c-a344006dad3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 105,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 338,
                "y": 216
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f9c01de0-eb9d-4de6-b8a2-6689dd92136d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 105,
                "offset": 5,
                "shift": 47,
                "w": 39,
                "x": 758,
                "y": 216
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "59df12e2-3fc2-42f8-9166-53bd7981ea59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 105,
                "offset": 6,
                "shift": 47,
                "w": 39,
                "x": 799,
                "y": 216
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5593cab2-97a3-4d8b-8623-2efe54c0f774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 105,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 840,
                "y": 216
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b3b4369f-4ccf-4e32-92ce-bb34c45ee8a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 105,
                "offset": 4,
                "shift": 47,
                "w": 38,
                "x": 760,
                "y": 323
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "afb8d387-ee39-40c5-83af-b083e666eade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 105,
                "offset": 7,
                "shift": 24,
                "w": 11,
                "x": 747,
                "y": 323
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "650d911f-394d-40ce-b3f7-e2c02ab285e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 105,
                "offset": 5,
                "shift": 24,
                "w": 13,
                "x": 732,
                "y": 323
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "dddefd91-2148-452e-bbb1-d916e702c8de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 105,
                "offset": 4,
                "shift": 52,
                "w": 44,
                "x": 686,
                "y": 323
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6fc634ca-4d16-4aab-980a-5902ccdb9436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 105,
                "offset": 4,
                "shift": 52,
                "w": 43,
                "x": 641,
                "y": 323
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "49d1772b-c3b5-467d-a97e-4d2ed67c7a4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 105,
                "offset": 4,
                "shift": 52,
                "w": 44,
                "x": 595,
                "y": 323
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ce262ff2-4a38-4c5e-8d9d-464ad1daadd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 105,
                "offset": 7,
                "shift": 50,
                "w": 36,
                "x": 557,
                "y": 323
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bef820c7-c99a-4b3d-9b0b-b3b3517730be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 105,
                "offset": 5,
                "shift": 74,
                "w": 63,
                "x": 492,
                "y": 323
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e89545ae-28ca-44b4-9858-fdb7c47c6a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 105,
                "offset": 2,
                "shift": 63,
                "w": 59,
                "x": 431,
                "y": 323
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6158eedc-20e6-4a06-86c0-4c85b169f85e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 105,
                "offset": 7,
                "shift": 49,
                "w": 38,
                "x": 391,
                "y": 323
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5bab2452-5709-41eb-a8bb-da736f6ba226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 105,
                "offset": 4,
                "shift": 69,
                "w": 61,
                "x": 328,
                "y": 323
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e8c2f35f-e5fe-4107-9be7-c8b10d589eac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 105,
                "offset": 7,
                "shift": 63,
                "w": 52,
                "x": 274,
                "y": 323
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3a119249-5f14-440c-a3aa-a1c4bd630c56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 105,
                "offset": 7,
                "shift": 46,
                "w": 36,
                "x": 236,
                "y": 323
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6d4e386b-8f9c-4063-bec9-290e8d92d2e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 105,
                "offset": 7,
                "shift": 41,
                "w": 32,
                "x": 202,
                "y": 323
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "dd6a55d2-a6eb-4c9c-8b49-c786997b8b96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 105,
                "offset": 4,
                "shift": 74,
                "w": 66,
                "x": 134,
                "y": 323
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7fcef4c4-71a7-48a1-a00d-20f859ce477f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 105,
                "offset": 7,
                "shift": 58,
                "w": 44,
                "x": 88,
                "y": 323
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "94a4db0e-df39-4e52-a171-465f0f346842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 105,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 79,
                "y": 323
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "62fc01fc-d5de-4377-a810-38748e95ce84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 105,
                "offset": 3,
                "shift": 41,
                "w": 31,
                "x": 46,
                "y": 323
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d989090d-d2db-4dbf-a8f7-3acc296686e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 105,
                "offset": 7,
                "shift": 50,
                "w": 42,
                "x": 2,
                "y": 323
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0e4e6cd2-c3b7-4298-8d87-7eba531d6d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 105,
                "offset": 7,
                "shift": 39,
                "w": 31,
                "x": 957,
                "y": 216
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ac5c6f73-ce24-4251-986d-e2e6000203d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 105,
                "offset": 4,
                "shift": 78,
                "w": 70,
                "x": 885,
                "y": 216
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1ffffcef-279a-47f0-bfa4-f900dac9e7a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 105,
                "offset": 7,
                "shift": 63,
                "w": 49,
                "x": 2,
                "y": 216
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "19115175-1196-4881-a733-4a7bc5965ec9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 105,
                "offset": 4,
                "shift": 74,
                "w": 66,
                "x": 913,
                "y": 109
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "622741cc-d58c-460e-aaf8-72cb2d6cee27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 105,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 872,
                "y": 109
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8ea88a22-321a-40a6-b36d-09f027c38db0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 105,
                "offset": 4,
                "shift": 74,
                "w": 66,
                "x": 930,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "50f4c2e2-74fb-4844-b46c-de423787723d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 105,
                "offset": 7,
                "shift": 52,
                "w": 40,
                "x": 872,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5546f7fd-4565-465a-875f-166ecaa4875c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 105,
                "offset": 1,
                "shift": 42,
                "w": 36,
                "x": 834,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fc15dcd8-1a79-48ba-8c8f-d57e44c10950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 105,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 798,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "23409e94-f8df-458f-af66-4eb11d653c22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 105,
                "offset": 7,
                "shift": 56,
                "w": 42,
                "x": 754,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "23390182-5a4a-4d68-a382-6a933a79fcac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 105,
                "offset": 2,
                "shift": 60,
                "w": 55,
                "x": 697,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "74a2f795-dc1f-4ebe-88be-a89811fc1121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 105,
                "offset": 3,
                "shift": 82,
                "w": 75,
                "x": 620,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "726e37b8-cfe3-44eb-9fd1-3ad583c7602d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 105,
                "offset": 2,
                "shift": 52,
                "w": 47,
                "x": 571,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "632ba494-d42d-45cb-9473-7f8d284761f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 105,
                "offset": 2,
                "shift": 50,
                "w": 46,
                "x": 523,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "35946c10-e497-4e4e-8d94-a2e021293c6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 105,
                "offset": 2,
                "shift": 41,
                "w": 36,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "02c79698-6c23-487d-9219-e6eb4d339da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 105,
                "offset": 12,
                "shift": 30,
                "w": 14,
                "x": 914,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5d7a469c-9b5b-4b48-a9a8-2bed4645d880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 105,
                "offset": 2,
                "shift": 51,
                "w": 47,
                "x": 436,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1c6698e6-1775-4f79-8c62-7f0fc1c94fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 105,
                "offset": 3,
                "shift": 30,
                "w": 15,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5e43d924-c3c6-4e7f-b2f4-499be1396f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 105,
                "offset": 4,
                "shift": 57,
                "w": 49,
                "x": 343,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7c436fd2-cadb-444c-b31a-6dad7047bfc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 105,
                "offset": -1,
                "shift": 43,
                "w": 44,
                "x": 297,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8057f8f1-4c2b-4986-be44-3ef0123d296b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 105,
                "offset": 4,
                "shift": 32,
                "w": 19,
                "x": 276,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3c1d6aea-eae0-4308-b2eb-c63ac968f949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 105,
                "offset": 4,
                "shift": 58,
                "w": 48,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "40067638-743d-4a5f-bd4f-c454d9b684f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 105,
                "offset": 6,
                "shift": 58,
                "w": 48,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e4f5e68e-a668-4144-bbd6-546869165387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 105,
                "offset": 4,
                "shift": 55,
                "w": 47,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2aee8502-6594-4475-9cdf-b4ff202f78c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 105,
                "offset": 4,
                "shift": 58,
                "w": 48,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9bbb8b6a-44da-45da-9119-68d80b6c1f05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 105,
                "offset": 4,
                "shift": 55,
                "w": 47,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "81a031ae-aebe-4201-b469-ada4fe9f5988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 105,
                "offset": 3,
                "shift": 27,
                "w": 23,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7c75b216-67fb-4563-b535-59fa7bfac95d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 105,
                "offset": 4,
                "shift": 57,
                "w": 48,
                "x": 2,
                "y": 109
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8937e684-3013-46bd-aa53-d3f6cc384b77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 105,
                "offset": 6,
                "shift": 52,
                "w": 40,
                "x": 418,
                "y": 109
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b8b57f99-6f71-4169-bfcd-3839153ae3db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 105,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 52,
                "y": 109
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b6079557-b6c3-4351-b3b8-0ba369999569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 105,
                "offset": -3,
                "shift": 17,
                "w": 17,
                "x": 823,
                "y": 109
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ef3e9c00-b30d-4390-a110-93f3a5ae5254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 105,
                "offset": 6,
                "shift": 43,
                "w": 38,
                "x": 783,
                "y": 109
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5162a43f-8d18-4e80-bfbe-39eda0eb27fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 105,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 774,
                "y": 109
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8a87f43d-9889-4b5d-8947-b4a67d1a3cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 105,
                "offset": 6,
                "shift": 80,
                "w": 68,
                "x": 704,
                "y": 109
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "de14e6fb-3d56-4e67-8f03-fc8c597b5988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 105,
                "offset": 6,
                "shift": 52,
                "w": 40,
                "x": 662,
                "y": 109
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c11fab97-f526-4aee-8480-5b9248672e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 105,
                "offset": 4,
                "shift": 56,
                "w": 48,
                "x": 612,
                "y": 109
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7d4f3ead-2f01-4fb1-9448-cab47ff1f3b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 105,
                "offset": 6,
                "shift": 58,
                "w": 48,
                "x": 562,
                "y": 109
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f0d4e5ad-f0ea-4c0e-bc3b-036d5a90e96f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 105,
                "offset": 4,
                "shift": 58,
                "w": 48,
                "x": 512,
                "y": 109
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d72b21dc-5096-4f65-bc42-9164313dc4d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 105,
                "offset": 4,
                "shift": 26,
                "w": 24,
                "x": 486,
                "y": 109
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9e833338-3e44-4f89-aec0-b18d51ea89d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 105,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 842,
                "y": 109
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a5e6f53e-9965-48f2-bbab-545ffabd4a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 105,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 460,
                "y": 109
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f491df61-f210-45ba-8554-d735f2dd7560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 105,
                "offset": 5,
                "shift": 52,
                "w": 41,
                "x": 375,
                "y": 109
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a2f390a1-96a5-4478-8171-ab49e72e0976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 105,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 330,
                "y": 109
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "476b64cf-4003-49a8-9497-4e2ae82ff038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 105,
                "offset": 0,
                "shift": 71,
                "w": 71,
                "x": 257,
                "y": 109
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7ead18ad-4cd2-4a90-af98-c441a7ad3b4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 105,
                "offset": -1,
                "shift": 41,
                "w": 43,
                "x": 212,
                "y": 109
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ec8fed06-7176-4233-a6cb-180cc49bb33f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 105,
                "offset": 1,
                "shift": 46,
                "w": 44,
                "x": 166,
                "y": 109
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "110aebde-7d48-40f5-8c5f-41b4216863a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 105,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 128,
                "y": 109
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1a83f20e-ff42-4d6c-8ef1-82fe9efc887f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 105,
                "offset": 3,
                "shift": 30,
                "w": 25,
                "x": 101,
                "y": 109
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "56a93dee-1054-48cc-b255-6bee154f77df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 105,
                "offset": 25,
                "shift": 57,
                "w": 7,
                "x": 92,
                "y": 109
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ca3d4378-952a-4dff-baba-6292d4f85e4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 105,
                "offset": 2,
                "shift": 30,
                "w": 25,
                "x": 65,
                "y": 109
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bfcf5ce7-21cd-4c53-bc46-de631d7fece6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 105,
                "offset": 4,
                "shift": 52,
                "w": 43,
                "x": 800,
                "y": 323
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4644dd0c-23fb-4a70-a1cd-4a78c2d15663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 105,
                "offset": 17,
                "shift": 82,
                "w": 49,
                "x": 845,
                "y": 323
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 64,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}