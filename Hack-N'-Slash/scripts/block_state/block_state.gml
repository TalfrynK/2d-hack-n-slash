///@arg block_sprite
///@arg block_friction
///@arg next_state

var block_sprite = argument0;
var block_friction = argument1;
var next_state = argument2;

set_state_sprite(block_sprite, 0, 0)
image_xscale = -sign(block_distance);
move_and_collide(block_distance, 0);
block_distance = approach(block_distance, 0, block_friction);
if block_distance = 0 { state = next_state; }