///@arg exp_drop

var exp_drop = argument0;

repeat (exp_drop)
{
	var x_exp = x + random_range(-3, 3);
	var y_exp = y + random_range(-3, 3);
	instance_create_layer(x_exp, y_exp, "Effects", o_exp);
	audio_play_sound(a_expr, 2, false);
}
