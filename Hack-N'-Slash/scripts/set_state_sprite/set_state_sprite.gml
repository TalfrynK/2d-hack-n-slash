///@arg sprite
///@arg imageSpeed
///@arg index

var sprite = argument0;
var imageSpeed = argument1;
var index = argument2;


if sprite_index != sprite
{
	sprite_index = sprite;
	image_speed = imageSpeed;
	image_index = index;
}