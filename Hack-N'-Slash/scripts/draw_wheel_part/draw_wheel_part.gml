/// @param x
/// @param y
/// @param radius
/// @param radius-offset
/// @param color
/// @param line_width
/// @param starting_angle
/// @param angle_lenght
/// @param alpha

// This scrpit draws an arc

var xx = argument0
var yy = argument1
var r = argument2
var roff = argument3
var color = argument4
var lwid = argument5
var sang = argument6
var anglen = argument7
// alpha argument8

draw_wheel_ext(x,y,r,roff,color,anglen,360,lwid,sang,false,argument8)