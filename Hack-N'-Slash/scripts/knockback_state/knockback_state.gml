///@arg knockback_sprite
///@arg knockback_friction
///@arg next_state

var knockback_sprite = argument0;
var knockback_friction = argument1;
var next_state = argument2;

set_state_sprite(knockback_sprite, 0, 0)
image_xscale = -sign(knockback_distance);
move_and_collide(knockback_distance, 0);
knockback_distance = approach(knockback_distance, 0, knockback_friction);
if knockback_distance = 0 { state = next_state; }