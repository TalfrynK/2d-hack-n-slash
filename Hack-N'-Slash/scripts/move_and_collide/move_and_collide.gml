///@arg x_move
///@arg y_move

var xmove = argument0;
var ymove = argument1;

if not place_meeting(x+xmove, y, o_wall)	
{
	x += xmove;
}

if not place_meeting(x, y+ymove, o_wall)
{
	y += ymove;
}