///@arg current_value
///@arg target_value
///@arg increment

var current_value = argument0;
var target_value = argument1;
var increment = argument2;

if current_value < target_value
{
	current_value += increment;
	current_value = min(current_value, target_value)
}
else
{
	current_value -= increment;
	current_value = max(current_value, target_value)
}

return current_value